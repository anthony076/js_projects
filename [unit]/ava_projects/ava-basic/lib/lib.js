function promise_add(a, b) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve(a + b)
        }, 1000)
    })
}

function promise_then_add(a, b) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve(a + b)
        }, 3000)
    }).then(function (result) {
        return result
    })
}


module.exports = {
    promise_add: promise_add,
    promise_then_add: promise_then_add,

}

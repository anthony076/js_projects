
import fs from "fs"
import test from 'ava'
import { JSDOM } from 'jsdom';
import { promise_then_add, promise_add } from "../lib/lib"

// =======================
// 測試前的環境設置
// =======================

// 測試前的環境設置，在執行測試前被調用，
// 只會被調用一次，但可以設置多個 before
test.before(t => { });

// 在所有測試都完成後的的清理工作
test.after('cleanup', t => { });

// 測試前的環境設置，每個測試執行前都會被調用
test.beforeEach(t => { });

// 測試後的清理工作，每個測試結束後都會被調用
test.afterEach(t => { });

// =======================
// ava 測試語法
// =======================

// 判斷是否相等
test("t.is()", t => {
    var result = "aaa"
    t.is(result, "aaa", "result is not correct")
})

// 測試是否拋出異常
test("t.throws()", t => {
    function error() { throw new Error("123"); }
    t.throws(error, "123")
})

// 透過 .then() 將異步函數的結果取回
test("test-promise-1", t => {
    return promise_add(3, 4).then(result => {
        t.is(result, 7)
    })
})

// 透過 .then() 將異步函數的結果取回
test("test-promise-2", t => {
    return promise_then_add(3, 3).then(result => {
        t.is(result, 6)
    })
})

// 透過 await 將異步函數的結果取回
test("test-promise-3", async t => {
    var result = await promise_then_add(3, 3)
    t.is(result, 6)
})

// 測試讀取檔案
test.cb('data.txt can be read', t => {
    // `t.end` automatically checks for error as first argument
    fs.readFile('data.txt', t.end);
});

// 強制將判斷測試結果判斷為 FAIL
test.skip("t.fail", t => {
    var result = "aaa"
    // 實際測試結果為 PASS
    t.fail(result, "aaa")
})

// 強制將判斷測試結果判斷為 PASS
test("t.pass", t => {
    var result = "aaa"
    // 實際測試結果為 FAIL
    t.pass(result, "aa")
})

// =======================
// 測試巨集的使用，用於重複調用 test case
// 適合測試邏輯相同，僅輸入值和預期值不同時，需要重複測試相同的測試邏輯
// 類似 pytest @mark.parametrize() 的用法
// =======================
function equal_test(t, input, expect) {
    t.is(input, expect)
}

var test_case = [
    { name: "aa", input: 123, expect: 123 },
    { name: "bb", input: 456, expect: 456 },
    { name: "cc", input: 789, expect: 789 },
]

for (var tcase of test_case) {
    test(tcase.name, equal_test, tcase.input, tcase.expect)
}

// =======================
// 前端測試 by ava + jsdom
// =======================
const config = { runScripts: 'dangerously' }
const window = new JSDOM(fs.readFileSync('./index.html'), config).window
const document = window.document;

test('test-front-end', t => {
    var btn = document.getElementById("btn")
    var msg = document.getElementById("msg")

    t.is(msg.textContent, "None")
    btn.click()
    t.is(msg.textContent, "hello")

});

// =======================
// 其他
// =======================

// 未完成實作的測試，會顯示在 test-report 中
test.todo("add more ava test example")

// t.log() 等同於 console.log()



/* 
    mocha.js 是單元測試框架，沒有斷言功能
    chai.js 提供更加口語化的斷言庫，非必要，也可以用原生的 assert
*/

var { add } = require("../lib/main")
var chai = require("chai")
var expect = chai.expect        // BDD style Test
//var assert = chai.assert      // Assertion Styles Test
//var should = chai.should()    // BDD style Test
// should 會修改 Object.prototype + 對 IE 有相容問題 +  無法客製化錯誤訊息

// chai doc: https://www.chaijs.com/api/bdd/

describe('AssertTest', function () {
    var foo = 'Hello';
    var bar = "World";

    it('aa', function () {
        expect(foo, "should return Hello").to.not.equal(bar);
    });


    it('bb', function () {
        expect("foo").equal("foo");
    });
});

describe('Lib-add', function () {

    it('test add', function () {
        expect(add(1, 2)).to.equal(3);
    });

    it('test minus', function () {
        expect(add(1, "2")).to.not.equal(3);
    });

});

var assert = chai.assert;

mocha.setup('bdd');
mocha.growl(); // <-- Enables web notifications


describe('Array', function () {
    describe('aaa', function () {
        it('should return 0 for first element in array', function () {
            assert.equal([1, 2, 3].indexOf(1), 0);
        });
    });
});

describe('Array', function () {
    describe('bbb', function () {
        it('should return -1 for first element in array', function () {
            assert.equal([1, 2, 3].indexOf(4), -1);
        });
    });
});

describe('Array', function () {
    describe('ccc', function () {
        it('should return 0 for first element in array', function () {
            assert.equal([1, 2, 3].indexOf(1), 0);
        });
    });
});

mocha.run();


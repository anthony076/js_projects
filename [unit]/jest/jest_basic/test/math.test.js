// ES6 的寫法
// jest 有使用 babel-jest, 所以認得 import 的語法
//import { sum, mul, sub, div } from '../src/math';

// nodejs 的寫法，使用 COMMON 模組協議
const math = require('../src/math.js');

class A {}

// describe() ，分組
describe("'測試外部函數", () => {
	test('div()', () => {
		expect(math.div(6, 3)).toBe(2);
	});

	test('mul()', () => {
		expect(math.mul(6, 3)).toBe(18);
	});

	test('sub()', () => {
		expect(math.sub(6, 3)).toBe(3);
	});

	test('sum()', () => {
		expect(math.sum(6, 3)).toBe(9);
	});

});

test('等於的用法', () => {
	expect(2).toBe(2);
	//expect('2').toBe(2);
});

test('判斷對象屬性或陣列元素', () => {
	// 判斷 Object 是否相等
	expect({ aa: 'aa' }).toEqual({ aa: 'aa' });
	// 判斷某個屬性是否包含在Object中
	expect({ bb: 123 }).toHaveProperty('bb', 123);

	// 判斷 Array 是否相等
	expect([ 1, 2, 3 ]).toEqual([ 1, 2, 3 ]);
	// 判斷某個元素是否包含在array中
	expect([ 5, 5, 6, 8, 8 ]).toContain(6);
});

test('判斷是否為空', () => {
	expect(null).toBeNull();
	expect(true).toBeTruthy();
	expect(123).toBeDefined();
});

test('判斷真假', () => {
	expect(true).toBeTruthy();
	expect(false).toBeFalsy();
});

test('使用regular-express', () => {
	expect('Hello world 123').toMatch(/d+/);
});

test('判斷異常', () => {
	expect(() => {
		throw 'ddd';
	}).toThrow('ddd');
});

test('判斷類型', () => {
	const a = new A();
	expect(a).toBeInstanceOf(A);
});

test('所有的匹配器都可以使用 .not() 將預期結果反向', () => {
	expect(5).not.toBe(2);
});

test('測試 PromiseObj resolve', () => {
	// 注意，測試 PromiseObj時，test() 一定要有返回
	const p = new Promise((resolve, reject) => {
		setTimeout(() => resolve('an'), 3000);
	});

	//方法一，在PromiseObj.then( cbFn )，在cbFn中拿到數據後，再使用 jest 的判斷函數
	//return p.then((x) => expect(x).toBe('an'));

	// 方法二，利用 expect(PromiseObj).resolves 來取得狀態改變後，傳遞進來的數據
	return expect(p).resolves.toBe('an');
});

test('測試 PromiseObj reject', () => {
	// 注意，測試 PromiseObj時，test() 一定要有返回
	const p = new Promise((resolve, reject) => {
		reject('error');
	});

	//方法一，在PromiseObj.catct( cbFn )，在cbFn中拿到數據後，再使用 jest 的判斷函數
	//return p.catch(x => expect(x).toBe('error'));

	// 方法二，利用 expect(PromiseObj).rejects 來取得狀態改變後，傳遞進來的數據
	return expect(p).rejects.toBe('error');
});

// 定義異步的耗時函數
const fetchApple = (cbFn) => {
	setTimeout(() => cbFn('apple'), 300);
};

test('測試回調函數是否正確被執行', (done) => {
	/* 
		測試流程，先執行異步函數，執行完才會調用測試函數
		step1，執行流程: 調用耗時函數，test() 結束
		step2，耗時函數調用測試函數，調用時，將 data 傳給測試函數，測試調用時傳入的參數是否正確
		step3，在測試函數中調用 done() ，會再次調用 test(done)，並把測試結果傳遞給 done 參數
		step4, test()函數中的 expect.assertions() 檢查測試結果
	*/

	// 預計會有1個斷言被執行
	expect.assertions(1);

	// 定義回調函數的測試函數()，此函數會在耗時函數中被執行，
	// 若此函數有被執行，則回調函數有正確被調用
	fetchApple((data) => {
		// 檢查傳進來的結果是否正確
		expect(data).toBe('apple');
		done();
	});
});

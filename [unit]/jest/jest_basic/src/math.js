const sum = (a, b) => a + b;
const mul = (a, b) => a * b;
const sub = (a, b) => a - b;
const div = (a, b) => a / b;

/* 
    以下兩種寫法皆可用於 jtest，
    #1 jtest 使用 babe-test，執行測試後，會先透過 babel-test 調用 babel api 進行語法轉譯，再進行測試
    #2 babel 可辨識 COMMON、AMD、ES6 的模塊協議，因此，以下兩種寫法都適用於 jest
*/

// es6 的寫法
//export { sum, mul, sub, div };

// nodejs 的寫法
module.exports = {
	sum: sum,
	mul: mul,
	sub: sub,
	div: div
};

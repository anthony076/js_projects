
let msg = document.querySelector(".msg")
let btn = document.getElementById("btn")

let state = true

function getName() {
    name = (state) ? "Anthony" : "Ching"
    state = !state
    return name
}

function changeMsg() {
    msg.textContent = "hello " + getName()
}

btn.addEventListener("click", changeMsg)

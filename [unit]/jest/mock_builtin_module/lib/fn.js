
const process = require('process');

function cwd() {
    // 返回 C:\Users\ching\Desktop\20190813\mock_fs
    return process.cwd()
}

module.exports = { cwd }

// 此方式仍然無法正確的 mock sayBar() 中的 bar()
/* let { sayBar, bar } = require("../lib/foo")

describe("test sayBar()-1", () => {
    // test fail
    it("wrong way`", () => {
        bar = jest.fn().mockReturnValue("mock bar")
        expect(sayBar()).toBe("mock bar")
    })
}) */

///////////////////////////////

// 此方式才能正確的 mock sayBar() 中的 bar()
// 此方式只會 mock sayBar() 中的 bar()，
let m = require("../lib/foo")

describe("test sayBar()-2", () => {
    // test pass
    it("correct way", () => {
        m.bar = jest.fn().mockReturnValue("mock bar")
        expect(m.sayBar()).toBe("mock bar")
    })

    // test pass
    it("verify bar()", () => {
        const { bar } = require("../lib/bar")
        expect(bar()).toBe("bar")
    })
})

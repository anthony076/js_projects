
jest.mock("../lib/bar.js", () => {
    return { bar: () => "mock bar" }
})

const { foo, sayBar } = require("../lib/foo")

describe("test foo module", () => {
    beforeEach(() => {
        console.log("beforeEach")
        jest.resetAllMocks()
    })

    it("foo()", () => {
        console.log("test foo()")
        expect(foo()).toBe("foo")
    })

    /* it("sayBar", () => {
        let { bar } = jest.requireActual("../lib/bar.js")
        console.log(bar())  // return "bar"

        expect(sayBar()).toBe("bar")    // return "mock bar" in sayBar()
    }) */

    it("sayBar()", () => {
        console.log("test sayBar()")
        //const { sayBar } = jest.requireActual("../lib/foo")   //FAIL
        //const { sayBar } = jest.resetModules("../lib/foo")    //FAIL
        jest.unmock("../lib/bar.js")                            //FAIL

        expect(sayBar()).toBe("bar")    // return "mock bar" in sayBar()
    })

})

describe("mock bar module", () => {
    it("mock sayBar", () => {
        expect(sayBar()).toBe("mock bar")
    })
})
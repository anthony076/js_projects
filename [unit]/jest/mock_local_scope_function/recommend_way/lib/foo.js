const { bar } = require("./bar")

function foo() {
    return "foo"
}

/*
// sayBar() 中的 bar() 是區域函數，無法被 mock，
function sayBar() {
    return bar()
}
*/

function sayBar() {
    // 將bar() 的函數調用，寫成公開函數的方式調用
    // 因為 exportFunctions 是公開的，因此可以被 mock
    return exportFunctions.bar()
}

const exportFunctions = {
    foo,
    sayBar,
    bar
}

module.exports = exportFunctions

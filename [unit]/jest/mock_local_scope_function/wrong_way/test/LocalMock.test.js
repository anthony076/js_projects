
const { foo, sayBar } = require("../lib/foo")

describe("test foo module1", () => {

    it("foo()", () => {
        expect(foo()).toBe("foo")
    })

    it("sayBar()", () => {
        expect(sayBar()).toBe("bar")
    })

})

describe("mock bar module by doMock()", () => {
    it("mock bar", () => {
        jest.doMock("../lib/bar.js", () => {
            return {
                bar: () => "mock bar"
            }
        })

        const { bar } = require('../lib/bar.js');

        // test pass，bar() return "mock bar"
        expect(bar()).toBe("mock bar")

        // test fail，bar() return "bar"
        expect(sayBar()).toBe("mock bar")
    })
})

describe("mock bar module by genMockFromModule()", () => {
    it("mock bar", () => {
        let { bar } = jest.genMockFromModule("../lib/bar.js")
        bar = jest.fn().mockReturnValue("mock bar")

        // test pass，bar() return "mock bar"
        expect(bar()).toBe("mock bar")

        // test fail，bar() return "bar"
        expect(sayBar()).toBe("mock bar")
    })
})

// test\main.test.js

// 此範例沒有在 package.json 中設置 jest.automock = true

const { sayBar } = require("../lib/foo")    // 導入被測試的函數

jest.mock("../lib/bar")
describe("jest mock usage", () => {
    // test pass，
    test("test sayBar by mock", () => {
        var bar = require("../lib/bar")
        console.log(typeof bar)    // 返回  object
        expect(sayBar()).toEqual("i am mock bar from manual-mock")
    })

    // test pass
    test("test sayBar", () => {
        expect(sayBar()).not.toEqual("bar")
    })
})

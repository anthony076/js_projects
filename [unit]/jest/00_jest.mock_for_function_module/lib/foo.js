let bar = require("./bar")

function foo() {
    return "foo"
}

function sayBar() {
    return bar()
}

module.exports = { foo, sayBar }

// 此範例沒有在 package.json 中設置 jest.automock = true

const { sayBar } = require("../lib/foo")    // 導入被測試的函數

// ==== 模擬並改寫 bar.js 中的 bar()，====
// step1，用 jest.mock 模擬 `返回函數的模組`
//        jest.mock( 模組名, 返回函數的工廠函數)，
//        流程，對指定模組名的模組，使用 require 語句後，jest 會調用 jest.mock() 定義的工廠函數來返回自定義的函數
//        注意，透過此方式會修改全局中的 bar 模組，會連帶影響 foo.js 的 sayBar() 調用 bar() 的結果

//        範例 1-1，模擬模組，工廠函數返回自定義的一般函數
//        () => () => "i am not mock bar"，
//        第一個箭頭函數，代表 bar 模組返回函數
//        第一個箭頭函數，代表 該函數返回字串
//jest.mock("../lib/bar", () => () => "i am not mock bar")

//        範例 1-2，模擬模組，且工廠函數返回 mockFn (希望 spy 函數的時候用)
jest.mock("../lib/bar", () => jest.fn(() => "i am not mock bar"))

// step2，導入要模擬的模組
var bar = require("../lib/bar")
console.log(typeof bar)    // 返回 Function

describe("jest mock usage", () => {
    // test pass，
    // foo.js 中的 bar()，已經被上述的 jest.mock() 替換掉
    test("test sayBar by mock", () => {
        console.log(typeof bar) // 返回 funciton
        console.log(bar.mock)   // 使用 2-1 時，返回 undefined，使用 2-2 時，返回 undefined
        expect(sayBar()).toEqual("i am not mock bar")
    })

    // test pass，jest.mock() 的影響是整個測試文件的
    test("test sayBar", () => {
        expect(sayBar()).not.toEqual("bar")
    })

})

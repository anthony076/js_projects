// ==== dom 宣告必須要導入 js 代碼前 ====
// 在 jsdom 中不需要載入 js 代碼 (不需要使用 script 標籤)
// require("../index") 的語句，就相當於是 script 標籤
document.body.innerHTML = `
<body>
<div class="msg">hello</div>
<button id="btn">Click</button>
</body>
`
// ==== 導入js代碼必須要在dom宣告之後 ====
// 相當於是 script 標籤
require("../index")

describe("test changeMsg()", () => {
    // test pass
    it("test msg element", () => {
        // 在測試代碼中仿照 js 代碼中操作 dom 元素
        // 差別是此處操作的是 jsdom 的元素
        let btn = document.getElementById("btn")

        btn.click()

        // 若已經有 *.snap 的快照檔時，會讀取快照檔的內容，並進行比較
        // 若沒有 *.snap 的快照檔時 (第一次使用)，會建立快照檔測試，但不進行測試
        expect(document.body.innerHTML).toMatchSnapshot();
    })
})

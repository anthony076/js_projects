
// 此範例沒有在 package.json 中設置 jest.automock = true

const { sayBar } = require("../lib/foo")    // 導入被測試的函數

// 要模擬的模組，返回的內容是 object
// step1，第一個箭頭函數是工廠函數，當指定模組被使用 require 語句時，
//       jest 會將工廠函數的返回值，傳遞給模組的變數
//       以下為例，運行到 var bar = require("../lib/bar")
//       jest 會將 () => { ... } 的返回值，傳遞給 var bar 的 bar 變數
jest.mock("../lib/bar", () => {
    let bar = () => "i am mock bar"
    return { bar: bar }
})

var bar = require("../lib/bar")
console.log(typeof bar)    // 返回  object

describe("jest mock usage", () => {
    // test pass，
    // foo.js 中的 bar()，已經被上述的 jest.mock() 替換掉
    test("test sayBar by mock", () => {
        expect(sayBar()).toEqual("i am mock bar")
    })

    // test pass，jest.mock() 的影響是整個測試文件的
    test("test sayBar", () => {
        expect(sayBar()).not.toEqual("bar")
    })

})

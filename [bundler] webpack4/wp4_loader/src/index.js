import fullname from './fullname';

//1 載入要打包的 css，告訴 webpack 那些 css 要打包
//      透過 css-loader 會讀取 *.css 的檔案內容，並打包進 bundle.js 中
//      透過 style-loader，才會在 bundle.js 中添加 `還原css樣式到頁面中` 的代碼
//2 只有 css-loader，bundle.js 會有 css 樣式的資料
//  只有 style-loader，會發生錯誤，找不到 css 樣式的資料
//  有 css-loader 和 style-loader ，bundle.js 有 css 的資料，並且會執行插入到頁面的動作
import './style.css';

const person = fullname('ching', 'Yang');

// 建立 element-object
var x = document.createElement('div');
x.innerHTML = person;
x.className = "nameTag";

// 將 element-object 插入到頁面中
document.body.appendChild(x);
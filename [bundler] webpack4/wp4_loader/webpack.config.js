const path = require('path');

// 載入 mini-css-extract-plugin
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

// 載入 html-webpack-plugin
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    // 主程式的位置，執行時將會從 index.js 開始執行
    entry: './src/index.js',
    // 轉譯後，檔案輸出的位置
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname,'dist')    // 當前目錄下的dist 資料夾
    },

    // module屬性，設定 Loader，告訴Webpack啟動時需要執行的模組
    module: {
        // rules 屬性是一個物件陣列，每一個物件則代表一個 Loader的所有相關訊息。
        //      其中，use，代表要使用的 Loader 套件名
        //           test，代表要處理的檔案名，以正則表達式中表示
        //           execlude，代表要排除的位置，Loader 不會處理該位置中的檔案
        rules: [
            {
                use: 'babel-loader',
                test: /\.js$/,
                exclude: /node_modules/
            },
            {
                // 多個 loader 會依序由右至左執行，或由上至下執行，css-loader 放第一個
                use:[
                    { loader: MiniCssExtractPlugin.loader },
                    'css-loader',
                ],
                test: /\.css$/
            }
        ]
    },

    // plugin 屬性，設定 plugin，告訴Webpack啟動時需要執行的 plugin
    plugins: [
        // 用於從 bundle.js 中取出 css，最小化後將檔案輸出成獨立檔案
        new MiniCssExtractPlugin({
            // 指定 css 檔案名稱
            //filename: "[name].css",       // [name] 是 webpack 的變數
            filename: "bundle.css",
            chunkFilename: "[id].css"       // [id]] 是 webpack 的變數
        }),
        
        // 用於將打包產出的所有資源，自動添加到模板中
        new HtmlWebpackPlugin({
            // 
            template: "src/index.html"
        })
    ]
}




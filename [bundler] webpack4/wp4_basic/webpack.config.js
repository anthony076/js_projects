const path = require('path');

module.exports = {
    // 主程式的位置，執行時將會從 index.js 開始執行
    entry: './src/index.js',
    // 轉譯後，檔案輸出的位置
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname,'dist')    // 當前目錄下的dist 資料夾
    }
}
## Video/Live Stream on PC/Android Browser by `ffmpeg(hls) + express + video.js` 

### Description
- 在PC端可播放串流視頻、串流影片
- 在手機可播放串流視頻、串流影片
- Issue，手機播放串流視頻時會出現綠畫面

### Stack
- ffmpeg，將視訊或影片轉換為 hls格式 (*.m3u8 + *.ts)
- express，返回 video.js 和影片檔給用戶端
- videojs，用戶端瀏覽器的解碼和播放，用於播放 hls切割過的影片

### Usage
- 準備，將 demo.flv 放置在根目錄下，串流影片才需要
- step1. 使用 `01_ffmpeg_to_hls_file.bat` 將影片或視訊轉換為 hls格式 (*.m3u8 + *.ts)
- step2. 使用 `02_run_static_server.bat` 啟動靜態伺服器
- step3. 在PC或手機上，使用瀏覽器連線到 `http://ip:1024` 觀看影片

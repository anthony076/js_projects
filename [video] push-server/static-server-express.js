var express = require("express");
var app = express();

const ip = "192.168.137.1"
//const ip = "192.168.2.191"

const port = 1024
var http = require("http").Server(app);

app.get("/", (req, res) => {
    res.sendFile(__dirname + "/view-flv.html")
})

// 把所有靜態的資源，指向 public 資料夾中
app.use(express.static("public"))

http.listen(port, ip, function () {
    console.log(` Listening on http:/${ip}:${port}`)
})
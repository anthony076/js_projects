cd %~dp0

@REM 參數 -f hls，轉換為 hls 影片，會建立 *.m3u8 + 數個 *.ts檔
@REM 參數 -hls_time n，設置每片的長度，默認值為2。單位為秒
@REM 參數 -hls_list_size n，設置播放列表保存的最多條目，設置為0會保存有所片信息，默認值為5
@REM 參數 -hls_wrap n，設置多少片之後開始覆蓋，如果設置為0則不會覆蓋，默認值為0.這個選項能夠避免在磁盤上存儲過多的片，而且能夠限制寫入磁盤的最多的片的數量
@REM 參數 -hls_start_number n，設置播放列表中sequence number的值為number，默認值為0

@REM 將 video 轉為 hls檔
ffmpeg -i demo.flv -preset ultrafast -tune zerolatency -s 640x480 -c:v h264 -c:a aac -f hls -hls_time 30 -hls_list_size 0 -hls_wrap 15 ./public/a.m3u8

@REM 將視訊轉為 hls檔
::ffmpeg -re -rtbufsize 1024M  -f dshow -i video="LG Camera" -preset ultrafast -tune zerolatency -s 640x480 -c:v h264 -c:a aac -f hls -hls_time 5.0 -hls_list_size 0 -hls_wrap 15 ./public/a.m3u8
// 爬取網站，https://v5.airmap.g0v.tw/#/map

const axios = require('axios');

async function get_pm25(locateId) {
    var response = await axios.get("https://api.airmap.g0v.tw/json/airmap.json")

    datas = await response.data
    for (item of datas) {
        if (item.SiteName == locateId) {
            console.log(`
            timestamp:${item.Data.Create_at}
            temp:${item.Data.Temperature}
            humid:${item.Data.Humidity}
            local:${item.SiteName}
            pm25:${item.Data.Dust2_5}
        `)
        }
    }
}

async function get_pm25_all() {
    var response = await axios.get("https://api.airmap.g0v.tw/json/airmap.json")

    datas = await response.data
    for (item of datas) {
        console.log("=====================")
        console.log(`
            timestamp:${item.Data.Create_at}
            temp:${item.Data.Temperature}
            humid:${item.Data.Humidity}
            local:${item.SiteName}
            pm25:${item.Data.Dust2_5}
        `)
    }
}

async function get_history(sensor, locateId, start, end) {
    // https://api.airmap.g0v.tw/json/query-history?group=感測器名稱&id=地點&start=起始時間&end=結束時間
    var response = await axios.get(
        `https://api.airmap.g0v.tw/json/query-history?group=${sensor}&id=${locateId}&start=${start}&end=${end}`
    )

    pm25_history = response.data.Dust2_5
    humidity_history = response.data.Humidity
    temp_history = response.data.Temperature
    isotimes = response.data.isotimes

    console.log(temp_history)
}

function main() {
    // ==== 取得特定位置的 pm25 ====
    // 輸入參數，locateId 的取得
    //  方法一，可由 get_pm25_all() 取得
    //  方法二，點擊 https://v5.airmap.g0v.tw/#/map 的點，由彈出視窗中取得
    locateId = "新北市北港國小"
    //get_pm25(locateId)

    // ==== 取得全台的 pm25 資料 ====
    //get_pm25_all()

    // ==== 查詢特定位置的 pm25 歷史資料
    //輸入參數，sensor、locateId、start、end 的取得
    //  方法一，可由 get_pm25_all() 取得
    //  方法二，點擊 https://v5.airmap.g0v.tw/#/map 的點，由彈出視窗中取得
    get_history(sensor = "LASS-Airbox", locateId = "74DA38E69E54", start = "1563814841", end = "1563858041")
}

main()
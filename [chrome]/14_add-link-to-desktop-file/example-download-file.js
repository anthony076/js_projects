chrome.runtime.onInstalled.addListener(function () {
  // 設定資料到本地儲存
  chrome.storage.local.set({ "aa": 123 }, function () {
    console.log('Data stored:', { "aa": 123 });

    // 讀取本地儲存的資料
    chrome.storage.local.get("aa", function (data) {
      console.log('Data retrieved:', data);

      // 下載檔案到指定路徑
      downloadToFile("list.txt", data.aa);
    });
  });
});

function downloadToFile(fileName, content) {
  const blob = new Blob([content], { type: 'text/plain' });
  const url = URL.createObjectURL(blob);

  chrome.downloads.download({
    url: url,
    filename: fileName,
    conflictAction: 'overwrite',
    saveAs: false
  }, function (downloadId) {
    if (chrome.runtime.lastError) {
      console.error('Error downloading file:', chrome.runtime.lastError);
    } else {
      console.log('File downloaded successfully.');
    }
  });
}

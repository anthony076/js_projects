## Add Link to Desktop file

- Demo

  <img src="doc/demo.png" width=50% height=auto>

- 功能 : 透過本插件取得網頁列表後，調用本機的第三方應用程式來執行

- 使用
  - step1，管理擴充功能 > 載入未封裝項目
  - step2，將目錄指向` [chrome] add-link-to-desktop-file` 的目錄
  - step3，`Add to List 按鈕`，將當前頁面的網址添加到列表中
    
    > 同樣的網址只會添加一次

  - step4，`Empty List 按鈕`，將列表清空

- TODO
  - 移除列表中的特定項目
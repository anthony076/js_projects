document.addEventListener('DOMContentLoaded', function () {

  // ----- 頁面載入後，顯示 list -----
  function showList() {

    chrome.storage.local.get({ 'list': [] }, function (result) {

      var urlList = document.getElementById('urlList');
      urlList.innerHTML = ''

      if (result.list.length != 0) {
        result.list.forEach(function (url) {
          const li = document.createElement('li');
          li.textContent = url;
          urlList.appendChild(li);
        });
      }

    });
  };

  showList()

  // ----- 添加到List -----

  // 取得 addToList 的按鈕控件
  var addToListButton = document.getElementById('addToList');

  // 為按鈕添加click事件
  addToListButton.addEventListener('click', function () {

    chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
      chrome.storage.local.get({ 'list': [] }, function (result) {

        // 將 result.list 轉換為 Set
        var uniqueSet = new Set(result.list);
        // 往 Set 添加新元素，避免添加到重複的元素
        uniqueSet.add(tabs[0].url);

        // 將 Set 轉換為一般的 Array
        var updatedArr = Array.from(uniqueSet);

        // 將寫入後的 list 寫入 storage 中儲存
        chrome.storage.local.set({ 'list': updatedArr }, function () {
          showList();
        });
      });
    });

  });


  // ----- 清空List -----

  var emptyListButton = document.getElementById('emptyList');

  emptyListButton.addEventListener('click', function () {

    // 將 list 寫入 storage 
    chrome.storage.local.set({ 'list': [] });

    showList();

  });

});
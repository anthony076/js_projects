
console.log("hello from bg.js");

// ==== 監聽 connect 事件 ====
//1，在發送端，透過 chrome.runtime.connect() 建立與後端的長連線
//2，在接收端，透過 chrome.runtime.onConnect() 監聽 connect 事件的觸發
//   被觸發後，會將 connect() 返回的 port 對象 傳遞給 onConnect()
chrome.runtime.onConnectExternal.addListener(function (port) {

    // 監聽前端傳遞進來的訊息
    port.onMessage.addListener(function (message) {
        console.log(message); //Test message X

        // 發送訊息給後端
        port.postMessage("Test message Y");
    });
});
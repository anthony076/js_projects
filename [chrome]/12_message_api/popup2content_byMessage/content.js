console.log("hello from content-script")

// 監聽 popup.js 發送過來的訊息
chrome.runtime.onMessage.addListener(function (message, sender, sendResponse) {
    sendResponse({
        message: "Test message Y",
        sender: "content_script.js"
    });

    console.log(`Message '${message}' from Sender 'popup-script of ${sender.id}'`)
});
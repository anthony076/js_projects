console.log("hello from popup-script")

var message = "HELLO background-script, I AM popup-script";
var btn = document.getElementById("send");

// 用來接收後端回傳的訊息
function resopnseCallback(responseObject) {
    console.log(`Message '${responseObject.message}' from Sender '${responseObject.sender}' `)
}

// 按下 popup.html 中的按鈕後才發送訊息
btn.addEventListener("click", function (e) {
    console.log("button clicked ...")
    chrome.runtime.sendMessage(message, resopnseCallback);
});

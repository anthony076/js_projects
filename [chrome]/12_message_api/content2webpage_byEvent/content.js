console.log("hello from content-script");

let message = "HI IAM content.js";

// 當 content-script 被注入網頁後，在當前網頁添加按鈕 
(
    function () {

        var btn = document.createElement("button");
        btn.innerText = "Click"
        // 加入此行，避免按鈕失效
        btn.style.position = "fixed";

        // 加入以下樣式，避免部分網頁無法顯示
        btn.style.display = "block";
        btn.style.width = "65px";
        btn.style.height = "50px";
        btn.style.top = "5%"
        btn.style.left = "10px";

        // 注意，插入位置錯誤有可能會造成按鈕無法點擊，
        // 例如，使前端框架的網頁，若插在 virtual-dom 的位置，有可能會因為重新渲染使用插入的元素失效
        document.body.appendChild(btn);

        // 按下網頁中的按鈕後才發送訊息
        btn.addEventListener("click", function (e) {
            console.log("button clicked ...")

            // ==== 發送訊息給網頁 ====
            // 1，window.postMessage(訊息，接收訊息的網址);
            // 2，window.location.origin 取得當前網址的跟路由，
            //   若 例如，https://github.com/DuLinRain/， 則 window.location.origin = https://github.com/
            // 注意，只有測試的網頁才會有效，因為只有測試網頁才會有接收訊息的代碼，
            // 如果需要在其他網站也生效，必須透過 content.js 也注入 window.addEventListener("message" ...) 的代碼
            window.postMessage(message, window.location.origin);
        });
    }
)()

// ==== 網頁發起訊息，content.js 接收的時候用 ====
/*
window.addEventListener("message",function(me) {
	console.log("message: " + me.data);
});
*/
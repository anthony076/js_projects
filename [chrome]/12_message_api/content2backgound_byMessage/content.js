console.log("hello from content-script")

//let extensionID = "ldeggmchcacighcgppbeifpamklfebca"
// 傳遞給 bs.js 的訊息
let message = "HI IAM WEBPAGE"

// 用來接收後端回傳的訊息
function resopnseCallback(responseObject) {
    console.log(`Message '${responseObject.message}' from Sender '${responseObject.sender}' `)
}

// 當 content-script 被注入網頁後，在當前網頁添加按鈕 
(
    function () {

        var btn = document.createElement("button");
        btn.innerText = "Click"
        // 加入此行，避免按鈕失效
        btn.style.position = "fixed";

        // 加入以下樣式，避免部分網頁無法顯示
        btn.style.display = "block";
        btn.style.width = "65px";
        btn.style.height = "50px";
        btn.style.top = "5%"
        btn.style.left = "10px";

        // 按下網頁中的按鈕後才發送訊息
        btn.addEventListener("click", function (e) {
            console.log("button clicked ...")

            //chrome.runtime.sendMessage(extensionID, message, resopnseCallback);
            // extensionID 是可選參數，若未指定，默認為當前插件的ID
            chrome.runtime.sendMessage(message, resopnseCallback);
        });

        // 注意，插入位置錯誤有可能會造成按鈕無法點擊，
        // 例如，使前端框架的網頁，若插在 virtual-dom 的位置，有可能會因為重新渲染使用插入的元素失效
        document.body.appendChild(btn);
    }
)()
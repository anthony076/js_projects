
console.log("hello from bg.js");

// 後端要回復的訊息
var responseObject = {
    message: "HELLO WEBPAGE, I AM BG",
    sender: "bg.js"
};

// onMessage，接收從插件內部傳遞的訊息
chrome.runtime.onMessage.addListener(
    function (message, sender, sendResponse) {
        sendResponse(responseObject);
        console.log(`Message '${message}' from Sender '${sender.url}'`);
    }
);


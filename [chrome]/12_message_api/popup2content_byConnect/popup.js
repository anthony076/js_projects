console.log("hello from popup-script")

var btn = document.getElementById("send");

// 按下 popup.html 中的按鈕後才發送訊息
btn.addEventListener("click", function (e) {
    console.log("button clicked ...")

    // ==== step1，透過 chrome.tabs.query() 取得當前瀏覽頁面，並將結果傳遞給回調 ====
    chrome.tabs.query({ "active": true }, function (tabs) {

        // ==== step2，透過 tabs.connect() 與 content-script 建立長連接 ====
        var port = chrome.tabs.connect(tabs[0].id, { "name": "connection1" });

        // ==== step3，監聽 content-script 傳遞進來的訊息 ====
        port.onMessage.addListener(function (message) {
            console.log(`[Recieve from content.js] : ${message}`);
        });

        // ==== step4，發送訊息給 content-script ====
        port.postMessage("Test message X");

    });
});

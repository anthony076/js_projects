/*
    #1，一定要有 default，輸入 gulp 的指令時，會自動執行 default 的工作
    #2，task 定義的工作，利用 gulp <工作名> 的方式來執行
    #3，一定要有 cb 的輸入參數，用來調用下一個 task
    #4, 在 task() 中使用 series()時，task名稱必須先註冊過後才能使用
*/

const { series, task } = require('gulp');
const exec = require('child_process').exec;
const del = require("del")
const copy = require("copy")

task("build:clean", (cb) => {
    // 刪除 dist 的資料夾
    del("dist")
    cb()
})

task("build", (cb) => {
    exec('npm run build', function (err, stdout, stderr) {
        console.log(stdout);
        console.log(stderr);
        cb(err);
    });
})

task("build:copy", (cb) => {
    copy('chrome/*.*', 'dist', cb);
})


// 必須放在最後
task("default", series("build:clean", "build", "build:copy"))

var toggle = false;

// 點擊插件按鈕的時候被觸發自動捲動+自動點讚，再次點擊就停止
chrome.browserAction.onClicked.addListener(function (tab) {
    console.log(tab)

    toggle = !toggle;

    if (toggle) {
        // toggle = false -> true，代表未執行過

        // 執行預設代碼
        chrome.tabs.executeScript(tab.id,
            {
                file: 'likescript.js'
            },
            () => { console.log("have execute js file") }
        );

    } else {
        // toggle = true -> false，代表已執行過
        // 執行代碼清除 timer 的代碼，用於停止點讚
        chrome.tabs.executeScript(tab.id,
            {
                code: 'clearInterval(timer);'
            },
            () => { console.log("have execute js code") }
        );

    }
});
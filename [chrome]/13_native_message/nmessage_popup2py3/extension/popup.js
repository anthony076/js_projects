var port = null;

// 顯示訊息
function appendMessage(text) {
    document.getElementById('response').innerHTML += "<p>" + text + "</p>";
}

// 更新前端按鈕樣式
function updateUiState() {
    // 若已連線，遮蔽 connect 按鈕，顯示 send 按鈕
    if (port) {
        document.getElementById('connect-button').style.display = 'none';
        document.getElementById('input-text').style.display = 'block';
        document.getElementById('send-message-button').style.display = 'block';
    } else {
        // 若未連線，顯示 connect 按鈕，遮蔽 send 按鈕
        document.getElementById('connect-button').style.display = 'block';
        document.getElementById('input-text').style.display = 'none';
        document.getElementById('send-message-button').style.display = 'none';
    }
}

function connect() {

    // 建立長連接
    var hostName = "com.google.ching.hua.echo";
    port = chrome.runtime.connectNative(hostName);
    appendMessage("Connecting to native messaging host <b>" + hostName + "</b>")

    // 訊息傳入的回調
    port.onMessage.addListener((message) => {
        appendMessage("Received message: <b>" + JSON.stringify(message) + "</b>");
    });

    // 離線時的回調
    port.onDisconnect.addListener(() => {
        appendMessage("Failed to connect: " + chrome.runtime.lastError.message);

        port = null;
        updateUiState();
    }
    );

    updateUiState();
}

// popup 每次都會重新啟動，重新初始化的時候需要重新連線
document.addEventListener('DOMContentLoaded', function () {
    // ==== 按下 connect鈕後建立長連接 ====
    // 本地應用傳遞訊息完畢後，就會自動關閉，需要手動重新連線，
    // 因此提供 Connect 按鈕進行下一次的連線
    document.getElementById('connect-button').addEventListener('click', connect);

    // ==== 按下 send鈕後傳送訊息給本機的應用程序 ====
    document.getElementById('send-message-button').addEventListener('click', function () {
        message = { "text": document.getElementById('input-text').value };
        port.postMessage(message);
        appendMessage("Sent message: <b>" + JSON.stringify(message) + "</b>");
    });

    updateUiState();
});

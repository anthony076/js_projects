// 建立選單選項1
chrome.contextMenus.create({
    "id": "playTTS",
    "title": "Play: %s",
    "contexts": ["all"],
});

// ==== step3，選單中的事件被觸發後的回調函數 ====
chrome.contextMenus.onClicked.addListener(function (clickData) {

    if (clickData.menuItemId == "playTTS") {
        var selection = clickData.selectionText;

        // ==== 添加 audio 元素到 網頁中 ====
        var audio = new Audio()
        // 參數:ie=編碼, 參數:q=要發音的字串
        // 參數:tl=tts-language, 參數:client=客戶端類型
        audio.src = "https://translate.google.com.tw/translate_tts?ie=UTF-8&&q=" + selection + "&tl=en&client=tw-ob"
        audio.play()
    }
});
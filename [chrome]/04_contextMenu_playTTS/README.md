

# manifest.json 的使用

Usage

Property            | description
--------------------|:-----
manifest_version    | manifest 版本，Chreome 18 以上版本使用 2 
name                | 插件名稱
version             | 插件版本
description         | 插件功能描述
icons               | 圖標設定
browser_action      | 瀏覽頁面設定
default_icon        | 顯示在瀏覽器的插件圖示
default_popup       | 點擊icon後，預設顯示的頁面


Example

![](/doc/manifest_example.png)
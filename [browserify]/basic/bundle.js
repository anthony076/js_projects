(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){

var saybar = function(){
    alert("hello bar")
}

module.exports = {
    barname:"bar",
    saybar:saybar,
}
},{}],2:[function(require,module,exports){

var sayfoo = function(){
    alert("hello foo")
}

module.exports = {
    fooname:"foo",
    sayfoo:sayfoo,
}
},{}],3:[function(require,module,exports){

var bar = require("./bar.js")
var foo = require("./foo.js")

var bar_btn = document.getElementById("bar_btn")
bar_btn.addEventListener("click", bar.saybar)


var foo_btn = document.getElementById("foo_btn")
foo_btn.addEventListener("click", foo.sayfoo)
},{"./bar.js":1,"./foo.js":2}]},{},[3]);

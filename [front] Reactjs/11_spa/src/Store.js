import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import { routerReducer } from 'react-router-redux';
import resetEnhancer from './enhancer/reset.js';

import { composeWithDevTools } from "redux-devtools-extension";

// ==== 處理 reducer ====
const originalReducers = { routing: routerReducer }
const reducer = combineReducers(originalReducers);

// ==== 初始狀態 ====
const initialState = {};

// ==== 處理中間件 ====
const middlewares = [];

if (process.env.NODE_ENV !== 'production') {
    // 用來檢查 react 使用方式是否正確，非必要
    middlewares.push(require('redux-immutable-state-invariant')());
}

const storeEnhancers = compose(
    resetEnhancer,
    composeWithDevTools(),
    applyMiddleware(...middlewares),
);

// ==== 將所有的 reducer、初始狀態、中間件 添加到全局的 Store 中 ====
//1     store 上增加了一個 _reducers 宇段，這是因為無論如何更改 Redux 的 reducer，都應該包含應用啟動時的 reducer ，
//      所以我們把最初的 reducer 存儲下來，在之後每次 store 的 reset 函數被調用時，都會用 combineReducers 來將啟動時的reducer 包含進來。
const store = createStore(reducer, initialState, storeEnhancers);
store._reducers = originalReducers;

export default store;


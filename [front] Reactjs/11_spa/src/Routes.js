import React from 'react';
import { Router, Route, browserHistory, IndexRoute } from 'react-router';
import { Provider } from 'react-redux';
import { syncHistoryWithStore } from 'react-router-redux';
import { combineReducers } from 'redux';

import App from "./pages/App.js";
import store from "./Store.js"

const createElement = (Component, props) => {
    return (
        <Provider store={store}>
            <Component {...props} />
        </Provider>
    )
}

const getHomePage = (nextState, callback) => {
    require.ensure([], function (require) {
        callback(null, require('./pages/Home.js').default);
    }, 'home');
};

const getAboutPage = (nextState, callback) => {
    require.ensure([], function (require) {
        callback(null, require('./pages/About.js').default);
    }, 'about');
};

const getNotFoundPage = (nextState, callback) => {
    require.ensure([], function (require) {
        callback(null, require('./pages/NotFound.js').default);
    }, '404');
};

const getCounterPage = (nextState, callback) => {

    /* 
    // 未導入 CounterPage組件的 reducer 
    require.ensure([], function (require) {
        callback(null, require('./pages/CounterPage.js').default)
    })  
    */

    // 導入 CounterPage組件的 reducer
    require.ensure([], function (require) {

        // 取出 CounterPage組件的 reducer，
        const { page, reducer, stateKey, initialState } = require('./pages/CounterPage.js');

        const state = store.getState();

        // 透過自定義的 reset Store Enhancer 重新設置 Store
        store.reset(
            combineReducers({ ...store._reducers, counter: reducer }),
            { ...state, [stateKey]: initialState }
        );

        callback(null, page);
    }, 'counter');

};

const history = syncHistoryWithStore(browserHistory, store);

const Routes = () => (
    <Router history={history} createElement={createElement}>
        <Route path="/" component={App}>
            <IndexRoute getComponent={getHomePage} />
            <Route path="home" getComponent={getHomePage} />
            <Route path="counter" getComponent={getCounterPage} />
            <Route path="about" getComponent={getAboutPage} />
            <Route path="*" getComponent={getNotFoundPage} />
        </Route>
    </Router>
);

export default Routes;

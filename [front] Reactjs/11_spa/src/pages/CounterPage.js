/* 
    為了將 功能模塊 和 頁面 嚴格區分開，透過 src/pages/CounterPage.js 來顯示 Counter組件的內容
*/

import React from 'react';
import { view as Counter, stateKey, reducer } from '../components/Counter';

const page = () => {
    return (
        <div>
            <div>Counter</div>
            <Counter />
        </div>
    );
};

const initialState = 100;

// 注意，在 CounterPage.js 中，但是只使用 Counter組件的 view ，沒有使用 reducer，
// 為避免在 view 中直接操作Redux Store ，所以只是讓 CounterPage.js 導出 reducer ，
// 由使用 CounterPage組件的模塊來操作Redux 就好
export { page, reducer, initialState, stateKey };

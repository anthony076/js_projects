import React, { PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { increment, decrement } from './actions.js';
import { connect } from 'react-redux';

const buttonStyle = {
    margin: '10px'
};

// stateKey 的預設值
export const stateKey = 'counter';

function Counter({ onIncrement, onDecrement, value }) {
    return (
        <div>
            <button style={buttonStyle} onClick={onIncrement}>+</button>
            <button style={buttonStyle} onClick={onDecrement}>-</button>
            <span>Count: {value}</span>
        </div>
    );
}

Counter.propTypes = {
    onIncrement: PropTypes.func.isRequired,
    onDecrement: PropTypes.func.isRequired,
    value: PropTypes.number.isRequired
};


const mapStateToProps = (state) => ({
    // 透過 stateKey 讀取 state 中的計數值
    value: state[stateKey] || 0
})

const mapDispatchToProps = (dispatch) => bindActionCreators({
    onIncrement: increment,
    onDecrement: decrement
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Counter);


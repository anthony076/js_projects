
import * as actions from "./actions.js";
import reducer from "./reducer";
import view, { stateKey } from "./view.js";

/* 
    stateKey :
    代表的是這個功能模塊需要佔據的 Redux 全局狀態樹的子樹宇段名，
    stateKey 的值是字串'coutner' ，會在 view.js 中定義，因為視圖中的 mapStateToProps 函數往往需要直接訪問 stateKey 值。
*/

export { actions, reducer, view, stateKey }






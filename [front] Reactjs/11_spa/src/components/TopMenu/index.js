import React from 'react';

import { Link } from 'react-router';

const liStyle = {
    display: 'inline-block',
    margin: '10px 20px'
}

const view = () => {
    return (
        <div>
            <ul>
                {/* Link 組件的 to 屬性指向一個路徑，對應的路徑需在 src/Routes 中要有定義*/}
                <li style={liStyle}><Link to="/home">Home</Link></li>
                <li style={liStyle}><Link to="/counter">Counter</Link></li>
                <li style={liStyle}><Link to="/about">About</Link></li>
            </ul>
        </div>
    );
};

export { view };

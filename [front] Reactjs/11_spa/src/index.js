import React from 'react';
import ReactDOM from 'react-dom';

import Routes from './Routes.js';

ReactDOM.render(
    <Routes />,
    document.getElementById('root')
);

// 透過 Redux 管理 React-Router 狀態的寫法
//#1    使用 React-Redux 庫的 Provider 組件，作為數據的提供者， Provider 必須居於接受數據的 React 組件之上。
//      換句話說，要想讓Provider 提供的store 能夠被所有組件訪問到，必須讓 Provider 處於組件樹結構的頂層，
//#2    React-Router 庫的 Router組件，也有同樣的需要
//寫法一，使用 組件的寫法
/*
ReactDOM.render(
    <Provider store={store} >
        <Routes />
    </Provider>,
    document.getElementById('root')
);
*/

// 寫法二，請見 src/Routes.js

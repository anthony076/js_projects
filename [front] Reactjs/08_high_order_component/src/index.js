// 導入 第三方 模塊
import React from 'react';
import ReactDOM from 'react-dom';


// 導入自定義模塊
//import App from "./App.js";
import CountDown from "./CountDown";

import * as serviceWorker from './serviceWorker';

function showCount(count) {
    return <div>{count > 0 ? count : 'Happy New '}</div>
}

ReactDOM.render(
    <CountDown startCount={10}>
        {showCount}
    </CountDown>,
    document.getElementById('root')
);

serviceWorker.unregister();

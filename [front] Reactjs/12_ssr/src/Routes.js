import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, IndexRoute, browserHistory, match } from 'react-router';
import { Provider } from 'react-redux';
import { combineReducers } from 'redux';

import { syncHistoryWithStore } from 'react-router-redux';

import App from './pages/App.js';
import { configureStore } from './Store.js';

const store = configureStore();
const win = global.window;

const getHomePage = (nextState, callback) => {
  require.ensure([], function (require) {
    callback(null, require('./pages/Home.js').default);
  }, 'home');
};

const getAboutPage = (nextState, callback) => {
  require.ensure([], function (require) {
    callback(null, require('./pages/About.js').default);
  }, 'about');
};

// 利用脫水數據，在客戶端產生新的 CounterPage組件實例
const getCounterPage = (nextState, callback) => {
  // 導入 CounterPage組件的 reducer
  require.ensure([], function (require) {
    const { page, reducer, stateKey, initState } = require('./pages/CounterPage.js');

    // 取得網頁中的脫水數據，json-format 的 stateObj
    const dehydratedState = (win && win.DEHYDRATED_STATE);

    // 當前 state
    const state = store.getState();
    // 將當前 state 和 脫水數據 merge 成一個新的 state
    const mergedState = { ...dehydratedState, ...state };

    // 和服務器端類似，首先要獲取一個statePromise ，優先從脫水數據中獲得初始狀態，
    // 只有沒有脫水初始狀態的時候，才使用 initState() 去異步獲取初始化數據
    const statePromise = mergedState[stateKey]
      ? Promise.resolve(mergedState[stateKey])
      : initState();

    // 當statePromise 完成時，使用reset 功能設置 Store 的狀態和 reducer 
    statePromise.then((result) => {
      store.reset(combineReducers({
        ...store._reducers,
        [stateKey]: reducer
      }), {
          ...state,
          [stateKey]: result
        });

      callback(null, page);
    });
  }, 'counter');
};

const getNotFoundPage = (nextState, callback) => {
  require.ensure([], function (require) {
    callback(null, require('./pages/NotFound.js').default);
  }, '404');
};

const history = syncHistoryWithStore(browserHistory, store);

const routes = (
  <Route path="/" component={App}>
    <IndexRoute getComponent={getHomePage} />
    <Route path="home" getComponent={getHomePage} />
    <Route path="counter" getComponent={getCounterPage} />
    <Route path="about" getComponent={getAboutPage} />
    <Route path="*" getComponent={getNotFoundPage} />
  </Route>
);

// 因為使用了服務器端渲染，同時瀏覽器端使用了React-Router 的代碼分片功能，
// 所以瀏覽器端也需要用match 函數來實現路由
export const renderRoutes = (domElement) => {
  match({ history, routes }, (err, redirectLocation, renderProps) => {
    ReactDOM.render(
      <Provider store={store}>
        <Router {...renderProps} />
      </Provider>,
      domElement
    );
  });
}




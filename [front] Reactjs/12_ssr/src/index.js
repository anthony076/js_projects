import {renderRoutes} from './Routes.js';

// 和 server-side一樣，入口函數 src/index.js 把渲染工作完全交給 Routes.js，
// 因此在入口函數所做的只是提供裝載 React組件的 DOM元素
renderRoutes(document.getElementById('root'));

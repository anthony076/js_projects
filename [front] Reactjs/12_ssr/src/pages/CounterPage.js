
import React from 'react';
import { view as Counter, stateKey, reducer } from '../components/Counter';

const page = () => {
    return (
        <div>
            <div>Counter</div>
            <Counter value={0} />
        </div>
    );
};

// 為了讓服務器和瀏覽器端共用一個的數據源，將 initialState 改為函數
// process.env.HOST_NAME 給本地服務器用，localhost:9000 給瀏覽器客戶端用
const END_POINT = process.env.HOST_NAME || 'localhost:9000';
//const initialState = 100;

const initState = () => {
    return fetch(`http://${END_POINT}/api/count`).then(response => {
        if (response.status !== 200) {
            throw new Error('Fail to fetch count');
        }
        return response.json();
    }).then(responseJson => {
        return responseJson.count;
    });
}

// 注意，在 CounterPage.js 中，但是只使用 Counter組件的 view ，沒有使用 reducer，
// 為避免在 view 中直接操作Redux Store ，所以只是讓 CounterPage.js 導出 reducer ，
// 由使用 CounterPage組件的模塊來操作Redux 就好
export { page, reducer, initState, stateKey };

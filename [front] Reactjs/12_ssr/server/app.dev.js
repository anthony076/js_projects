const express = require('express');
const path = require('path');

const renderPage = require('./routes.Server.js').renderPage;
const webpack = require('webpack');

// ======= webpack 相關 =======
// 讀取 webpack 設定檔
const webpackConfig = require('../config/webpack.config.dev.js');
// 取得webpack 實例，webpack 是一個編譯器
const compiler = webpack(webpackConfig);
const webpackDevMiddleware = require('webpack-dev-middleware')(
  compiler,
  {
    noInfo: true,
    publicPath: webpackConfig.output.publicPath
  });

// 取得打包前後的文件說明檔
function getAssetManifest() {
  const content = webpackDevMiddleware.fileSystem.readFileSync(__dirname + '/../build/asset-manifest.json');
  return JSON.parse(content);
}

// ======= Express 相關 =======
//1 app.use()，執行中間件

const app = express();

let assetManifest = null;


// 指定 static 資料夾的路徑
// express.static() 內建的中間件，將指定的路徑訂為 static 資料來，用來返回給 client
app.use(express.static(path.resolve(__dirname, '../build')));

// 執行 webpackDevMiddleware 中間件，啟動時編譯
//1   在 Express 服務器啟動的時候， webpack-dev-middleware 根據 webpack 來編譯生成打包文件，
//    之後每次相關文件修改的時候，就會對應更新打包文件
//2   webpack-dev-middleware 並沒有將產生的打包文件存放在真實的文件系統中，而是存放在內存中的虛擬文件系統
//    所以要獲取資源描述文件不能像產品環境那樣直接 require 就行，而是要讀取webpack-dev-middleware 實例中的虛擬文件系統
//3   webpackDevMiddleware 和 webpack-hot-middleware 的差異
//    webpackDevMiddleware，  適用於伺服器端的檔案更新時自動重新編譯，瀏覽器客戶端需要刷新重新向服務器請求資源時才能得到更新的打包文件
//4   webpack-hot-middleware，將新的代碼主動堆送到瀏覽器，讓瀏覽器主動將網頁內容更新為重新編譯後的內容
app.use(webpackDevMiddleware);

// 執行 webpack-hot-middleware 中間件，更新時編譯
//1   將新的代碼主動堆送到瀏覽器，讓瀏覽器主動將網頁內容更新為重新編譯後的內容
//2   原理，webpack-hot-middleware 的工作原理是讓網頁建立一個websocket 鏈接到服務器，
//    每次有代碼文件發生改變就會有消息推送到網頁中，網頁就會發出請求獲取更新的內容
app.use(require('webpack-hot-middleware')(compiler, {
  log: console.log,
  path: '/__webpack_hmr',
  heartbeat: 10 * 1000
}));

app.use('/api/count', (req, res) => {
  res.json({ count: 100 });
});

// 建立預設路徑的路由
app.get('*', (req, res) => {

  // webpack-dev-middleware 並沒有將產生的打包文件存放在真實的文件系統中，而是存放在內存中的虛擬文件系統
  // 所以要獲取資源描述文件不能像產品環境那樣直接 require 就行，而是要讀取webpack-dev-middleware 實例中的虛擬文件系統
  if (!assetManifest) {
    assetManifest = getAssetManifest();
  }

  return renderPage(req, res, assetManifest);
});

// 設定 express 使用的模板引擎
app.set('view engine', 'ejs');
// 指定模板引擎的模板位置
app.set('views', path.resolve(__dirname, 'views'));

module.exports = app;

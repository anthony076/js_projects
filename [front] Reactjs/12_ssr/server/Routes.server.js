import React from 'react';
import ReactDOMServer from 'react-dom/server';
import { Router, Route, match, RouterContext, IndexRoute, browserHistory } from 'react-router';
import { Provider } from 'react-redux';
import { combineReducers } from 'redux';

import { configureStore } from '../src/Store.js';

// Server 端渲染，在服务器端渲染，没有使用分片，自然也不需要動態加載模塊，所有的页面都可以直接導入
import App from '../src/pages/App.js';
import Home from '../src/pages/Home.js';
import About from '../src/pages/About.js';
import NotFound from '../src/pages/NotFound.js';
import { page as CounterPage, reducer, stateKey, initState } from '../src/pages/CounterPage.js';

// 建立路由規則組件
const routes = (
  <Route path="/" component={App}>
    <IndexRoute component={Home} />
    <Route path="home" component={Home} />
    <Route path="counter" component={CounterPage} />
    <Route path="about" component={About} />
    <Route path="*" component={NotFound} />
  </Route>
);

// counter路由(/counter) 對應 CounterPage組件的 1_初始相關數據 或 2_數據處理函數
const pathInitData = {
  '/counter': {
    stateKey,
    reducer,
    initState
  }
}

// obj-to-JSON 轉換函數
// 用於將 store 中的 stateObj 轉成 json-format 的脫水數據
function safeJSONstringify(obj) {
  return JSON.stringify(obj).replace(/<\/script/g, '<\\/script').replace(/<!--/g, '<\\!--');
}

// 返回渲染後的組件
function renderMatchedPage(req, res, renderProps, assetManifest) {
  // 對於 SSR，每個 request 都會產生新的 store 實例
  const store = configureStore();

  // 取出 CounterPage組件的初始數據和數據處理函數
  const path = renderProps.location.pathname;
  const pathInfo = pathInitData[path] || {};
  const { stateKey, reducer, initState } = pathInfo;

  //1 建立 statePromiseObj， 當 state 的內容改變了，會自動調用 then() 回調函數的內容
  //2 首先要獲取一個statePromise ，優先從脫水數據中獲得初始狀態，
  //  只有沒有脫水初始狀態的時候，才使用 initState() 去異步獲取初始化數據  
  const statePromise = (initState) ? initState() : Promise.resolve(null);

  //1 當 statePromise 完成時，使用reset 功能設置 Store 的狀態和 reducer ，
  //2 註冊 statePromise 的回調函數，statePromise 狀態改變時，會自動執行 .then() 中的函數內容
  //  當 statePromise 的 then 函數被調用時，代表頁面所需的文件已經准備好
  statePromise.then((result) => {

    // ==== step1 更新 state ====
    if (stateKey) {

      // 取得當前的 state
      const state = store.getState();

      // 設置Store 上的狀態，同時也要更新 Store 上的 reducer
      // 透過 1_stateKey 和 自定義的 store.reset()，更換 state 的內容
      store.reset(combineReducers({
        ...store._reducers,
        [stateKey]: reducer
      }), {
          ...state,
          [stateKey]: result
        });
    }

    // ==== step2 準備字串類型的組件(component) ====
    // 根據更新後的 state 產生組件，將 JSX語法的組件轉為字串的形式
    const appHtml = ReactDOMServer.renderToString(
      <Provider store={store}>
        <RouterContext {...renderProps} />
      </Provider>
    );

    // ==== step3 建立第一次渲染用的脫水數據(object-format) ====
    // 注意，此脫水數據 和 step2 中渲染組件的 store 是同一個數據源
    const dehydratedState = store.getState();

    // 返回 SSR 的網頁內容
    return res.render('index', {
      title: 'Sample React App',
      PUBLIC_URL: '/',
      assetManifest: assetManifest,   // 重新打包後的 js-mapping file
      appHtml: appHtml,               // 返回 string 類型 的組件

      // 注意，因為脫水數據可能包含不安全的字符，需要避免跨站腳本攻擊的漏洞，
      // 因此透過 safeJSONstringify() 轉換成較安全的 json-format-string 字串，而不是一般的 string
      dehydratedState: safeJSONstringify(dehydratedState)   // 返回 json-string 的脫水數據
    });
  });
}

// 用於接收來自瀏覽器請求的路由，透過 renderMatchedPage() 
// 找到選擇符合的路由後，返回渲染的組件字串
export const renderPage = (req, res, assetManifest) => {
  // 透過 react-router 提供的 match() 進行路由匹配檢查
  // 如果匹配路由成功，那就調用匹配成功的調用函數來返回給客戶端的內容
  match({ routes: routes, location: req.url },
    // 匹配成功的回調函數
    function (err, redirect, renderProps) {
      if (err) {
        return res.status(500).send(err.message);
      }

      if (redirect) {
        return res.redirect(redirect.pathname + redirect.search);
      }

      if (!renderProps) {
        return res.status(404).send('Not Found');
      }

      return renderMatchedPage(req, res, renderProps, assetManifest);
    });
};

import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import thunkMiddleware from 'redux-thunk'
import { reducer as weatherReducer } from './weather/';

//import Perf from 'react-addons-perf'      // for react v15 performance addons

const reducer = combineReducers({
    weather: weatherReducer
});

const middlewares = [thunkMiddleware];

/* 
// ==== for react v15 performance addons ====

//const win = window;
//win.Perf = Perf
if (process.env.NODE_ENV !== 'production') {
    middlewares.push(require('redux-immutable-state-invariant')());
}
*/

const storeEnhancers = compose(
    applyMiddleware(...middlewares)
    //,(win && win.devToolsExtension) ? win.devToolsExtension() : (f) => f,
);

export default createStore(reducer, {}, storeEnhancers);



import { FETCH_STARTED, FETCH_SUCCESS, FETCH_FAILURE } from './actionTypes.js';
import * as Status from './status.js';

export default (state = { status: Status.LOADING }, action) => {
    switch (action.type) {
        case FETCH_STARTED: {
            // 將異步狀態改為 LOADING
            return { status: Status.LOADING };
        }
        case FETCH_SUCCESS: {
            // 將異步狀態改為 SUCCESS
            return { ...state, status: Status.SUCCESS, ...action.result };
        }
        case FETCH_FAILURE: {
            // 將異步狀態改為 FAILURE
            return { status: Status.FAILURE };
        }
        default: {
            return state;
        }
    }
}

import { FETCH_STARTED, FETCH_SUCCESS, FETCH_FAILURE } from './actionTypes.js';

// 用來避免多個異步請求完成的順序不一致，造成資料錯亂的問題
let nextSeqId = 0;

// 一般的actionObj，會透過中間件調用一般的actionObj
export const fetchWeatherStarted = () => ({
    type: FETCH_STARTED
});

export const fetchWeatherSuccess = (result) => ({
    type: FETCH_SUCCESS,
    result
})

export const fetchWeatherFailure = (error) => ({
    type: FETCH_FAILURE,
    error
})

// 異步的actionObj，異步操作完成後，會透過中間件調用一般的actionObj
export const fetchWeather = (cityCode) => {
    // redux-thunk 會自動把 dispatch 和 getState 帶入
    return (dispatch, getState) => {
        const apiUrl = `/data/cityinfo/${cityCode}.html`;

        // 異步請求的編號，每個異步請求都有自己的編號
        const seqId = ++nextSeqId;

        //如果不相同，代表有新的fetchWeather 被調用，也就是有新的訪問服務器的請求被發出去了，
        //代表當前 seqId 代表的請求就已經過時了，直接丟棄掉，不需要dispatch 任何action 
        const dispatchIfValid = (action) => {
            if (seqId === nextSeqId) {
                return dispatch(action);
            }
        }

        // 透過 dispatchIfValid() 來觸發更新
        dispatchIfValid(fetchWeatherStarted())

        // 進行 fetch() 的異步操作
        fetch(apiUrl).then((response) => {
            // 若 response-status-code 不是200 就拋出錯誤
            if (response.status !== 200) {
                throw new Error('Fail to get response with status ' + response.status);
            }

            // response.json() 將 responseObj 轉為 jsonObj
            response.json()
                .then((responseJson) => {
                    // 觸發 異步操作已成功 的狀態變更
                    dispatchIfValid(fetchWeatherSuccess(responseJson.weatherinfo));
                })
                .catch((error) => {     //捕捉 resopnse.json 的錯誤
                    // 觸發 異步操作以錯誤 的狀態變更
                    dispatchIfValid(fetchWeatherFailure(error));
                });

        }).catch((error) => {     //捕捉 fetch() 的錯誤
            dispatchIfValid(fetchWeatherFailure(error));
        })
    };
}

// ==== configure enzyme for React v16 ====
import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
configure({ adapter: new Adapter() });
// ==========================================

import React from 'react';
import { mount, render } from 'enzyme';
import { createStore, combineReducers } from 'redux';
import { Provider } from 'react-redux';

import { reducer as todosReducer, actions } from "../../../../src/todos/index.js";
import { reducer as filterReducer } from "../../../../src//filter/index.js";
import { FilterTypes } from "../../../../src/constants.js";
import TodoList from "../../../../src/todos/views/todoList.js";
import TodoItem from "../../../../src/todos/views/todoItem.js";

describe('todos', () => {
    it('should add new todo-item on addTodo action', () => {
        const store = createStore(
            combineReducers({
                todos: todosReducer,
                filter: filterReducer
            }), {
                todos: [],
                filter: FilterTypes.ALL
            });

            const subject = (
                <Provider store={store}>
                    <TodoList />
                </Provider>
            );            

        const wrapper = mount(subject);

        // 觸發UI更新
        store.dispatch(actions.addTodo('write more test'));
        expect(wrapper.find('.text').text()).toEqual('write more test');
    });

});

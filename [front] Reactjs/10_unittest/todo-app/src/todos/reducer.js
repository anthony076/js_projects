import { ADD_TODO, TOGGLE_TODO, REMOVE_TODO } from './actionTypes.js';

// reducer 接收 1_舊state，2_ActionObj
export default (state = [], action) => {
    switch (action.type) {

        // 新增代辦事項 
        // action，{ type: ADD_TODO, completed: false, id: 123, text: "123" }
        case ADD_TODO: {

            // 不能直接使用數組push 或者unshift 操作
            // reducer 應該產生新的 Array，而不是去修改原來的 Array
            return [
                // 新的值會從 ActionObj 傳進來
                {
                    id: action.id,
                    text: action.text,
                    completed: false
                },
                ...state    // 舊的state
            ]
        }

        // 變更代辦事項的狀態，更改 state 的 complete 屬性
        // action，{ type: TOGGLE_TODO, id: id }
        case TOGGLE_TODO: {
            return state.map((todoItem) => {
                if (todoItem.id === action.id) {
                    return { ...todoItem, completed: !todoItem.completed }; // 將complete屬性值反向
                } else {
                    return todoItem;
                }
            })
        }

        // 移除代辦事項
        // action，{ type: REMOVE_TODO, id: id }
        case REMOVE_TODO: {
            // 數組的 filter 會返回新的數組
            return state.filter((todoItem) => {
                return todoItem.id !== action.id;
            })
        }

        // reducer 函數會接收到任意action ，包括它根本不感興趣的action ，
        // 透過 default，將 state 原樣返回，表示不需要更改state 。
        default: {
            return state;
        }
    }
}

import { ADD_TODO, TOGGLE_TODO, REMOVE_TODO } from "./actionTypes.js";

// 為了實現為每個產生的待辦事項賦予一個唯一i d 的目的
let nextTodoId = 0;

// 注意，若使用箭頭函數，返回對象時要加上()，否則會被誤認為代碼塊
export const addTodo = (text) => ({
    type: ADD_TODO,
    completed: false,
    id: nextTodoId++,
    text: text
})

export const toggleTodo = (id) => ({
    type: TOGGLE_TODO,
    id: id
})

export const removeTodo = (id) => ({
    type: REMOVE_TODO,
    id: id
})



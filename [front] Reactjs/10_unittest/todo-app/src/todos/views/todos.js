import React from 'react';
import AddTodo from './addTodo.js';
import TodoList from './todoList.js';

import './style.css';

// 在 jsx 中指定元素的樣式，用的不是 HTML 裡的class屬性，而是用className屬性，
// 因為 jsx 中 class 是宣告類的保留字，因此無法直接使用

export default () => {
    return (
        <div className="todos">
            <AddTodo />
            <TodoList />
        </div>
    );
}


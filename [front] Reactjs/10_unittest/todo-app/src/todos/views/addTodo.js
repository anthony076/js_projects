import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types'

import { addTodo } from '../actions.js';

// 為什麼 AddTodo 不使用無狀態的寫法，為了取得 ref 屬性傳進來的 input 元素的引用
// 若寫成 無狀態，引用的處理就會變得複雜
class AddTodo extends Component {

    constructor(props, context) {
        super(props, context);

        this.onSubmit = this.onSubmit.bind(this);
        this.onInputChange = this.onInputChange.bind(this);

        this.state = {
            value: ''
        };
    }

    onSubmit(ev) {
        // 取消該元素的默認行為，
        // 避免按下submit鈕後，自動產生頁面跳轉
        ev.preventDefault();

        const inputValue = this.state.value;

        // 去除空白
        if (!inputValue.trim()) {
            return;
        }

        // 調用 onAdd()，讓用戶知道添加成功
        this.props.onAdd(inputValue);

        // 將輸入框清空
        this.setState({value: ''});
    }

    onInputChange(event) {
        this.setState({
            value: event.target.value
        });
    }

    render() {
        return (
            <div className="add-todo">
                <form onSubmit={this.onSubmit}>

                    {/* ref屬性是 react 內建屬性，父組件不需要傳遞 */}
                    {/* 組件加載完畢後，react 會檢查 組件的 ref屬性，若該屬性是函數就會執行該函數，
                        並將實際的 dom 實例傳給該函數 */}
                    <input className="new-todo" onChange={this.onInputChange} value={this.state.value} />
                    <button className="add-btn" type="submit"> 添加 </button>

                </form>
            </div>
        )
    }
}

AddTodo.propTypes = {
    onAdd: PropTypes.func.isRequired
};


const mapDispatchToProps = (dispatch) => {
    return {
        onAdd: (text) => {
            dispatch(addTodo(text));
        }
    }
};

export default connect(null, mapDispatchToProps)(AddTodo);


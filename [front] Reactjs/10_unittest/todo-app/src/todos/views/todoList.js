import React from 'react';
//import {bindActionCreators} from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types'

import TodoItem from './todoItem.js';
import { toggleTodo, removeTodo } from '../actions.js';
import { FilterTypes } from '../../constants.js';

const TodoList = ({ todos, onToggleTodo, onRemoveTodo }) => {
    return (
        <ul className="todo-list">
            {/* 注意，jsx 的語法只能是 js 的表達式，不能是 js 的語句，因此不能使用 for 或 while 的語句 */}
            {   
                // 將傳進來的 todos，都轉換成一個 TodoItem 的組件，
                // todos 的內容，[{ completed: false, id: 123, text: "123" }, ...]
                todos.map((item) => (
                    <TodoItem
                        key={item.id}
                        text={item.text}
                        completed={item.completed}
                        onToggle={() => onToggleTodo(item.id)}
                        onRemove={() => onRemoveTodo(item.id)}
                    />
                ))
            }
        </ul>
    );
};

TodoList.propTypes = {
    todos: PropTypes.array.isRequired
};

// 根據 Store 上的 filter 狀態，決定要顯示的項目
const selectVisibleTodos = (todos, filter) => {
    switch (filter) {
        case FilterTypes.ALL:
            return todos;
        case FilterTypes.COMPLETED:
            return todos.filter(item => item.completed);
        case FilterTypes.UNCOMPLETED:
            return todos.filter(item => !item.completed);
        default:
            throw new Error('unsupported filter');
    }
}

const mapStateToProps = (state) => {
    return {
        todos: selectVisibleTodos(state.todos, state.filter)
    };
}

// mapDispatchToProps()，可用 redux 的 bindActionCreators() 簡化
/*
    方法一，
        const mapDispatchToProps = (dispatch) => bindActionCreators({
        onToggleTodo: toggleTodo,
        onRemoveTodo: removeTodo
        }, dispatch);

    方法二，
        const mapDispatchToProps = {
            onToggleTodo: toggleTodo,
            onRemoveTodo: removeTodo
        }
*/
const mapDispatchToProps = (dispatch) => {
    return {
        onToggleTodo: (id) => {
            dispatch(toggleTodo(id));
        },
        onRemoveTodo: (id) => {
            dispatch(removeTodo(id));
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(TodoList);


// 導入 第三方 模塊
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from "react-redux";
import store from "./Store.js";

// 導入自定義模塊
import TodoApp from "./TodoApp.js";

import * as serviceWorker from './serviceWorker';

ReactDOM.render(
    <Provider store={store}>
        <TodoApp />
    </Provider>,
    document.getElementById('root')
);

serviceWorker.unregister();

// 導入 第三方 模塊
import React from 'react';

// 導入自定義模塊
import { view as Todos } from './todos/';
import { view as Filter } from './filter/';

function TodoApp() {
  return (
    <div>
      <Todos />
      <Filter />
    </div>
  );
}

export default TodoApp;

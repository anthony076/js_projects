import React from 'react';
import { connect } from 'react-redux';
import { setFilter } from '../actions.js';
import PropTypes from 'prop-types'

// children 組件是內建屬性，代表被包裹的子組件，不需要透過外部傳入，直接使用即可
// 例如，<Foo><Bar>123</Bar></Foo>，
//      在Foo 組件中使用 children，指的是被包裹的 Bar 組件
//      在Bar 組件中使用 children，指的是被包裹的 123
const Link = ({ active, children, onClick }) => {
  // active=true，當前實例就是被選中的過濾器，不該被再次選擇，所以不應該渲染超鏈接標籤a 。
  if (active) {
    return <b className="filter selected">{children}</b>;
  } else {

    // active=false,渲染一個超鏈接標籤
    return (
      <a href="#" className="filter not-selected" onClick={(ev) => {
        // 避免執行元素預設的動作，避免產生跳轉的動作
        ev.preventDefault();
        onClick();
      }}>
        {children}
      </a>
    );
  }
};

Link.propTypes = {
  active: PropTypes.bool.isRequired,
  children: PropTypes.node.isRequired,  // 特殊的属性children，用來代表被包裹住的子组件
  onClick: PropTypes.func.isRequired
};

const mapStateToProps = (state, ownProps) => {
  return {
    active: state.filter === ownProps.filter
  }
};

const mapDispatchToProps = (dispatch, ownProps) => ({
  onClick: () => {
    dispatch(setFilter(ownProps.filter));
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(Link);

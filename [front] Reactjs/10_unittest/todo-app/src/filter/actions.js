import { SET_FILTER } from './actionTypes.js';

// 注意，若使用箭頭函數，返回對象時要加上()，否則會被誤認為代碼塊
export const setFilter = filterType => ({
    type: SET_FILTER,
    filter: filterType
});

const { Observable, TestScheduler } = require('rxjs');
const assert = require('assert');


// @範例，利用 nodejs 內建的斷言庫進行測試
// 缺點，不會收集測試結果，不會產生報表
const assertFn = (act, exp) => assert.deepEqual(act, exp, "666")
scheduler = new TestScheduler(assertFn)

const source = '--a--b--|'
const expected = '--a--c--|'

const source$ = scheduler.createColdObservable(source);
scheduler.expectObservable(source$).toBe(expected)
scheduler.flush()



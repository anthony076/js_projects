// 可以用在 nodejs 或 webpack 的環境
const { Observable, TestScheduler } = require('rxjs');

let scheduler;

describe('同步數據流的測試', () => {
	// 準備同步數據流
	const s1$ = Observable.of(1, 2, 3);

	test('將數據流轉成 Array 後測試', () => {
		const arr$ = s1$.toArray();
		arr$.subscribe((v) => expect(v).toEqual([1, 2, 3]));
	});

	test('將數據經過運算處理後再測試，只需要判斷一個值', () => {
		// 將數據流進行加總
		const result$ = s1$.reduce((a, b) => a + b, 0);

		// 測試
		result$.subscribe((v) => expect(v).toBe(6));
	});

	test('利用數據流個數測試', () => {
		// 準備數據
		const count$ = s1$.count();

		// 測試
		count$.subscribe((v) => expect(v).toBe(3));
	});
});

describe('異步數據流內容的測試', () => {
	// 準備同步數據流
	const s1$ = Observable.interval(1000).take(5);

	test('利用 Array，方法一', () => {
		const arr$ = s1$.toArray();
		arr$.subscribe(
			(v) => expect(v).toEqual([0, 1, 2, 3, 4])
		);
	});

	test('利用 Array，方法二', () => {
		const result = [];

		s1$.subscribe(
			(v) => result.push(v),
			null,
			() => expect(result).toEqual([0, 1, 2, 3, 4]));
	});

});

describe('異步數據流時間測試的基礎', () => {
	/* 
		驗證時間時，無法透過時間差，因為時間差不構精確
		要驗證時間必須透過 TimeScheduler
	*/

	// initial，每次測試新的用例前，建立新的 TestScheduler實例，TestSchedulerObj
	beforeEach(() => {
		// 語法，new TimeScheduler( 判斷函數 )
		// TestScheduler類的構造函數，接受一個判斷函數，
		//1		該判斷函數具有實際值和期望值的參數，TestScheduler類會在調用判斷函數時，自動將實際值和期望值傳入
		//2		判斷函數 不限定任何斷言庫，依照自己慣用的斷言庫即可
		//3		注意，傳入的是對象，因此應該使用 toEqual 來判斷，而不是使用 toBe(值)
		scheduler = new TestScheduler(
			(act, exp) => expect(act).toEqual(exp)
		)
	})

	test('TestScheduler 的基本使用', () => {
		// 利用彈珠語法建立期望值和實際值
		const source = '--a--b--|'
		const expected = '--a--b--|'

		// 透過 TimeScheduler類，讓我們可依照彈珠圖建立異步數據流
		const source$ = scheduler.createColdObservable(source);

		// 對預期數據流和實際數據流進行測試
		scheduler.expectObservable(source$).toBe(expected);

		// TestScheduler控制下的所有數據流開始運轉
		scheduler.flush()
	});

});

describe("彈珠圖表示法相關", () => {
	// initial，每次測試新的用例前，建立新的 TestScheduler實例，TestSchedulerObj
	beforeEach(() => {
		scheduler = new TestScheduler(
			(act, exp) => expect(act).toEqual(exp)
		)
	})

	test('彈珠數據源取出的是字串', () => {
		// 利用彈珠語法建立期望值和實際值
		const source = '-a-b|'
		const expected = '-a-b|'

		// 依照彈珠圖建立異步數據流，並透過第二參數進行類型轉換
		const source$ = scheduler.createColdObservable(source, { a: 1, b: 3 });

		// 對預期數據流和實際數據流進行測試
		scheduler
			.expectObservable(source$.map(x => x * 2))	// 非字面量數據源
			.toBe(expected, { a: 2, b: 6 });			// 並透過第二參數對預期彈珠數據流進行類型轉換後，才進行測試 

		// TestScheduler控制下的所有數據流開始運轉
		scheduler.flush()
	});

	test('讓字面量有效的方式', () => {
		// 利用彈珠語法建立期望值和實際值
		const source = '-1-3|'
		const expected = '-2-6|'


		// 透過 TimeScheduler類，讓我們可依照彈珠圖建立異步數據流
		const source$ = scheduler.createColdObservable(source);

		// 對預期數據流和實際數據流進行測試
		scheduler
			.expectObservable(source$.map(x => (x * 2).toString()))	// 將取出的上游彈珠數據源轉成字面量
			.toBe(expected)

		// TestScheduler控制下的所有數據流開始運轉
		scheduler.flush()
	});

	test('修改彈珠符號 - 預設的時間', () => {
		// 注意時間設定是全域的，會影響到其他的測試用例

		// 取得原始時間設定
		const originalTimeFactor = TestScheduler.frameTimeFactor;

		// 修改原始時間設定
		TestScheduler.frameTimeFactor = 1

		// 建立計時器
		let time = scheduler.createTime('-----|')

		expect(time).toBe(5);

		// 恢復原始時間設定
		TestScheduler.frameTimeFactor = originalTimeFactor
	});

	test('彈珠符號 | 對應的測試', () => {
		const source$ = Observable.empty()
		const expected = "|"

		scheduler.expectObservable(source$).toBe(expected)

		scheduler.flush()
	});

	test('彈珠符號 # 對應的測試', () => {
		const source$ = Observable.throw("error")
		const expected = "#"

		scheduler.expectObservable(source$).toBe(expected)

		scheduler.flush()
	});

	test('ObservableObj 永不完結的測試', () => {
		const source$ = Observable.never()
		const expected = "---"

		scheduler.expectObservable(source$).toBe(expected)

		scheduler.flush()
	});

})

describe("異步數據流測試範例", () => {
	// initial，每次測試新的用例前，建立新的 TestScheduler實例，TestSchedulerObj
	beforeEach(() => {
		scheduler = new TestScheduler(
			(act, exp) => expect(act).toEqual(exp)
		)
	})

	test('interval 測試用例', () => {
		// 使用TestSchedulerObj控制時間
		// 如果沒有帶入scheduler，會依照實際時間來產生數據流 
		const source$ = Observable
			.interval(10, scheduler)
			.take(4)
			.map(x => x.toString())

		// 10ms後開始產生數據，每個數據間隔10ms，產生完第四個數據(數字3)後立刻完結
		// 注意數字3和完結是同時發生的，所以使用 ()
		const expected = "-012(3|)"
		scheduler.expectObservable(source$).toBe(expected)
		scheduler.flush()
	});

	test('merge 測試用例，merge彈珠圖的問題', () => {
		/* 
			缺點，以下圖為例
			s1、s2 在 2 的時候產生第一個數據，利用彈珠圖共占用 4個位置(40ms)
			若 s1、s2 的第二個數據小於 40ms 就無法用彈珠圖表示
		*/
		const s1 = '-a----b---|';
		const s2 = '-c----d---|';
		// 			123456789ab			在 2 和 7 產生數據
		const expected = '-(ac)-(bd)|';
		// 				  123456789ab	在 2 和 7 開始合併數據

		const s1$ = scheduler.createColdObservable(s1);
		const s2$ = scheduler.createColdObservable(s2);

		const mergedSource$ = Observable.merge(s1$, s2$);

		scheduler.expectObservable(mergedSource$).toBe(expected)

		scheduler.flush()
	});

	test('解決merge彈珠圖的問題，利用hot-observableObj 將數據流分開，避免數據同時發生', () => {

		const s1 = '-a-^b----|';
		const s2 = '---^---c-|';
		const expected = '-b--c-|';

		// 將 createColdObservable() 改成 createHotObservable
		const s1$ = scheduler.createHotObservable(s1);
		const s2$ = scheduler.createHotObservable(s2);

		const mergedSource$ = Observable.merge(s1$, s2$);

		scheduler.expectObservable(mergedSource$).toBe(expected)

		scheduler.flush()
	});

	test('測試訂閱時機是否正確，以concat() 為例', () => {
		const source1 = '-a-b--|';
		// 				 1234567

		const source2 = '-c--d-|';
		// 				 789abcd

		const expectedRes = '-a-b---c--d-|';
		// 				     123456789abcd

		const expectedSub1 = '^-----!';
		//	   				  1234567
		const expectedSub2 = '------^-----!';
		//	   				  123456789abcd

		// 建立數據流
		const src1$ = scheduler.createColdObservable(source1);
		const src2$ = scheduler.createColdObservable(source2);

		// 檢查 concat 的結果是否正確
		scheduler.expectObservable(src1$.concat(src2$)).toBe(expectedRes);

		// 檢查 src1 的訂閱時間是否正確
		scheduler.expectSubscriptions(src1$.subscriptions).toBe(expectedSub1);
		// 檢查 src2 的訂閱時間是否正確
		scheduler.expectSubscriptions(src2$.subscriptions).toBe(expectedSub2);

		scheduler.flush();

	});
})

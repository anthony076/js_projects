
// Set 的基本用法
function example1(){

    // @範例一，
    var s = new Set([1, 2, 3, 4, 4])
    console.log(s)              // 返回 Set { 1, 2, 3, 4 }，

    // @範例二，
    var s = new Set();

    [2, 3, 5, 4, 5, 2, 2].forEach(x => s.add(x));
        
    console.log(s)  // 返回 Set { 2, 3, 5, 4 }，    
}


// Set加入值的時候，不會發生類型轉換，
// 類似於精確相等運算符（===），使用的算法叫做“Same-value-zero equality”
function example2(){
    console.log( new Set([5, "5"]))                 // 返回 Set { 5, '5' }，
    console.log( new Set([NaN, NaN]))               // 返回 SetSet { NaN }
    console.log( new Set([{}, {}]))                 // 返回 Set { {}, {} }
    console.log( new Set([undefined, undefined]))   // 返回 Set { undefined }

}


// Set 實例常用方法
function example3(){
    var s = new Set([1,2,2,3,3,4,6,6,5])

    console.log(s)              // 返回 Set { 1, 2, 3, 4, 6, 5 }，
    console.log(s.size)         // 返回 6，
    console.log(...s)           // 返回 1 2 3 4，
    
    // 刪除值
    console.log(s.delete(1))    // 返回 true，
    console.log(s)              // 返回 Set { 2, 3, 4, 6, 5 }，
    
    // 判斷值
    console.log(s.has(1))       // 返回 ，false
    console.log(s.has(2))       // 返回 ，true

    // 添加值
    s.add(1)
    console.log(s)              // 返回 Set { 2, 3, 4, 6, 5, 1 }，

    // 清除所有值
    s.clear()
    console.log(s)              // 返回 ，Set {}

    // 將 Set 轉為一般的 Array，去重之後再轉換
    var s = new Set([1,2,2,3])
    console.log(Array.from(s))  // 返回 ，[ 1, 2, 3 ]

}

// Set 的遍歷
function example4(){
    var s = new Set([4,2,2,1,4,4,3])

    // 默認遍歷器生成函數就是它的values方法
    // 因此，可以省略values方法，直接用for...of循環遍歷 
    for(let i of s){
        console.log(i)          // 返回 4 2 1 3 ，
    }

    // Set.keys() 範例
    for(let i of s.keys()){
        console.log(i)          // 返回 4 2 1 3 ，
    }

    // Set.values() 範例
    for(let i of s.values()){
        console.log(i)          // 返回 4 2 1 3 
    }

    // Set.entries() 範例
    // Set 沒有鍵名，使用 Set.entries()時，键名和键值是同一个值
    for(let i of s.entries()){
        console.log(i)          // 返回 [4,4] [2,2] [1,1] [3,3]
    }

    // Set.forEach() 範例
    s.forEach((value, key) => console.log(key + ":" + value)) // 返回 ，4:4 2:2 1:1 3:3
}

// WeakSet 的使用
function example5(){
    
    // Error，數組的成員成為WeakSet的成員，而不是a數組本身
    //var ws = new WeakSet([3, 4])
    //console.log(ws)

    // 不報錯，但 WeakSet 為空
    //var ws = new WeakSet( [ {a:1}, {a:2} ] )  // 返回 WeakSet {}
    //console.log(ws)

    // 像 WeakSet 添加成員，必須透過 WeakSet.add()，且傳入的參數必須為對象
    var ws = new WeakSet()
    
    // 錯誤寫法，{a:1} 因為沒有任何引用，因為被垃圾回收機制回收
    //ws.add({a:1})

    // 正確添加方法
    var foo = { name:"foo" }
    
    // 添加
    ws.add(foo)
    console.log(ws)             // WeakSet {}
    console.log(ws.has(foo))    // true

    // 刪除
    ws.delete(foo)
    console.log(ws.has(foo))    // false

}


//example1()
//example2()
//example3()
//example4()
example5()
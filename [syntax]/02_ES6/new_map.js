
// Map() 的常用方法
function example1(){

    // @範例一，利用 Array 添加 Map結構的成員
    // new Map( [ [value-key, value], [value-key, value], ... ] )
    var m = new Map(
        [
            [12, "this is 12"],
            [{a:1}, "this is object"]
        ]
    )   
    
    console.log(m)      // 返回 Map { 12 => 'this is 12', { a: 1 } => 'this is object' }，

    // @範例二，任何 1_具有Iterator接口、且 2_每個成員都是一個雙元素的數組的數據結構都可以當作Map構造函數的參數
    var s = new Set([
        ["foo",1],
        ["bar",2],
    ])

    var m = new Map([
        [1, "this is 1"],
        [2, "this is 2"],
    ])

    var map1 = new Map(s)
    console.log(map1)       // 返回 Map { 'foo' => 1, 'bar' => 2 }，

    var map2 = new Map(m)
    console.log(map2)       // 返回 Map { 1 => 'this is 1', 2 => 'this is 2' }，

    // @範例三，Map() 實例 常用方法
    var objA = 11
    var objB = {a:1}
    var m = new Map()

    // 添加成員 Map().set(value-key, value))
    m.set(objA, "thi is 11")
    m.set(objB, "thi is objB")
    console.log(m)              // 返回 Map { 11 => 'thi is 11', { a: 1 } => 'thi is objB' }，

    // 存取成員，Map().get(value-key)，利用 value-key 存取 value
    console.log(m.get(objA))    // 返回 thi is 11，

    // 判斷 value-key 是否存在
    console.log(m.has(objA))    // 返回 true，

    // 刪除 value-key 和其值
    console.log(m.delete(objA))     // 返回 true，刪除成功
    console.log(m.has(objA))        // 返回 false，objA 不存在
}

// Map 使用的注意事項
function example2(){
    var m = new Map()

    // @範例一，同一個 key 重複賦值時，後面會覆蓋前面
    m.set(1, "aaa")
    m.set(1, "bbb")
    console.log(m)  // 返回 ，Map { 1 => 'bbb' }

    // @範例二，讀取未設定的 key 會返回 undefined
    console.log(m.get(2))    // 返回  undefined

    // @範例三，只有對同一個對象的引用，Map 結構才將其視為同一個鍵
    //        對於合成對象，例如 Arrary 或 Object，需要透過變數將引用固定
    // 錯誤寫法，未使用同一個對象引用
    m.set(["a"], 555)
    console.log(m.get(["a"]))   // 返回 ，undefined

    // 正確寫法
    var arr = ["a"]
    m.set(arr, 555)
    console.log(m.get(arr))     // 返回 555，

}

// Map 的遍歷
function example3(){
    var m = new Map([
        [1,"foo"],
        [2,"bar"],
        [3,"task"]
    ])

    // 使用 Map().keys() 遍歷 key
    for(var i of m.keys()){
        console.log(i)              // 返回 1 2 3，
    }

    // 使用 Map().values() 遍歷 values
    for(var i of m.values()){
        console.log(i)              // 返回 "foo" "bar" "task"
    }

    // 使用 Map().entries() 遍歷 key and values
    for(var [key, value] of m.entries()){
        console.log([key, value])   // 返回 [1, "foo"] [2, "bar"] [3, "task"]
    }

    // 直接使用 Map object ，實際上是調用 Map().entries()
    for(var [key, value] of m){
        console.log([key, value])   // 返回 [1, "foo"] [2, "bar"] [3, "task"]
    }

}

// Map 和其他數據結構的互相轉換
function example4(){

    // @範例一，Map 轉 Array
    var m = new Map([
        [1,"foo"],
        [2,"bar"],
        [3,"task"]
    ])

    var arr_keys = [...m.keys()]
    console.log(arr_keys)       // 返回 ，[ 1, 2, 3 ]

    var arr_values = [...m.values()]
    console.log(arr_values)     // 返回 ，[ 'foo', 'bar', 'task' ]

    var arr_values = [...m.entries()]
    console.log(arr_values)     // 返回 ，[ [ 1, 'foo' ], [ 2, 'bar' ], [ 3, 'task' ] ]

    // @範例二，Array 傳 Map
    // 將數組傳入Map 構造函數

    // @範例三，Map 轉對象
    // 如果有非字符串的鍵名，那麼這個鍵名會被轉成字符串，再作為對象的鍵名
    var m = new Map([
        [1,"foo"],
        [2,"bar"],
        [3,"task"]
    ])

    var obj = {}

    for (var [key, value] of m){
        obj[key] = value
    }

    console.log(obj)    // 返回 { '1': 'foo', '2': 'bar', '3': 'task' } 

    // @範例四，對象轉 Map
    var obj = { '1': 'foo', '2': 'bar', '3': 'task' }
    var m = new Map()
    
    for (var [key, value] of Object.entries(obj)){
        m.set(key, value)
    }
    
    console.log(m)      // 返回 Map { '1' => 'foo', '2' => 'bar', '3' => 'task' } 

    // @範例五，Map 和 JSON 互轉
    // http://es6.ruanyifeng.com/?search=%3C&x=0&y=0#docs/set-map

}

// WeakMap 的基本使用範例
function example5(){
    // WeakMap() 只有四個方法可用，set()、get()、has()、delete()

    // 使用 set 方法添加成员
    const wm1 = new WeakMap();
    const key = {foo: 1};
    wm1.set(key, 2);
    console.log(wm1.get(key))        // 返回 2 

    // WeakMap 也可以接受一个数组，作为构造函数的参数
    const k1 = [1, 2, 3];
    const k2 = [4, 5, 6];
    const wm2 = new WeakMap([[k1, 'foo'], [k2, 'bar']]);
    console.log(wm2.get(k2))        // 返回  bar
}

// WeakMap 弱引用的只是鍵名，而不是鍵值。鍵值依然是正常引用
function example6(){
    var wm = new WeakMap()
    
    var key = {}
    var obj = "this is obj"

    wm.set(key, obj)
    console.log((wm.get(key)))  // 返回  this is obj 

    // 刪除對 value 的引用
    obj = null
    
    // 弱引用只對鍵名，因此，即使刪除 value 的引用 (此例為 obj)，WeakMap 內部的引用仍然存在
    console.log(wm.get(key))    // 返回  this is obj 

}

//example1()
//example2()
//example3()
//example4()
//example5()
example6()
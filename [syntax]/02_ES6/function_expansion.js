
// ===== ES6 函數可使用默認值 =====
// 當參數為 undefined，才會觸發該參數等於默認值，null則沒有這個效果
function add(x, y=5){
    console.log(x, y)
}

add(1)              // 1 5
add(1, undefined)   // 1 5
add(1, null)        // 1 null


// ===== ES6 函數可使用解構賦值 =====
function foo({x, y=5}){
    console.log(x, y)
}

foo({})             // undefined 5，解構賦值
foo({x:1, y:1})     // 1 1，解構賦值
foo({x:1})          // 1 5，解構賦值
                    // Error，如果函數foo調用時沒提供參數，變量x和y就不會生成，從而報錯
//foo()             // 可透過 Function foo( {x, y=5} = {} ) 避免            


// ==== 函數的length屬姓，返回沒有指定默認值的參數個數 ====
//1 返回未使用默認值的參數個數，位於默認值後方的參數，不會被 length 屬性計入
//4 函數的 length屬性，不計入 rest參數
function barA(x, y, z=1){
    console.log(x, y, z)
}

function barB(x, y=1, z){
    console.log(x, y, z)
}

console.log(barA.length) // 2，返回未使用默認值的參數個數
console.log(barB.length) // 1，位於默認值後方的參數，不會被 length 屬性計入


// ==== 函數參數作用域，函數參數的作用域是獨立的(等效 let 宣告參數) ====
//1     宣告函數參數時，function f(x, y=x)，會在參數區域建立`獨立的作用域`，
//      等同於 let y = x
//2     函數調用時，會先尋找傳進來的值，
//      若無，尋找全域值，
//      若再無，報錯，因為 let 不允許只宣告不賦值

// @範例一
var x = 1;

// 參數y = x 形成一個單獨的作用域，調用時指向傳入的 x
function f(x, y=x) {
  console.log(y);
}

f(2) // 2

// @範例二
var x = 1;

//1 參數 y = x 形成一個單獨的作用域，調用時指向傳入的 x
//  若調用時，變數不存在，指向全局的變數
//2 函數參數和 函數體內是不同的作用域
function g(y=x) {
  let x = 2;
  console.log(y);
}

g() // 1


// ==== 使用 rest參數，...變量名，用於獲取函數的多餘參數 ====
//1 多個變量會儲放在一個 array 中，並依照指定的變量名來命名
//2 相較於 函數的 arguments object，
//      arguments 是 array-like object，是一個類似 array 的 object
//      rest參數建立的 array ，是真的 array
//3 rest 參數是函數參數中的最後一個參數，後面不能再接其他參數
//4 函數的 length屬性，不計入 rest參數

function h(a, ...b){
    console.log(a, b)
}

h(1,2,3,4,5)

// ==== ES6 修改 嚴格模式在函數中的使用規則 ====
//1 在 ES5 時，函數內部可以設定為嚴格模式，且適用於 1_函數參數宣告 和 2_函數體 兩種作用域
//  但函數執行時，事先執行 函數參數宣告，再執行函數體，造成嚴格模式的錯誤，在函數體才發現
//2 在 ES6，只要參數使用了默認值、解構賦值、或者擴展運算符，就不能顯式指定嚴格模式
//  除非使用 全局性的嚴格模式，或是將函數包在 IIFE 中
function i(x=1){
    // 錯誤寫法，使用參數默認值後，就不能使用嚴格模式
    'use strict'
    console.log(x)
}

//i()


// 對象鍵名的簡化
function example1() {
    console.log("==== example1 ====")

    // @ 範例一，屬性的鍵名簡化
    // 錯誤，不允許直接寫入值
    //console.log({"abc"})

    // 正確，ES6 簡化鍵名，鍵名 = 變數名(或函數名) 
    const foo = "bar"
    console.log({ foo })    // {foo: "bar"}

    // @ 範例二，方法的鍵名簡化
    var obj = {
        m() {
            console.log("I amd method")
        }
    }

    obj.m()   //I amd method

    // @ 範例三，只要是對象都可使用
    let f = (x, y) => ({ x, y })
    console.log(f(1, 2))    // { x: 1, y: 2 }

}

// 在對象宣告中，用 [字面量] 定義鍵名，好處是，方括號內可放表達式
function example2() {
    // ES5 時，物件[字面量]，只能用來添加或存取對象的屬性，不能用在對象的定義中，
    //var o = { ["name"]:123 }    // 在ES5會報錯

    // [字面量] 只能用在 object 的 添加或存取
    var o = {}
    o["name"] = 123     // 利用[字面量]添加
    console.log(o.name) // 返回 123，利用[字面量]存取

    // ES6 允許在物件宣告中，使用[字面量] 定義鍵名
    var p = {
        ["name"]: "pp",          // 可以在物件宣告中，使用[字面量] 定義鍵名
        ["a" + "bc"]: 123,       // [字面量] 可以放怎達式
        ["sayHi"]() {            // [字面量] 適用於 函數名宣告
            return ("Hi")
        }
    }
    console.log(p.name)     // 返回 pp
    console.log(p.abc)      // 返回 123
    console.log(p.sayHi())  // Hi

}

// 若該函數定義為屬性描述器的 getter/setter ，直接在函數前添加 get/set 關鍵字
function example3() {
    const obj = {
        _value: 5566,
        get foo() { return this._value },
        set foo(x) { this._value = x }
    };

    // 透過描述器取得私有屬性 _value 的值
    console.log(obj.foo)                // 返回5566 

    // 使用屬性描述器時，函數名由描述器.get.name 取得
    const descriptor = Object.getOwnPropertyDescriptor(obj, 'foo');
    console.log(descriptor.get.name)    // 返回 get foo
}

// Object.is()，同值相等
function example4() {
    console.log(Object.is(+0, -0))      // false
    console.log(Object.is(NaN, NaN))    // true
}

// Object.assign()，複製物件的屬性
function example5() {
    // @ 範例一，基本範例
    var target = { a: 1 };
    var source1 = { b: 2 };
    var source2 = { c: 3 };

    Object.assign(target, source1, source2)
    console.log(target)     // 返回 { a: 1, b: 2, c: 3 }

    // @ 範例二， 同名屬性的複製，後面的屬性會覆蓋前面的
    var target = { a: 1, b: 1 };
    var source1 = { b: 2, c: 2 };     // 覆蓋 b, b=2
    var source2 = { c: 3 };           // 覆蓋 c, c=3

    Object.assign(target, source1, source2);
    console.log(target)                 // {a:1, b:2, c:3}

    // @ 範例三，複製時，只會複製 souceObj 的可列舉屬性，私有屬性不複製

    for (let i in Object(true)) {
        console.log(i)  // 返回 空， true-object 無可列舉屬性
    }

    for (let i in Object(10)) {
        console.log(i)  // 返回 空， value-object 無可列舉屬性
    }

    for (let i in Object("abc")) {
        console.log(i)  // 返回 1 2 3 ，string-object 有可列舉屬性
    }

    // true-object 和 value-object 無可列舉屬性，因此會被略過
    var v1 = 'abc';
    var v2 = true;
    var v3 = 10;

    var obj = Object.assign({}, v1, v2, v3);
    console.log(obj);    // { "0": "a", "1": "b", "2": "c" }

    // @ 範例四，Object.assign() 是淺拷貝
    var obj1 = { a: { b: 1 } };
    var obj2 = Object.assign({}, obj1);

    obj1.a.b = 2;
    console.log(obj2.a.b) // 2

    // @ 範例五，Object.assign() 不會複製函數屬性，若該屬性是取值函數，先取值，再複製，
    var source = {
        get foo() { return 1 }
    };


    console.log(Object.assign({}, source))  // { foo: 1 }
}

// Object.getOwnPropertyDescriptors()的使用方法
function example6() {
    // @範例一，
    var obj = {
        _bar: 0,
        foo: 123,
        set bar(value) {
            this._bar = value
        }
    };

    // 取得對象的屬性描述器
    //console.log(Object.getOwnPropertyDescriptors(obj))
    //console.log(Object.getOwnPropertyDescriptors(obj, 'bar'))

    // @範例二，Object.assign() 無法拷貝get屬性或set屬性的函數，只拷貝值
    var target_issue = {}
    Object.assign(target_issue, obj);
    //console.log(Object.getOwnPropertyDescriptors(target_issue, 'bar'))

    // @範例三，解決 Object.assign() 無法拷貝get屬性或set屬性的函數的問題
    // 透過 getOwnPropertyDescriptors() 和  Object.defineProperties() 
    var target_correct = {}
    Object.defineProperties(target_correct, Object.getOwnPropertyDescriptors(obj))
    //console.log(Object.getOwnPropertyDescriptors(target_correct, "bar"))

    // @範例四， Object.getOwnPropertyDescriptors() + Object.create() 實現淺拷貝
    // Object.create(要繼承的原型對象，額外添加的屬性) 的用法，
    // https://developer.mozilla.org/zh-TW/docs/Web/JavaScript/Reference/Global_Objects/Object/create
    var clone = Object.create({}, Object.getOwnPropertyDescriptors(obj));
    console.log(clone)
}

// Object.setPrototypeOf()、Object.getPrototypeOf()、super使用
function example7() {
    // @範例一， Object.setPrototypeOf() 的使用
    var proto = {
        x: 10,
        hello() {
            console.log("hello")
        }
    };
    var obj = {
        x: 20,
        hello() {
            // 等同於 return Object.getPrototypeOf(this).hello()
            return super.hello()
        }
    };

    // 指定繼承原型 
    Object.setPrototypeOf(obj, proto);
    console.log(obj.x)  // 返回 20

    // 原型鏈是連動的，修改父類，子類會連動
    proto.y = 50
    console.log(obj.y)  // 返回 50，

    // @範例二，Object.getPrototypeOf() 的使用
    console.log(Object.getPrototypeOf(obj) === proto) //true

    // @範例三，super 的使用
    // super 只能用在對像中
    obj.hello()         // 返回 hello
}

// Object.keys()、Object.values()、Object.entries() 
// 列舉自身且可列舉的屬性，繼承屬性和不可列舉屬性會略過
function example8(){
    // 錯誤寫法
    var obj = Object.create({p:123}, {o:{value:456}})
    
    //Object.create()的第一個參數，是原型屬性，不可列舉
    //Object.create()的第二個參數，是屬性描述器，預設不可列舉
    console.log(obj)                // 返回 {}
    console.log(Object.values(obj)) // 返回 []

    // 正確寫法
    var obj = Object.create({p:123}, {o:{value:456, enumerable:true}})
    console.log(obj)                // 返回 { o: 456 }
    console.log(Object.values(obj)) // 返回 [456]

}

// ======= 調用區 ======
//example1()
//example2()
//example3()
//example4()
//example5()
//example6()
//example7()
example8()



//import "babel-polyfill"
import { readonly } from 'core-decorators';

// 在 webpack4 中使用 Decorator 語法，npm install --save-dev @babel/plugin-proposal-decorators
// https://babeljs.io/docs/en/babel-plugin-proposal-decorators
// https://stackoverflow.com/questions/48055771/how-do-i-use-and-apply-javascript-decorators

// 取消 vscode 對 Decorator 的 Warning
// https://www.jianshu.com/p/dab5ce019809


// @範例一，修飾函數不需要額外參數
console.log("==== 範例一 ====")

// step1. 建立裝飾器函數
function changeFoo(cls) {
    // 添加類的靜態屬性
    cls.foo = "foo"

    // 添加實例屬性
    cls.prototype.bar = "bar"
}

// step2. 使用裝飾器裝飾類
@changeFoo
class A { }

// @changeFoo 相當於 A = changeFoo(A) || A
//A = changeFoo(A) || A

console.log(A.foo)  // 返回 foo 

var a = new A()
console.log(a.foo)  // 返回 undefined 
console.log(a.bar)  // 返回 bar 


// @範例二，修飾函數需要額外參數
console.log("==== 範例二 ====")

function changeAge(age) {
    return function (cls) {
        // 添加類屬性
        cls.age = age
    }
}

// 利用 裝飾器添加類屬性
@changeAge(10)
class B { }

console.log(B.age)  // 返回 10 

// @範例三，類方法屬性的修飾 (透過屬性描述器)
console.log("==== 範例三 ====")

function readonly(cls, name, desc) {
    // cls, name, desc 會自動被帶入
    console.log(cls)        // 返回 C {}        ，被修飾的類
    console.log(name)       // 返回 sayHi       ，被修飾的屬姓名
    console.log(desc)       // 返回 descriptor  ，被修飾屬姓的描述器

    console.log(desc.value) // 返回 [Function: sayHi] 

    desc.value = () => console.log("hello")
    return desc
}

class C {
    @readonly
    sayHi() {
        console.log("hi")
    }
}

var c = new C()
c.sayHi()           // 返回 hello 

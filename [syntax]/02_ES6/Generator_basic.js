
// Generator 基本範例
function example1() {

    // 透過 Generator函數建立 iterator
    function* helloWorldGenerator() {
        yield 'hello';
        yield 'world';
        return 'ending';    // 迭代結束的返回值
    }

    // 取得 Generator 返回的 iterator
    var iterator = helloWorldGenerator();

    // next() 自動返回符合迭代器協議的 object
    console.log(iterator.next())    // { value: 'hello', done: false }
    console.log(iterator.next())    // { value: 'world', done: false }
    console.log(iterator.next())    // { value: 'ending', done: true }
    console.log(iterator.next())    // { value: undefined, done: true }
    console.log(iterator.next())    // { value: undefined, done: true }
}

// 直接調用迭代器時，迭代器只能迭代一次
// 透過可迭代對象間接調用迭代器時，迭代器可重複迭代，因為每次都取得新的迭代器實例
function example2() {
    // 透過 Generator函數建立 iterator
    function* helloWorldGenerator() {
        yield 'hello';
        yield 'world';
        return 'ending';    // 迭代結束的返回值
    }

    // 取得 Generator 返回的 iterator
    var iterator = helloWorldGenerator();

    console.log(iterator.next())    // { value: 'hello', done: false }
    console.log(iterator.next())    // { value: 'world', done: false }
    console.log(iterator.next())    // { value: 'ending', done: true }
    console.log(iterator.next())    // { value: undefined, done: true }
    console.log(iterator.next())    // { value: undefined, done: true }

    // 直接調用迭代器只能迭代一次，若要再迭代，需要重新取得迭代器
    //var iterator = helloWorldGenerator()     // 重新取得迭代器
    for (var i of iterator) {
        console.log(i)          // 若有重新取得迭代器，返回 h  i，否則，返回 空
    }

}

// 將 Generator 變成單純暫緩執行的函數
function example3() {
    function* lazy() {
        console.log("executed")
    }

    // get iterator
    var l = lazy()

    setTimeout(() => l.next(), 5000)

}

// 用 Generator 部屬 可迭代接口
function example4() {

    // @範例一，透過匿名Generator
    var myIterable = {}
    myIterable[Symbol.iterator] = function* () {
        yield 1;
        yield 2;
        yield 3;
    }

    console.log(...myIterable)      // 返回 1 2 3

    // @範例二，透過具名Generator
    function* gen() {
        yield "a"
        yield "b"
        yield "c"
    }

    var myIterable = {}
    myIterable[Symbol.iterator] = gen

    console.log(...myIterable)      // 返回 a b c 
}

// 透過 Generator.next(value) 改變 Generator 的行為
// step1. 執行代碼至 yield 並取值後返回，
// step1. 並在下一次進入時，將暫停的 yield 表達式，替換成 next()的輸入參數後，繼續執行
function example5() {

    function* foo(x) {
        var y = 2 * (yield (x + 1));
        var z = yield (y / 3);
        return (x + y + z);
    }

    // @解析一，
    var a = foo(5);

    // 本次執行語句 (yield (x + 1))，x = 5，因此 yield 返回值為 6
    console.log(a.next()) // Object{value:6, done:false}

    // next() 輸入參數為 undefined，因此，(yield (x + 1)) 被取代為 undefined
    // var y = 2 * undefined，y為 undefined
    // yield (y / 3) 等同於 yield (undefined / 3)，返回 NaN
    console.log(a.next()) // Object{value:NaN, done:false}

    // next() 輸入參數為 undefined，因此，yield (y / 3) 被取代為 undefined
    // var z = undefined
    // return (x + y + z) 等同於 5 + undefined + undefined。返回 NaN
    console.log(a.next()) // Object{value:NaN, done:true}


    // @解析二，
    var b = foo(5);

    // 本次執行語句 (yield (x + 1))，x = 5，因此 yield 返回值為 6
    console.log(b.next())    // { value:6, done:false }

    // next() 輸入參數為 12，(yield (x + 1)) 被取代為 12
    // var y = 2 * 12，y為 24
    // yield (y / 3) 等同於 yield (24 / 3)，返回 8
    console.log(b.next(12))     // { value:8, done:false }

    // next(13) 輸入參數為 13，因此，yield (y / 3) 被取代為 13
    // var z = 13
    // return (x + y + z) 等同於 5 + 24 + 13。返回42
    console.log(b.next(13))     // { value:42, done:true }
}

// Generator.throw(Error)，在生成器中觸發 Error
function example6() {

    // @範例一，
    var g = function* () {
        try {
            yield;
        } catch (e) {
            console.log('内部捕获', e);
        }
    };

    var i = g();

    // 要讓 .throw() 正常工作，必須要執行一次 next()，
    // 讓 Generator 內部，讓 yield 停在 try 語句的代碼塊中，
    // 下一次執行 generator.thow() 時，才會進到 catch()，
    i.next();

    // 在外部手動觸發 Generator函數中的Error
    try {
        i.throw('a');   // 被 Genertor內部捕獲
        i.throw('b');   // Generator 執行完畢，被外部捕獲
    } catch (e) {
        console.log('外部捕获', e);
    }

    /* 
        返回
        内部捕获 a
        外部捕获 b
    */

    // @範例二，全局的 throw()拋出的錯誤不會影響到遍歷器的狀態
    var gen = function* gen() {
        yield console.log('hello');
        yield console.log('world');
    }

    var g = gen();
    g.next();           // hello

    try {
        // 觸發全局的 throw()
        throw new Error();

    } catch (e) {
        g.next();       // world
    }

    // @範例三，單純來自 Generator 函數內部的錯誤，
    // 若內部沒有使用 try-catch，則該錯誤也可以在外部被捕捉
    function* foo() {
        var x = yield 3;
        var y = x.toUpperCase();    // 數字沒有 toUpperCase() 的方法，此行會觸發錯誤
        yield y;
    }

    var it = foo();

    it.next();          // 返回 { value:3, done:false }

    try {
        it.next(42);
    } catch (err) {
        console.log(err);   // 捕捉執行 it.next(42)時的錯誤
    }
}

// Generator.throw(Error)，在生成器中觸發 Error
function example7() {

    function* gen() {
        yield 1;
        yield 2;
        yield 3;
    }

    var g = gen();

    console.log(g.next())        // { value: 1, done: false }
    console.log(g.return('foo')) // { value: "foo", done: true }
    console.log(g.next())        // { value: undefined, done: true }

}

// yield* 的使用，在 Generator 中使用其他的 Generator
function example8() {

    function* foo() {
        yield 'a';
        yield 'b';
        return 'c'
    }

    function* bar() {
        yield 'x';
        
        // 透過 yield* 告訴 js 要調用另一個 Generator 
        // 類似委託的功能，解析時，可以將 function* foo() 的內容插入到此處
        yield* foo();

        // 若不使用 yield*，得到的是 iterator對象
        //yield foo()             // 返回 iterator對象的空對象
        
        // 若不使用 yield*，得到的是 iterator對象，可用 for-of 將值取出
        /* 
        for(let value of foo()){
            console.log(value)
        }
        */

        // 任何數據結構只要有Iterator接口，就可以被yield*遍歷
        //yield* [1, 2, 3]

        yield 'y';
    }

    for (let v of bar()) {
        console.log(v);         // 返回 x a b 1 2 3 y 
    }
    
}

// yield* 的使用，被委託的 Generator 內含有 retrun，
// 則 return 值 不是列舉對象，而是返回值，需要有變數承接
function example9() {

    function* foo() {
        yield 'a';
        yield 'b';
        return 'c'
    }

    function* bar() {
        yield 'x';
        yield* foo();
        yield 'y';
    }

    // 不會列舉 return 值 c
    for (let v of bar()) {
        console.log(v);         // 返回 x a b y 
    }
    
}

//example1()
//example2()
//example3()
//example4()
//example5()
//example6()
//example7()
//example8()
example9()


// 在同樣的作用域內，let 宣告過的變數，不允許重複宣告，
function example1(){
    var a = 10
    var a = 5       // var 允許重複宣告
    console.log(a)  // 5

    let b = 10
    //let b = 5     // Error
    console.log(b)  // 10

    let c = 10
    {
        let c = 1
        console.log(c)  // 1
    }
}

//  let 在每次迴圈中，循環變量都是獨立的實例，
function example2(){
    var a = []

    // 透過 var 宣告迴圈的循環變量是共享的
    for(var i=0; i<10; i++){
        a[i] = function(){
            console.log(i)  
        }
    }

    a[4]() // 10

    var b = []

    // 透過 var 宣告迴圈的循環變量是共享的
    for(let i=0; i<10; i++){
        b[i] = function(){
            console.log(i)  
        }
    }

    b[4]() // 4


}

// const 不允許重新賦值
function example3(){
    const a = {}
    a.name = "aa"
    
    console.log(a)  // { name: 'aa' }

    //a  = { name:"bb"}   // Error

}

//example1()
//example2()
example3()
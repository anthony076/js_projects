

// @範例一，繼承基本範例，透過 extends 關鍵字繼承
console.log("==== 範例一 ====")

// 父類
class base {
    constructor(name = "base") {
        this.name = name
        this.age = 30
    }

    sayhi() {
        console.log(`Hi ${this.name}`)
    }
}

// 子類繼承父類，子類必須在constructor方法中調用super方法，否則新建實例時會報錯。
//1 子類自己的this對象，必須先通過父類的構造函數，得到與父類同樣的實例屬性和方法，然後再用子類的構造函數修改
//  如果不調用super方法，子類就得不到this對象。
//2 ES5的繼承，是先創造子類的實例對象this，然後再將父類的方法添加到this上面
//  ES6的繼承，是先將父類實例對象的屬性和方法，加到this上面，再用子類的構造函數修改
class Person extends base {
    constructor(name, height) {

        // 只有調用super之後，才可以使用this關鍵字，否則會報錯。這是因為子類實例的構建，基於父類實例
        // 注意，super雖然代表了父類A的構造函數，但是返回的是子類B的實例，即super內部的this指的是B，
        // 因此super()在這里相當於A.prototype.constructor.call(this)。
        super(name)     // 取代父類中的 name 屬性     

        // 新增子類的 height 屬性
        this.height = height
    }
}

var p = new Person("An", 150)
console.log(p.name)     // 返回 An 
console.log(p.height)   // 返回 150 
p.sayhi()               // 返回 Hi An 

// 只要 class 有在原生鍊上，透過 instanceof 查找都會返回 true
console.log(p instanceof base)     // 返回 true 
console.log(p instanceof Person)   // 返回 true

// @範例二，super 的使用，
// 用法一，super()，調用父類的構造函數
// 用法二，super，指向父類的`原型對象`，用來執行父類原型對象的屬性或方法
// 注意，不是父類實例的屬性或方法
console.log("==== 範例二 ====")

// 定義父類
class A {
    constructor(x = 5, y = 5) {
        this.x = x
        this.y = y
    }

    // 在 class 中建立的方法都會放在原型鏈上
    add() {
        console.log(this.x, this.y)
    }
}

// 定義父類原型鏈的屬性
A.prototype.z = 200

// 定義子類
class B extends A {
    constructor(x, y) {
        super()
        this.y = y

        // 透過 super 會調用父類原型鏈中的屬性或方法，
        // 父類 add() 中的 this 會指向當前對象，若當前對象沒有，會沿著原型鏈網上尋找
        // 此例中，this 當前對象是 子類B，因此 this.y = 100，this.x 則是指向父類的 this.x = 5 
        super.add() // 返回 5 100 

        // 透過 super 會調用父類原型鏈中的屬性或方法，
        // 在父類中的屬性 y，是父類自身屬性，不是原型鏈的屬性，因此返回 undefined
        console.log(super.y)    // 返回 undefined
        console.log(super.z)    // 返回 200 
    }
}

var a = new A()
a.add()                 // 返回 5 5  

var b = new B(1, 100)

// @範例三，父類中同時具有同名的靜態方法和實例方法時
// super 用在子類的實例方法中，指向父類中的實例方法，
// super 用在子類的靜態方法中，指向父類中的靜態方法
console.log("==== 範例三 ====")

class C {
    constructor() {
        // this.x ，父類的實例屬性 x
        this.x = 1;
    }

    // 定義同名的靜態方法 print()
    static print() {
        // 在靜態方法中，this.x 指向父類的 `靜態屬性 x`
        console.log(this.x);
    }

    // 定義同名的實例方法 print()
    print() {
        // 在實例方法中，this.x 指向當前實例對象的 `實例屬性 x`
        console.log(this.x)
    }

    static pp() {
        console.log("i am pp")
    }
}

class D extends C {
    constructor() {
        super();
        this.x = 2;
    }

    static m() {
        // 在子類的靜態方法中，super 指向父類的靜態方法，
        // this 指向當前類的靜態屬性，若不存在，沿著原型鏈上找
        super.print();
    }

    m() {
        // 在子類的實例方法中，super 指向父類的實例方法
        super.print()
    }

    pp() {
        // super 指向父類的實例方法，父類的 pp() 是靜態方法
        //super.pp()
        C.pp()
    }
}

// 調用子類的靜態方法，
// 子類沒有定義靜態屬性x (沒有 D.x)，但有父類定義的靜態屬性x (C.x)
// 因此 this 指向父類定義的靜態屬性x (C.x)
C.x = 3
D.m()       //返回 3，

// 子類有定義靜態屬性x (D.x)，this 指向當前的類的靜態屬性
D.x = 4;
D.m()       // 返回4，調用靜態方法，

// 調用子類的實例方法，
var d = new D()
d.m()       // 返回2，this 指向當前的實例對象
d.pp()


// @範例四，class 繼承的兩條繼承鍊
console.log("==== 範例四 ====")

class E {
    constructor() {
        this.x = 1
    }
}
class F extends E {
    constructor() {
        super()
        this.x = 2
    }
}

/* 
    注意，__proto__ 代表指向父類的 XXXX ，XXXX 依照 __proto__ 前面的物件決定
    子類.__proto__ ，代表 __proto__ 指向父類的構造函數
    prototype.__proto__ ，代表 __proto__ 指向父類的原型對象
*/

// 子類.__proto__ 屬性，指向父類 (__proto__前面是類，因此等同於指向父類的構造函數)
console.log(F.__proto__)               // 返回 [Function: E]，指向父類的構造函數
console.log(F.__proto__ == E)         // 返回 true 
var f = new F.__proto__()               // 等同於 new F()，調用父類來建立實例
console.log(f.x)                        // 返回 1

// 子類.prototype 屬性，指向自身原型對象
// 子類.prototype.__proto__ 屬性，指向自身原型對象的父類 (__proto__前面是 prototype，因此等同於指向父類的原型對象)
console.log(F.prototype)                       // 返回 F {}，指向自身的原型對象
console.log(F.prototype.constructor)          // [Function: F]，指向自身原型對象的構造函數
console.log(F.prototype.constructor === F)    // 返回 true 

console.log(F.prototype.__proto__)                // 返回 E {}，指向自身原型對象的父類，即父類的原型對象
console.log(F.prototype.__proto__ === E.prototype) // 返回 true ，子類原型對象的父類 = 父類的原型對象

console.log(F.prototype.__proto__.constructor === E)  // 返回 true，子類原型對象的父類構造函數 = 父類的構造函數


// @範例五，手動實作繼承
console.log("==== 範例五 ====")

// 父類
class Parent { }

// 子類
class Son { }

/* 
    Object.setPrototypeOf() 的實作內容

    Object.setPrototypeOf = function (obj, proto) {
        obj.__proto__ = proto;
        return obj;
    }
*/

// 將父類的原型對象，指定給子類的原型對象
// 相當於 B.prototype.__proto__ = A.prototype，
Object.setPrototypeOf(Son.prototype, Parent.prototype)

// 將父類的靜態屬性(構造函數是特殊的對象，一種叫函數的對象)，指定給子類
// 相當於 B.__proto__ = A;
Object.setPrototypeOf(Son, Parent)

// 利用 class 可以繼承任何函數，包含原始類型的物件(ES5 不行)
class G extends Object { }

console.log( G.__proto__ === Object )   // 返回 true 
console.log( G.prototype.__proto__ === Object.prototype )   // 返回 true 

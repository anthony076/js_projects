
// async/await 基本範例
// async 返回的是 Promise 對象，
function example1() {

    // 耗時操作
    function timeout(ms) {
        return new Promise((resolve) => {
            setTimeout(resolve, ms);
        });
    }

    // 透過 async，將有調用耗時操作的函數定義為異步，await，標記出耗時操作的函數
    // async 相當於 function*，  await 相當於 yield
    // 被 async 包裝的函數，本質上還是 Generator，具有 Generator 的特性，只是不需要再手動調用，
    async function asyncPrint(value, ms) {
        await timeout(ms);      // 相當於透過 yield 去調用耗時函數

        // 並將 return 值，包裝成 Promise.resolve(值)後返回
        // 若沒有 return 值，則包裝成 Promise.resolve()
        return "async done"     // 相當於所有的 yield 結束後，最後返回的 value 值
    }

    // async 函數返回不再是 iterator 對象，而是 Promise 對象，因此可以使用 .then()
    var p = asyncPrint('hello world', 1000)
    p.then((v) => console.log(v))

    /* 
        返回
        hello world
        async done
    */
}

// 捕捉 async 函數中的 Error
// 同 Promise-Object 的捕捉方式
function example2() {

    async function f() {
        throw new Error("Something Wrong")
    }

    f().then(v => console.log(v))
        .catch(e => console.log(e))

}

// await 後方的返回值
function example3() {

    function timeout(ms) {
        setTimeout(console.log("timeup"), ms);
    }

    async function f() {

        // await 後方接的是一般函數，報錯
        //await timeout(1000)

        // await 後方接的是值，直接返回
        // 等同於 console.log(123)
        console.log(await 123)

        await Promise.resolve("456")

        return "done";
    }

    f().then(v => console.log(v))

    /* 
        返回
        123
        done
    */

}

// await reject/Error 的處理方式
function example4() {

    // 若其中一個 await 的 操作有 Error 或 reject，
    // 所有代碼就會停止
    async function f1() {
        await Promise.reject("f1Error")

        // 因為 前一步的 await 觸發 rejected
        // 所以以下的 code 不執行
        console.log(123)
    }

    // 解法一，將try-catch 加在 async function 中
    async function f2() {
        try {
            await Promise.reject("f2Error");
        } catch (e) {
            console.log(e)
            console.log(456)
            return "f2done"
        }
    }

    // 解法二，在 await 的 Promise 對象中，添加 .catch() 的方法
    async function f3() {
        await Promise.reject("f3Error").catch(e => console.log(e))
        console.log(789)
        return "f3done"
    }

    // 返回 f1Error
    f1().then(v => console.log(v)).catch(e => console.log(e))

    // 返回 f2Error 456 f2done
    f2().then(v => console.log(v)).catch(e => console.log(e))

    // 返回 f3Error 789 f3done
    f3().then(v => console.log(v))

}

// 同時觸發所有 await 操作的寫法
function example5() {

    // 耗時操作一
    function delayA(ms) {
        return new Promise(
            resolve => setTimeout(resolve, ms)
        ).then(console.log("delayA done"))
    }

    // 耗時操作二
    function delayB(ms) {
        return new Promise(
            resolve => setTimeout(resolve, ms)
        ).then(console.log("delayB done"))
    }

    async function ops() {
        // @範例一，非並發寫法，delayB() 會等 delayA() 完成後才執行，，共耗時2.2秒
        // delayA() 和 delayB() 有順序關係時才使用此寫法
        //await delayA(1000).then(console.log("delayA done"))
        //await delayB(1000).then(console.log("delayB done"))

        // @範例二，同時觸發的寫法(並發寫法)
        // 寫法一，使用Promise.all()，共耗時1.1秒
        await Promise.all([delayB(1000), delayA(1000)])

        // 寫法二，await 後方接耗時函數的引用，而不是直接調用耗時函數，，共耗時1.2秒
        // await 後方接函數引用值，會立刻執行，不會等待
        //let a = delayA(1000)
        //let b = delayB(1000)
        //await a, b

        return "all await done"
    }

    ops().then(v => console.log(v))
}

//example1()
//example2()
//example3()
//example4()
//example5()
example6()

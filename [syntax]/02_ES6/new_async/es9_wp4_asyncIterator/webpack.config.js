const path = require("path")

module.exports = {
    // 設定主程式位置
    entry: "./src/main.js",

    // 設定輸出位置
    output: {
        filename: "output.js",
        path: path.resolve(__dirname, "dist")
    },

    module:{
        rules:[
            {
                use: "babel-loader",
                test: /\.js$/,
                exclude: /node_modules/
            },
        ]
    },
}
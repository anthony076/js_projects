
// 注意，本範例中使用的語法，nodejs 尚不支援，需要透過 webpack 執行
// poly-fill 不要放在模組中，放在主程式中

module.exports = function () {

    console.log("==== for-await-of 的用法 ====")

    // 建立異步生成器
    async function* createAsyncIterable(arr) {
        for (var v of arr) {
            yield v
        }
    }

    // 建立異步生成器自動產生的異步迭代器
    const asyncIterable = createAsyncIterable(['a', 'b']);

    // 因為有異步操作，所以需要寫在 async 函數中
    async function f() {
        // 異步迭代器若發生錯誤，必須透過 try-catch 捕捉
        try {

            // 透過 for-await-of 自動調用異步迭代器的 next()
            for await (const i of asyncIterable) {
                console.log(i)
            }

        } catch(e) {
            console.log(e)
        }

    }

    f()

}




// 注意，本範例中使用的語法，nodejs 尚不支援，需要透過 webpack 執行
// poly-fill 不要放在模組中，放在主程式中

module.exports = function () {

    console.log("==== 建立異步 Generator 簡單範例 ====")

    // 透過 async + function* 建立異步Generator
    async function* gen() {
        yield 'hello';
    }

    // 取得異步Generator的 異步iterator 實例
    const genObj = gen();

    // 調用 異步iterator 來異步取值
    //  genObj.next() 返回的是一個 PromiseObjecct
    genObj.next().then(x => console.log(x));    // 返回 { value: 'hello', done: false }


}



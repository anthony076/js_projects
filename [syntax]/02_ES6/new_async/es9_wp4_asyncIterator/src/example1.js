
// 注意，本範例中使用的語法，nodejs 尚不支援，需要透過 webpack 執行
// poly-fill 不要放在模組中，放在主程式中

module.exports = function(){

    console.log("==== 異步生成器的寫法一 ====")
    
    // 透過 async + Generator 的語法，建立`異步生成器` 
    // async Generator 中的 yield 會將值包裝成 PromiseObject
    // 該 PromiseObject 可由 異步迭代器.next()
    async function* createAsyncIterable(arr) {
        for (var v of arr) {
            yield v
        }
    }

    // 取得 異步迭代器 的實例，
    // 調用`異步生成器`的函數 會返回 `異步生成器`
    const asyncIterable = createAsyncIterable(['a', 'b']);
    //console.log(asyncIterable)    // 返回 _AsyncGenerator { _invoke: [Function: send] }

    // 取得`異步可迭代對象`的`異步迭代器實例`
    const asyncIterator = asyncIterable[Symbol.asyncIterator]();
    //console.log(asyncIterator)    // 返回 _AsyncGenerator { _invoke: [Function: send] }

    // 手動調用異步迭代器
    asyncIterator.next()            // 異步迭代器的 next()，返回的是 PromiseObject
        .then(iterResult1 => {
            console.log(iterResult1);   // { value: 'a', done: false }
            return asyncIterator.next();
        })
        .then(iterResult2 => {
            console.log(iterResult2);   // { value: 'b', done: false }
            return asyncIterator.next();
        })
        .then(iterResult3 => {
            console.log(iterResult3);   // { value: undefined, done: true }
        });

}




// 注意，本範例中使用的語法，nodejs 尚不支援，需要透過 webpack 執行
// poly-fill 不要放在模組中，放在主程式中

module.exports = function(){

    console.log("==== 異步生成器的寫法二 ====")

    // 透過 async + Generator 的語法，建立`異步生成器` 
    // async Generator 中的 yield 會將值包裝成 PromiseObject
    // 該 PromiseObject 可由 異步迭代器.next()
    async function* createAsyncIterable(arr) {
        for (var v of arr) {
            yield v 
        }
    }

    // 透過 await 會自動調用 異步迭代器.next()返回的 PromiseObjec中的 then()
    // then() 是由 異步生成器自動產生
    async function f(){
        // 取得 異步迭代器 的實例，
        // 調用`異步生成器`的函數 會返回 `異步生成器`
        //const asyncIterable = createAsyncIterable(['a', 'b']);
        const asyncIterator = createAsyncIterable(['a', 'b']);

        // (非必要)取得`異步可迭代對象`的`異步迭代器實例`
        //const asyncIterator = asyncIterable[Symbol.asyncIterator]();

        // asyncIterator.next() 返回的是 
        console.log(await asyncIterator.next())     // 返回 { value: 'a', done: false } 
        console.log(await asyncIterator.next())     // 返回 { value: 'b', done: false }
        console.log(await asyncIterator.next())     // 返回 { value: undefined, done: true }
    }

    f()

}



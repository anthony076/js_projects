
// 使用解構時，兩邊的使用的模式要相同
function example1(){
    var a, b, c = [1, 2, 3]
    console.log(a, b, c)      // undefined undefined [ 1, 2, 3 ]

    var [a, b, c] = [1, 2, 3]
    console.log(a, b, c)      // 1 2 3  

}

// 可使用 預設值避免出現 undefined
function example2(){
    var [a, b=6, c] = [1, ,3]
    console.log(a, b, c)    // 1 6 3
}

// 預設值可以是解構變量中的其他已經聲明過的變數
// 但必須是已經聲明過的
function example3(){
    var [ x=1, y=x ] = []
    console.log(x,y)    // 1 1

    // var [ x=y, y=1 ] = []    // Error    
}

// 對可迭代對象都可以使用解構賦值
function example4(){
    let [x, y, z] = new Set(['a', 'b', 'c'])
    console.log(x, y, z)    // a b c
}

// Object 是按照鍵名做賦值解構
function example5(){
    // 建立與鍵名同名的變數(因為同名，所以鍵名省略)
    let { aa, bb, cc }= { bb:2, cc:3, aa:1 }
    console.log(aa, bb, cc)     // 1 2 3

    // 建立與鍵名不同名的變數(因為不同名，所以鍵名不可省略)
    let { bb:y, cc:z, aa:x }= { bb:22, cc:33, aa:11 }
    console.log(x, y, z)     // 11 22 33
}

// 從 非對象或非樹組(字串、數值、布林) 中進行解構賦值
// 會自動被轉換成對象或 array-like 後，再進行解構賦值
function example6(){
    // 左側是 arraly，因此右側的字串當成 array 解構賦值
    var [a, b, c] = "xyz"
    console.log(a, b, c)    // x y z

    // 左側是 object，因此右側的字串當成 object 解構賦值
    // "xyz".length = 3
    var { length:x } = "xyz"
    console.log(x)          // 3
}



// ========== 調用區 ==========
//example1()
//example2()
//example3()
//example4()
//example5()
example6()
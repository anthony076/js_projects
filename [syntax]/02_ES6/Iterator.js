
//Iterator 函數實現範例
http://es6.ruanyifeng.com/?search=%3C&x=0&y=0#docs/iterator#for---of-%E5%BE%AA%E7%8E%AF

// Iterator 調用範例，以字串為例
function example1() {
    var someString = "hi";
    typeof someString[Symbol.iterator]  // 返回 function 

    var iterator = someString[Symbol.iterator]();

    console.log(iterator.next())  // { value: "h", done: false }
    console.log(iterator.next())  // { value: "i", done: false }
    console.log(iterator.next())  // { value: undefined, done: true }
}

// 改寫 字串原生的 iterator函數
function example2() {
    var str = new String("hi");

    console.log([...str])   // 返回 ["h", "i"]

    // 重新部屬字串的 iterator 接口，
    // 讓接口指向自定義的函數
    str[Symbol.iterator] = function () {
        return {
            _first: true,
            next: function () {
                if (this._first) {
                    this._first = false;
                    return { value: "bye", done: false };
                } else {
                    return { done: true };
                }
            },
        };
    };

    console.log([...str])       // 返回 ["bye"]
    console.log(str.toString()) // 返回 "hi"
}

// 直接調用迭代器時，迭代器只能迭代一次
// 透過可迭代對象間接調用迭代器時，迭代器可重複迭代，因為每次都取得新的迭代器實例
function example3() {
    
    var someString = "hi";
    var iterator = someString[Symbol.iterator]();

    console.log(iterator.next())  // { value: "h", done: false }
    console.log(iterator.next())  // { value: "i", done: false }
    console.log(iterator.next())  // { value: undefined, done: true }

    // 直接調用迭代器只能迭代一次，若要再迭代，需要重新取得迭代器
    //var iterator = someString[Symbol.iterator]();     // 重新取得迭代器
    for(var i of iterator){
        console.log(i)          // 若有重新取得迭代器，返回 h  i，否則，返回 空
    }

    // 透過可迭代對象則沒有此問題，可迭代對象調用迭代器時，會重新取得迭代器實例
    for(var i of someString){
        console.log(i)          // 返回 h  i
    }

}

//example1()
//example2()
example3()
/*
    
    箭頭函數，簡化版的匿名函數寫法，同 py 的 lambda表達式

    使用方式
    #1，無參數，    語法，() => 單一輸出
    #2，單一輸入，  語法，引數名 => 輸出
    #3，多個輸入，  語法，(引數1, 引數2, ...) => 輸出 

    使用原則
    #1  () 在輸入端，代表無參數 或 多個參數，單一參數可省略
        () 在輸出端，代表要返回對象
    #2  輸出的 return 可省略
    #3  箭頭函數可使用嵌套

    使用限制
    箭頭函數本身，沒有外層函數的對應變量：this、arguments、super、new.target
    需要綁定對象的函數，不能使用箭頭函數，箭頭函數中的 this 無法綁定
    http://es6.ruanyifeng.com/?search=%3C&x=0&y=0#docs/function

    #1  箭頭函數體內的this對象，是綁定定義時所在的對象，而不是調用時所在的對象
    #2  不可以當作構造函數，
        構造函數需要綁定一個實例對象，而箭頭函數體內的this永遠指向定義時所在的對象
        箭頭函數的源碼中，由於沒有宣告自己的 this 的變數，因此永遠指向外層對向的 this
    #3  不可以使用yield命令，因此箭頭函數不能用作Generator函數

    其他相關主題
    http://es6.ruanyifeng.com/?search=%3C&x=0&y=0#docs/function
    #1  尾調用 和 尾調用優化
    #2  用ES6的預設值，改寫尾遞歸，和 尾遞歸優化的實現
*/


// ==== 方法一，無參數 ====
// 等同於，var a = function(){ console.log(123) }
var a = () => console.log(123)

// 因為未使用 return 因此會返回 undefined
console.log(a())    // 123 undefined，


// ==== 方法二，單一輸入 ==== 
// 等同於，var aa = function(v){ return v }
var b = v => v;

console.log(b("aa"))    // aa

// ==== 方法三，多個輸入 ==== 
// 等同於，var aa = function(v){ return v }
var c = (a, b) => a + b;

console.log(c(2, 3))    // 5

// ==== 返回對象時，需要用() 將對象包起來 ====
// 因為 代碼塊 和 對象都是使用{}表示，加上() 以供區別
var d = (e, f) => ({ e: e, f: f })

console.log(d(3, 4))     // {e:3, f:4}

// ==== 箭頭函數的this 指向定義時的對象 ====

// 使用一般函數時，this 指向的是調用對象，
// 此處調用對象是 setTimeout()的對象，因此返回 undefined
function foo() {
    setTimeout(function () {
        console.log("id:", this.id)
    }, 100)
}

foo.call({ id: 42 });   // undefined

// 使用箭頭函數時，this 指向的是調用對象，
// 此處透過 .call() 指向 this 的對象是 { id: 42 }
function bar() {
    setTimeout(() => {
        console.log("id:", this.id)
    }, 100)
}
bar.call({ id: 42 });   // 42

// ==== 對箭頭函數使用嵌套 ====
// 請參考，http://es6.ruanyifeng.com/?search=%3C&x=0&y=0#docs/functio，
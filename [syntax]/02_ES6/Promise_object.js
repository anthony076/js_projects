
/*異步操作的基本概念，異步操作包含兩個函數，
    1_真正要執行操作的函數(該函數屬於耗時的操作)
    2_耗時的操作完成後，等待系統調用的回調函數 (回調函數包含耗時操作的結果)

    Step1 的函數會立即執行
    Step2 的函數等待系統調用
*/

// 基本使用範例，需要輸入參數的寫法
function example1() {

    // ==== Step1. 定義耗時操作的函數，並將該操作定義在 Promise 對象中 ====
    // new Promise(function))，其中，function 為 (resolve, reject) => 耗時操作
    function timeout(ms) {
        return new Promise((resolve, reject) => {
            // 實際要進行的耗時操作
            setTimeout(resolve, ms, "done");
        });
    }

    // ==== Step2. 定義耗時操作完成後的回調函數 ====
    // Promise實例生成以後，可以用then方法分別指定resolved狀態和rejected狀態的回調函數。
    // 語法，PromiseObject.then(resolved的回調函數, rejected的回調函數)，
    // 注意，耗時函數 timeout()必須返回 PromiseObject才會進行回調，.then()才會有效果
    timeout(3000).then(
        (value) => { console.log(value) },  // 定義 resolved 狀態的回調函數
        (error) => { console.log(error) }   // 定義 rejected 狀態的回調函數  
    )

}

// 基本使用範例，不需要輸入參數的寫法
function example2() {

    // Step1. 定義耗時操作，將耗時操作定義在 PromiseObject 的構造函數中
    var timeout = new Promise((resolve, reject) =>
        setTimeout(resolve, 3000, "done")
    )

    // Step2. 定義 Step1 中， PromiseObject 的回調函數
    timeout.then(
        (value) => console.log(value),  // 定義 resolved 狀態的回調函數
        (error) => console.log(error),  // 定義 rejected 狀態的回調函數
    )
}

// Promise 的執行順序，
// 構造函數中的 耗時操作先執行，並在構造函數中調用 resolve()或reject()改變 PromiseObject的狀態，
// 耗時操作執行完畢後，才會去執行回調函數
function example3() {

    let promise = new Promise(function (resolve, reject) {
        // Step1. 立即執行耗時操作

        console.log('Promise before');

        resolve();  // 改變 Promise 的狀態，將 pending 狀態改成 resolved，不是調用回調函數

        console.log('Promise after');
    });

    // Step3. 主線程結束後才調用回調函數
    promise.then(function () {
        console.log('resolved.');
    });

    // Step2. 異步操作不阻塞主線程
    console.log('Hi!');

}

// 在構造函數中的 resolve() 或 reject() 中添加參數，則該參數會被帶進 then()的回調函數中
function example4() {
    var p = new Promise(
        (resolve, reject) => {
            resolve(1);         // 改變 PromiseObject的狀態
            console.log(2);
        })

    p.then(r => { console.log(r) });

    // 印出 2 1
}

// Promise.then() 的鍊式使用
function example5() {
    var p = new Promise(
        (resolve, reject) => resolve("done")
    )

    p.then(function (value) {
        console.log(value)
        return "123"
    }).then(value => console.log(value))

    // 返回 done 123
}

// Promise.catch() 的使用範例一，錯誤發生在 PromiseObject的耗時操作
// PromiseObject 或 PromiseObject 相依的操作，發生錯誤都會被 catch 捕捉
function example6() {

    // 建立耗時操作 p1
    var p1 = new Promise(
        (resolve, reject) => reject(new Error('p1 Error'))
    );

    // 建立耗時操作 p2
    var p2 = new Promise(
        // p2 相依於 p1 的狀態
        //(resolve, reject) => resolve(p1)    // 顯示 p1 Error

        (resolve, reject) => reject(new Error('p2 Error'))    // 顯示 p2 Error
    );

    // 建立耗時操作的回調函數
    p2.then(value => console.log(value))
        .catch(error => console.log(error))
}

// Promise.catch() 的使用範例二，錯誤發生在 .then() 的回調函數中
// 注意，不能將異常寫在resolved 或 rejected 的回調函數中，會被回調函數吃掉
function example7() {

    // 建立耗時操作 p1
    var p1 = new Promise(
        (resolve, reject) => console.log("in Promise")
    );

    // 錯誤寫法，異常不能寫在 resolved 或 rejected 的回調函數中，因為回調函數成功執行，
    //p1.then(() => console.log(123)).catch( error => console.log(error) )  // 不會拋出異常

    // 正確寫法，要在 then 中拋出異常，不能將異常寫在 resolved 或 rejected 的回調函數中
    // 若異常發生在 resolved 或 rejected 的回調函數裡，只會更改 status，不會把異常拋出
    //p1.then(Promise.reject(new Error('then error')))   // 注意，Error 不能寫在 resolve 或 reject 的回調函數裡
    //.catch( error => console.log(error) )  

    // 正確寫法，.catch() 會捕捉 .catch 之前發生的異常
    p1.then(console.log(123))
        .then(Promise.reject(new Error('then error')))   // 注意，Error 不能寫在 resolve 或 reject 的回調函數裡
        .catch(error => console.log(error))
}

// Promise.all() 的使用，all resolved 和 one reject 的狀況，
function example8() {

    var p1 = Promise.resolve(3);

    var p2 = 1337;
    // 手動觸發 reject
    //var p2 = Promise.reject(" manual reject");

    var p3 = new Promise((resolve, reject) => {
        setTimeout(resolve, 100, 'foo');
    });

    Promise.all([p1, p2, p3])
        .then(values => { console.log(values) })
        .catch(error => console.log(error));

    // 返回 [ 3, 1337, 'foo' ] 或  manual reject
}

// Promise.all() 的使用，Promise 實例的成員，自定義自己的 .catch() 和 .then() 的狀況
// 自定義 .then()，會將返回值傳遞給自己的 .then()，不會傳遞給 Promise.all() 的 .then()
// 自定義 .catch()，會調用自己的 .catch()，不會調用 Promise.all() 的 .catch()
function example9() {

    // p1 Promise 定義自己的 .then()
    var p1 = new Promise((resolve, reject) => resolve("p1 ok"))
        .then(result => console.log("i am p1"))

    var p2 = new Promise((resolve, reject) => resolve("p2 ok"))

    // p3 Promise 定義自己的 .catch()
    var p3 = new Promise((resolve, reject) => reject("Error:"))
        .catch(e => console.log(e + " from p3"))

    // p4 Promise 會調用 Promise.all() 的 .catch()
    var p4 = new Promise((resolve, reject) => reject("Error:"))

    Promise.all([p1, p2, p3, p4])
        .then(result => console.log(result))
        .catch(e => console.log(e + " from .all()"))

    /* 返回
    i am p1
    Error: from p3
    Error: from .all()
    */
}

// Promise.race() 的使用，
// p 的狀態為 p1、p2 中狀態先改變者
function example10() {
    var p1 = new Promise(
        //(resolve, reject) => setTimeout(() => resolve("p1 timeup"), 7000)
        (resolve, reject) => setTimeout(() => resolve("p1 timeup"), 3000)
    )

    var p2 = new Promise(
        (resolve, reject) => setTimeout(() => resolve("p2 timeup"), 5000)
    )

    var p = Promise.race([p1, p2])
        .then(result => console.log(result))
        .catch(e => console.log(e))
}

// Promise.resolve() + thenable 的用法
function example11(){
    let thenable = {
        then: function (resolve, reject) {
            resolve(42)
            //reject("Error");
        }
    };
    
    let p1 = Promise.resolve(thenable);
    
    p1
        .then(value => console.log(value))  // 返回 42 
        .catch(e => console.log(e))         // 返回 Error 
}

// Promise.reject() + thenable 的用法
function example12(){
    let thenable = {
        then: function (resolve, reject) {
            reject("Error");
        }
    };
    
    let p1 = Promise.reject(thenable);
    
    p1.catch(e => console.log(e))         // 返回 { then: [Function: then] }
}

//example1()
//example2()
//example3()
//example4()
//example5()
//example6()
//example7()
//example8()
//example9()
//example10()
//example11()
example12()




function cout(result){
    console.log(result)
}

// 新增 N進制的寫法
function example1() {
    //2進制寫法
    console.log(0b0010) // 2
    console.log(0B0010) // 2

    //8進制寫法
    console.log(0o0010) // 8
    console.log(0O0010) // 8

    //16進制寫法
    console.log(0x0010) // 16
    console.log(0X0010) // 16   
}

//  新增數值方法
//      Number.isFinite()，檢查一個數值是否為有限的（finite）
//      Number.isNaN()，檢查一個值是否為NaN
//      Number.parseInt()，將數值轉為 Int
//      Number.parseFloat()，將數值轉為 Float
//      Number.isInteger()，判斷一個數值是否為整數
function example2(){
    // 傳統方法先調用Number()將非數值的值轉為數值，再進行判斷，
    // 而這兩個新方法只對數值有效，不轉換

    // ==== .isNaN() 使用範例 ====
    // 不是數值，一律返回 false

    // issue
    cout(isFinite(25))    // true
    cout(isFinite("25"))  // true，字串不應該進行判斷
    
    // ES6 solution
    cout(Number.isFinite(0.8))       // true
    cout(Number.isFinite(NaN))       // false
    cout(Number.isFinite(Infinity))  // false
    cout(Number.isFinite('foo'))     // false

    // ==== .isNaN() 使用範例 ====
    // 不是NaN，一律返回 false
    // issue
    cout(isNaN(NaN))      // true
    cout(isNaN("NaN"))    // true，，字串不應該進行判斷

    // ES6 solution
    cout(Number.isNaN(NaN))     // true
    cout(Number.isNaN(15))      // false
    cout(Number.isNaN('15'))    // false

    // ==== Number.isInteger() 使用範例 ====
    // 注意精度，js 採用 IEEE 754，對多精度只到53位，第54位以後被拋棄
    cout(Number.isInteger(3.0000000000000002))   // true
    
    //小於 js 能夠分辨的最小值，會被自動轉為 0
    cout(Number.isInteger(5E-325))   // true
}

// Number.EPSILON 誤差常量，
// 用來解決浮點數值運算不精確的問題
function example3(){
    // issue
    // 與其他語言一樣，浮點數計算是不精確的
    cout( 0.1 + 0.2 == 0.3)     // false
    cout( 0.1 + 0.2 )           // 0.30000000000000004

    // ES6 solution
    // Number.EPSILON 定義為 能夠接受的誤差範圍，
    // 若兩個浮點數的差小於這個值，我們就認為這兩個浮點數相等
    function equal (left, right) {
        cout(Number.EPSILON)    // 2.220446049250313e-16

        // 利用 Number.EPSILON 設置可接受最小誤差
        return Math.abs(left - right) < Number.EPSILON * Math.pow(2, 2);
      }

    cout(equal(0.1 + 0.2, 0.3)) // true
}

// Number.MAX_SAFE_INTEGER / Number.MIN_SAFE_INTEGER 整數精度上下限
// Number.isSafeInteger()，判斷 數值是否落在整數精度上下限之間
function example4(){
    // issue
    // js 能夠準確表示的整數範圍在-2^53到2^53之間
    cout(Math.pow(2, 53) === Math.pow(2, 53)+1)   // true

    // ES6 Solution
    // Number.MAX_SAFE_INTEGER 代表 整數精度上限
    // Number.MAX_SAFE_INTEGER 代表 整數精度下限
    cout(Number.MAX_SAFE_INTEGER === Math.pow(2, 53)-1)     // true
    cout(Number.MIN_SAFE_INTEGER === -Math.pow(2, 53)+1)    // true

    cout(Number.isSafeInteger(Math.pow(2,53)))  // false
    cout(Number.isSafeInteger(Math.pow(2,53)-1))  // true

}

// Math 的新增方法
function example5(){
    // Math.trunc()，去除一個數的小數部分，返回整數部，
    cout(Math.trunc(1.234))     // 1
    cout(Math.trunc('1.234'))   // 1，字面量會先轉成數字
    cout(Math.trunc(true))      // 1
    cout(Math.trunc('foo'))     // NaN，無法轉換的會返回 NaN

    // Math.sign()，判斷一個數到底是正數、負數、還是零
    // 正數，返回+1，負數，返回-1，其他值，返回NaN
    cout(Math.sign(-5))         // -1
    cout(Math.sign(5))          // 1
    cout(Math.sign(0))          // 0
    cout(Math.sign(-0))         // -0
    cout(Math.sign("123"))      // 1
    cout(Math.sign("foo"))      // NaN

    // Math.cbrt()，用於計算一個數的立方根
    cout(Math.cbrt(8))        // 2
    cout(Math.cbrt('8'))        // 2
    
    // Math.clz32()，js 的整數使用32位二進制形式表示，
    // 用來計算數值換成二進制後，前導0 的個數
    cout(Math.clz32(0)) // 32

    // Math.imul，返回正確的低位數值用
    // 對於那些很大的數的乘法，低位數值往往都是不精確的，Math.imul方法可以返回正確的低位數值。

    // Math.fround，是將64位雙精度浮點數轉為32位單精度浮點數。
    // 如果小數的精度超過24個二進制位，返回值就會不同於原值，否則返回值不變（即與64位雙精度值一致）。

    // Math.hypot，返回所有參數的平方和的平方根
    cout(Math.hypot(3, 4)) // 5

    // 對數方法
    // Math.expm1(x) 等同於 Math.exp(x) - 1
    // Math.log1p(x) 等同於 Math.log(1 + x)
    // Math.log10(x)
    // Math.log2(x)
    // Math.sinh(x)
    // Math.cosh(x)
    // Math.tanh(x)
    // Math.asinh(x)
    // Math.acosh(x)
    // Math.atanh(x)
}

// ES6 新增指數運算符 ** 
// 注意，js 中的指數是右結合，從右邊開始計算
function example6(){
    cout(2**2)  // 4
    cout(2**2**3)  // 81，2 ** (2**3) = 2 ** 8 = 81
    
}



// ========== 調用區 ==========
//example1()
//example2()
//example3()
//example4()
//example5()
example6()
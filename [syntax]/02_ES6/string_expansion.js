/*
    字串編碼的 bug，字串.length 是取字節長度，不是取字符長度，一個字符可能是由多個字節組成
        透過字串的 .length 屬性取字串長度時，js 會將大於\uFFFF的 Unicode 字符，算作兩個字節，實際上可能是一個字符

    問題的原因
        js 的字符以 utf-16 儲存，1字符=2字節=4位元，js 字碼只支援 utf-16 表示法，超過四位元會被誤判
        
    解決方式，
        透過 ... 擴展運算符取字串長度
        透過 Array.from()取字串長度
        透過 for-of 列舉
        
*/

// 透過 \u{ 字碼 } 可避免只支援四位元字碼的限制
function example1() {
    // Issue，
    //      js 的字符以 utf-16 儲存，1字符=2字節=4位元
    //      js 字碼只支援 utf-16 表示法，超過四位元會被誤判
    //      例如，\u20BB7 正確應顯示為 𠮷
    console.log("\u20BB7")          // ₻7，因為，被轉換為 "\u20BB" + "7" 
    // 必須轉成 utf-16 的四字節表示法才能正確顯示
    console.log("\uD842\uDFB7")     // 𠮷，\u20887 = \uD842\uDFB7(utf-16表示法)

    // ES6 Solution
    // 透過 \u{ 字碼 } 可避免四位元的限制
    console.log("\u{20BB7}")    // 𠮷
}

// 改用 charCodeAt() 取得字串的編碼 (字串 -> 編碼)
function example2() {
    // 𠮷，\u20887 = \uD842\uDFB7(utf-16 表示法)
    var s = "𠮷"

    // js issue，
    //      對於超過 兩字節的字串，無法正確顯示 unicode 編碼
    //      字串長度 2 ，因為 被轉換為 "\u20BB" + "7"，參考 example1()
    console.log(s.length)                       // 2，代表字串超過兩個字節 
    console.log(s.charAt(0).toString(16))       // 亂碼，顯示第一個字節
    console.log(s.charAt(1).toString(16))       // 亂碼，顯示第二個字節

    // ES6 Solution
    // 透過 charCodeAt() 取得正確的字串的編碼
    console.log(s.charCodeAt(0).toString(16))   // d842
    console.log(s.charCodeAt(1).toString(16))   // dfb7

}

// 改用 String.fromCodePoint() 從編碼取得字串
function example3() {
    // issue，
    //      fromCharCode()無法正確識別超過 4位元 的 unicode 編碼
    console.log(String.fromCharCode("0x20BB7"))     // ஷ

    // ES6 solution，改用 String.fromCodePoint() 
    console.log(String.fromCodePoint("0x20BB7"))    // 𠮷
}

// 利用 for-of 或 擴展運算符 (...)，才能正確列舉字符的雙字節
// 取得字串正確長度的方式
function example4() {
    // 𠮷，\u20887 = \uD842\uDFB7(utf-16 表示法)
    let text = String.fromCodePoint(0x20BB7);

    // 無法列舉字串
    for (let i = 0; i < text.length; i++) {
        console.log(text[i]);   // 亂碼 亂碼
    }

    // 正確列舉字串
    for (let i of text) {
        console.log(i);         // 𠮷

    }

    // 用 ...擴展運算符 取得正確字串長度
    console.log("\u{D83D}\u{DE80}")             // 火箭圖標
    console.log("\uD83D\uDE80".length)          // 返回 2，正確應為 1，火箭圖標算一個字符，由兩個字節組成的
    console.log([..."\uD83D\uDE80"].length)     // 返回 1，透過 ... 擴展運算符，正確解析字串長度

    // 用 Array.from() 取得正確字串長度
    arr = Array.from("\u{D83D}\u{DE80}")
    console.log(arr.length)                     // 返回 1，用 Array.from() 取得正確字串長度
}

// ES6 新增的字串處理方法
//      includes()，    判斷字串頭部
//      startsWith()，  字串中是否包含特定字串
//      endsWith()，    判斷字串尾部
//      repeat()        重複字串
//      padStart()      字串長度不足，頭部自動補全
//      padE()          字串長度不足，尾部自動補全
function example5() {
    let s = "Hello, Anthony"

    console.log(s.includes("An"))    // true
    console.log(s.startsWith("He"))  // true
    console.log(s.endsWith("ny"))    // true

    let tmp = "an"
    console.log(tmp.repeat(3))  // ananan

    console.log(" test".padStart(10, "="))
    console.log(" test".padEnd(10, "="))
}

// 模板字符串
//      #1 透過 `` 指定為模板，可於普通字串或多行字串
//      #2 在字串模板中，使用 ${ js code } 來執行 js 的語法
//         使用 ${ 變數名 }，
//         使用 ${ 表達式 } 會進行求值，
//         使用 ${ 函數名() } 會插入函數返回值，
//      #3 空格和縮進都會保留
function example6() {
    let name = "An", age = 30, height = 170
    temp = `Hello ${name}, 
        your age is ${age}, and ${height} tall}`
    console.log(temp)   // Hello An, your age is 30, and 170 tall

    let x = 1, y = 5
    console.log(`${x} +  ${y} =  ${x + y}`)
}

// 模板標籤(模板函數) 的使用
//      語法，標籤名 + 模板，例如 tag``
//
//  #1  標籤名 = 函數名，模板 = 函數參數
//      模板標籤其實不是模板，而是函數調用的一種特殊形式。
//      “標籤”指的就是函數，緊跟在後面的模板字符串就是它的參數
//
//  #2  模板標籤(模板函數) 的參數轉化原則，
//
//      模板標籤是函數，模板中的文字會被轉化為函數的參數後，再進行調用
//
//      2-1，將不含變數的純字串歸為一類，放在一個 array 中，當成第一參數
//      2-2，對變量進行求值，並 `依序` 放在第一參數後
//      2-3，變量求值只會發生在兩個純文字之間，
//           因此，若變量求值在最後，會再 array 中自動補上空字串
//      
function example7() {

    function tag(s, v1, v2) {
        console.log(s[0]);      // Hello 
        console.log(s[1]);      //  world
        console.log(s[2]);      // ""
        console.log(v1);        // 15
        console.log(v2);        // 50

        return "OK";
    }

    let a = 5;
    let b = 10;

    // 模板字符串前面有一個標識名tag，它是一個函數
    tag`Hello ${a + b} world ${a * b}`;

    // 等同于
    //tag(['Hello ', ' world ', ''], 15, 50);

    // 一般函數也可以寫成模板標籤的形式
    // 模板 `123` 轉化為標籤函數的參數為 ['123']
    console.log`123`                            // [ '123' ]，此處 console.log 當成是標籤名，後方的模板是 console.log() 的參數
    console.log`Hello ${1 + 1} world ${2 + 2}`  // [ 'Hello ', ' world ', '' ] 2 4

}

// String.raw``，將模板中的變數和特殊符號進行轉譯，轉譯成純字串
// 作為處理模板字符串的基本方法，它會將所有變量替換，而且對斜杠進行轉義
function example8(){
    console.log(String.raw`Hi\n${2+3}!`)    // Hi\n5!
}


// ========== 調用區 ==========
//example1()
//example2()
//example3()
example4()
//example5()
//example6()
//example7()
//example8()
/*
    Arrary 的新增功能
    #1 ... 擴展運算符 ，把 array 中的元素 展開
    #2 Array.from() ，將`可迭代對象`轉為真正的 Array
        2-1 將以下兩類對象轉為真正的數組，
            a. 類似數組的對象（array-like object）
            b. 可遍歷（iterable）的對象（包括 ES6 新增的數據結構 Set 和 Map）
        2-2 第二個參數，內建 map 的功能
        2-3 第三個參數，用來綁定 this 的對象
    
    #3  將對象轉為數組的兩種方式
        透過 ... 擴展運算符，利用 Iterator 的 Symbol.iterator 接口
        透過 Array.from()，利用 object 的 length 屬性

    #4  Array.of()，用於將`一組值`轉換為 Array
        用來取代 Array() 或 new Array() 創建數組時，因參數不同造成返回行為不一致的問題

    #5  Array 實例的方法 
        - copyWithin())，           Array 內的元素複製與取代
        - find()和findIndex() ，    篩選 Array 中的元素
        - fill()，                  填充/初始化 Array，會覆蓋原有元素
        - entries()/keys()/values() 返回 鍵值/鍵/值 的 Iterator
        - includes()                檢查 Array 中是否包含某個元素
        - flat()/flatMap() 

    #6 Array 的空位
        - 空位 =\= undefined，undefined 是值的一種，但 空位不是值
        - 避免在 ES5 使用空位，因為 ES5 對空位的處理不一致
            - forEach（），filter（），reduce（），every（）和some（）都會跳過空位
            - map()會跳過空位，但會保留這個值
            - join()和toString()會將空位視為undefined，而undefined和null會被處理成空字符串
        - ES6 明確將空位轉為undefined
        
        http://es6.ruanyifeng.com/?search=%3C&x=0&y=0#docs/array#%E6%89%A9%E5%B1%95%E8%BF%90%E7%AE%97%E7%AC%A6
*/

// ==== ... 擴展運算符的使用 =====
// @ 範例一 
console.log(...[1, 2, 3])   // 1 2 3


// @ 範例二 
function add(x, y) {
    return x + y;
}

const numbers = [4, 38];
console.log(add(...numbers)) // 42


// @ 範例三，將一個數組添加到另一個數組的尾部 
let arr1 = [0, 1, 2];
let arr2 = [3, 4, 5];

arr1.push(...arr2);     // 等同於 arr1.push( 3, 4, 5)
console.log(arr1)       // [ 0, 1, 2, 3, 4, 5 ]


// @ 範例四，可用來複製數組，真實複製元素，而不是複製指針 
const a1 = [1, 2];

// 寫法一
const a2 = [...a1];

// 寫法二
//const [...a2] = a1;

console.log(a2)     // [1, 2]

// @ 範例五，合併數組
const ar1 = ['a', 'b'];
const ar2 = ['c'];
const ar3 = ['d', 'e'];

console.log([...ar1, ...ar2, ...ar3])   // [ 'a', 'b', 'c', 'd', 'e' ]

// @ 範例六，用於解構賦值
const [first, ...rest] = [1, 2, 3, 4, 5]
console.log(first)  // 1
console.log(rest)   // [2,3,4,5]

// @ 範例七，用於函數
function foo(first, ...rest) {
    console.log(first)  // 1
    console.log(rest)   // [2,3,4,5]
}

foo(1, 2, 3, 4, 5)

// @ 範例八，可正確識別多字節的字
console.log("\u{D83D}\u{DE80}")         // 火箭圖標
console.log("x\uD83D\uDE80y".length)    //4，正確應為 3，火箭圖標算一個字符，由兩個字節組成的

console.log([..."x\uD83D\uDE80y"].length)   // 3，正確解析

// @ 範例九，任何實現 Iterator 接口的對象，都可以透過 ... 擴展運算符轉為真正的 Array
//1     例如 Array、String (內建Iterator)、Map object、Set object、Generator objct
//      ... 擴展運算符轉成真正的 Arrya，靠的是調用 Iterator 的 遍歷器接口（Symbol.iterator），如果一個對像沒有部署這個接口，就無法轉換

//2      透過 ... 擴展運算符將 Map/Set object 轉為 Array 的範例，請參考  
//      http://es6.ruanyifeng.com/?search=%3C&x=0&y=0#docs/array#%E6%89%A9%E5%B1%95%E8%BF%90%E7%AE%97%E7%AC%A6

//3     對於那些沒有部署 Iterator 接口的類似數組的對象(array-like object)，擴展運算符就無法將其轉為真正的數組
//      但可以透過 Array.from()，將 array-like object 轉為真正的數組，Array.from() 可以將任何有length屬性的對象，都可以通過Array.from方法轉為數組

var someString = new String('hi');

// 實現迭代器，字串內建迭代器
console.log(someString[Symbol.iterator])        //[Function: [Symbol.iterator]]

// 重新定義迭代器內容
someString[Symbol.iterator] = function () {
    return { 
        // 在迭代器中實現 next()，迭代時會自動調用迭代器的 next()
        next: function () {
            if (this._first) {
                this._first = false;
                return { value: 'bye', done: false };
            } else {
                return { done: true };
            }
        },
        _first: true
    };
};

// 將具有迭代器功能的字串，轉為Array
console.log([...someString])

// ==== Array.from() 的使用 =====
// @ 範例一，基本使用
//1     任何有length屬性的對象，都可以通過Array.from方法轉為數組
//2     Array.from的第二個參數，用來對每個元素進行處理，將處理後的值放入返回的數組 (同 map)
let arrayLike = {
    0:'a',
    1:"b",
    2:"c",
    length:5
}

let arr = Array.from(arrayLike)
console.log(arr.length)         // 5
console.log(arr)                // [ 'a', 'b', 'c', undefined, undefined ]

// @ 範例二，第二個參數，內建 map 的功能
console.log(Array.from([1,2,3], x => x**2))     // [1,4,9]

// @ 範例三，第三個參數，綁定 map function 中 this 的對象
console.log( 
    Array.from(
        [1, 2, 3],                          // 要轉成 Array 的對象 
        function(x){ return x+this.n },     // map function
        {n:5}                               // 指定 map function 中 this 的綁定對象 
    )                                       // [6, 7, 8]
    
    //若要使用第三參數，不能使用箭頭函數，因為箭頭函數沒有自己的 this ，無法綁定
    //Array.from([1, 2, 3], x => x+this.n, {n:5})     // [ NaN, NaN, NaN ]，
)


console.log("\u{D83D}\u{DE80}")             // 火箭圖標
console.log("\uD83D\uDE80".length)          // 返回 2，正確應為 1，火箭圖標算一個字符，由兩個字節組成的
console.log([..."\uD83D\uDE80"].length)     // 返回 1，透過 ... 擴展運算符，正確解析字串長度

// @ 範例四，用 Array.from() 取得正確字串長度
// issue，字串的.length 屬性是取字節的長度，不是字串的長度
console.log("\u{D83D}\u{DE80}".length)      // 返回 2，正確應為1，\u{D83D}\u{DE80} 是一個火箭圖標，由兩個字節組成

arr = Array.from("\u{D83D}\u{DE80}")
console.log(arr.length)                     // 返回 1，用 Array.from() 取得正確字串長度

// ==== Array.of() 的使用 =====
// Array.of方法用於將一組值，轉換為數組，
// 用來取代 Array() 或 new Array() 創建數組時，因參數不同造成返回行為不一致的問題
// @ 範例一
// Issue : Array() 或 new Array() 創建數組時，因參數不同造成返回行為不一致
console.log(Array(3))           // [ , , ]，預期應該是 [3]
console.log(Array(1,2,3))       // [1, 2, 3]

console.log(Array.of(3))        // [3]
console.log(Array.of(1,2,3))    // [1, 2, 3]


// ==== Array實例方法: copyWithin(target, start, end) =====
// 將 start 到 end 的元素複製後，取代位於 target 的元素

// 複製元素3到元素5 (4, 5)，並取代元素 0
console.log([1, 2, 3, 4, 5].copyWithin(0, 3, 5))   // 返回 [ 4, 5, 3, 4, 5 ]
// 複製元素3到元素4 (4)，並取代元素 0
console.log([1, 2, 3, 4, 5].copyWithin(0, 3, 4))   // 返回 [ 4, 2, 3, 4, 5 ]

// ==== Array實例方法: find()和 findIndex() =====
// Array.find()，     成員篩選，利用回調函數，找出第一個符合條件的數組成員
// Array.findIndex()，成員篩選，利用回調函數，找出第一個符合條件的數組成員的 Index
// 第二個參數用來綁定 回調函數中的 this，若要使用第二個參數，不可用箭頭函數
// 此方法用來解決 Array.indexOf 無法識別 NaN 的問題
console.log([1, 4, -5, 10].find((n) => n < 0))       // -5
console.log([1, 4, -5, 10].findIndex((n) => n < 0))  // 2

// ==== Array實例方法: fill() =====
//fill(value, start, end))，填充/初始化 Array，會覆蓋原有元素
console.log(['a', 'b', 'c'].fill(6))        // [6,6,6]
console.log(['a', 'b', 'c'].fill(6,1,2))    // ['a',6,'c']

// ==== Array實例方法: entries() keys() values()=====
// Array.entries()，  返回 Array 中 所有元素鍵值的 Iterator，類似py迴圈中的 enermerate() 
// Array.keys() ，    返回 Array 中 所有元素鍵名的 Iterator，
// Array.values()     返回 Array 中 所有元素值的 Iterator，

var arr3 = ["a", "b", "c"]

// .entries() 的使用
for (let [index, elem] of arr3.entries()) {
    console.log(index, elem);   // 0 'a' 1 'b' 2 'c'
}

// .keys() 的使用
for (let key of arr3.keys()) {
    console.log(key);   // 0 1 2
}

// .values() 的使用
//console.log([1,2,3].values())     // nodejs v8.12.0 不支援，chrome69 支援

// ==== Array實例方法: includes() =====
// Array.includes(value, 搜尋起始位置)，用來檢查 Array 中是否包含某個元素
// 用來取代 indexOf() 對 NaN 的誤判
// Issue
console.log([NaN].indexOf(NaN))     // 返回 -1
// ES6 Solution
console.log([NaN].includes(NaN))     // 返回 true

// ==== Array實例方法: flat()和flatMap() =====
// Array.flat(層數)，       Array扁平化，將多維 Array 轉為 一維 Array，會改變原始 Array
// Array.flatMap(map函數)  將 Array 元素經過 Map 後，在扁平化，只能扁平一層

//nodejs v8.12.0 不支援，chorme69 支援
//console.log([1, 2, [3, 4, [5]]].flat(1))        // 返回 [1,2,3,4,[5]]，
//console.log([1, 2, [3, 4, [5]]].flat(Infinity)) // 返回 [1,2,3,4,5]，

//console.log([1, 2, 3].flatMap(n => n*n))         // 返回 [1, 4, 9]
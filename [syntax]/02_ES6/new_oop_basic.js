
// @範例一，簡單範例
console.log("==== 範例一 ====")

class Point {       // 類名指向構造函數
    // 構造函數
    constructor(x, y) {
        // 添加實例新屬性
        this.x = x;
        this.y = y;
    }

    // 在class中建立方法成員時，不需要透過 function 語句，方法之間不需要逗號分隔，加了會報錯。
    // class 中建立的方法成員，會放在原生鍊中，且是不可列舉的
    toString() {
        return '(' + this.x + ', ' + this.y + ')';
    }

    // 可使用 Symbol 表達式
    ["say" + "hi"]() {
        return "Hello"
    }
}

// Point 相當於是構造函數
p = new Point(10, 20)

console.log(p.x, p.y)       // 返回  10 20
console.log(p.toString())   // 返回  (10, 20)
console.log(Point === Point.prototype.constructor)  // 返回 true

console.log(Point.prototype)                // 返回 Point {} 
console.log(Point.prototype.constructor)    // 返回 [Function: Point]

// 利用原型鏈添加 add()
Point.prototype.add = (x, y) => x + y

// 檢查方法存在自身屬性或原生鍊
console.log(p.hasOwnProperty("toString"))   // 返回 false, class 建立的方法都會放在原生鍊中，不會在自身屬性中 
console.log("toString" in p)                // 返回 true，實例p 擁有 toString方法 (在原生鍊中)
console.log("add" in p)                     // 返回 true，實例p 擁有 add方法 (在原生鍊中) 

// 檢查列舉性                                // class 中建立的方法，會放在原生鍊中，且是不可列舉的
// class 中建立的屬性，會放在自身屬性中，且是可列舉的
console.log(Object.keys(p))                                 // 返回  [ 'x', 'y' ]，只檢查可列舉的的自身屬性
console.log(Object.getOwnPropertyNames(p))                  // 返回  [ 'x', 'y' ]，檢查可列舉+不可列舉的自身屬性
console.log(Object.getOwnPropertyNames(Point.prototype))    // 返回  [ 'constructor', 'toString', 'sayhi', 'add' ]

// 調用利用原型鏈添加的方法
console.log(p.add(1, 2))   // 返回 3 
console.log(p.sayhi())    // 返回 Hello 


// @範例二，構造函數 constructor() 可以指定將另一個對象返回給實例
console.log("==== 範例二 ====")

class A { }
let a = new A

class Foo {
    constructor() {
        // 指定構造函數返回另一個對象實例
        return Object.create(a)
    }
}

let f = new Foo()
console.log(f instanceof Foo) // 返回 false 
console.log(f instanceof A)   // 返回 true 

// @範例三，class 可使用表達式
console.log("==== 範例三 ====")

// 使用 class 表達式，可寫成立即執行的 class
var b = new class {
    constructor(name) {
        this.name = name
        this.y = 100
    }

    sayname() {
        console.log(this.name)
    }
}("An")

b.sayname()         // 返回 An 
console.log(b.y)    // 返回 100 

// @範例四，在 class 中使用 getter / setter
console.log("==== 範例四 ====")

class MyClass {
    constructor() {
        this._name = "NA"
    }

    get name() {
        console.log("getting ... ")
        return this._name
    }

    set name(value) {
        console.log("setting ... ")
        this._name = value
    }

}

var my = new MyClass()
console.log(my._name) // 返回 NA
my.name = "an"
console.log(my.name)    // 返回 an 

// @範例五，在 class 中使用 Generator
console.log("==== 範例五 ====")

class GG {
    constructor(...args) {
        this.args = args
    }

    // 將 Generator 指定給實例
    *[Symbol.iterator]() {
        for (var arg of this.args) {
            yield arg
        }
    }

    // 在 class 中使用一般的 Generator
    * list() {
        yield "123"
        yield "456"
    }

    getList() {
        for (var i of this.list()) {
            console.log(i)
        }
    }
}

var g = new GG("hello", "world")
g.getList()

for (var element of g) {
    console.log(element)
}

// @範例六，class 的靜態方法
console.log("==== 範例六 ====")

// 建立類的靜態的屬性和方法
class Parent {

    // 定義靜態方法
    static sayHi() { 
        console.log("hi") 
    }

    // 實例的構造函數
    constructor(name) {
        this.name = name
    }
}

// ES6 規定，類的靜態屬性，必須定義在 class 外部
Parent.ParentName = "Parent"

// 調用構造函數的靜態屬性
console.log(Parent.ParentName)  // 返回 Parent 
// 調用構造函數的靜態方法                 
Parent.sayHi()                  // 返回 hi 

// 實例無法調用靜態屬性或方法
var sonA = new Parent("sonA")
var sonB = new Parent("sonB")
console.log(sonA.ParentName)    // 返回 undefined
console.log(sonB.ParentName)    // 返回 undefined
console.log(sonA.name)          // 返回 sonA
console.log(sonB.name)          // 返回 sonB 


function tt(){
    console.log(new.target)    
}

var t = new tt()



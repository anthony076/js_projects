import "babel-polyfill"

async function* gen() {
    yield 'hello';
}
const genObj = gen();
genObj.next().then(x => console.log(x));
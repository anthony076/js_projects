
// Symbol 基本特性
function example1(){
    // Symbol() 的參數是為了方便識別
    let s1 = Symbol("boo")
    let s2 = Symbol("boo")

    // Symbol 是具有唯一性的字串
    console.log(s1 === s2)  // 返回 false
    
    // 賦值時是傳遞引用，所以 s3 = s1
    let s3 = s1
    console.log(s1 === s3)  // 返回 true

    // Symbol 轉成字串後就失去獨特性
    console.log(s1.toString() == s2.toString()) // 返回 true
}

// Symbol()會自動調用 Object 的 toString()返回值，當作輸入參數
// 若無 toString()，會顯示為 Symbol([object Object])
function example2(){ 

    var obj = {
        name:"an"
    }
    
    console.log(Symbol(obj))    // 返回 Symbol([object Object])

    var obj = {
        toString(){ return "abc" }
    }

    console.log(Symbol(obj))    // 返回 Symbol(abc)
}

// Symbol 的限制
function example3(){

    // Symbol 不能列舉
    var s = Symbol("abc")
    
    for (var i of s){
        console.log(i)      // Error
    }

    // Symbol 不能做字串相加
    console.log(s+"def")    // Error

    // Symbol 不能用點運算符
    let a = {
        [mySymbol]: 'Hello!'
      };
}

// Symbol 作為屬姓名的寫法，
// 把 Symbol值放在方括號中 [Symbol]，Symbol 才會被當成字串
function example4(){
    let name = Symbol()
    var obj = {}

    // 寫法一，
    // Symbol 不能用點運算符，點運算符後面接字串值，不是接 Symbol值

    // 錯誤寫法
    obj.name = "an"
    // 正確寫法
    obj[name] = "an"

    console.log(obj[name])      // 返回 an

    // 寫法二，在對象的內部使用Symbol 值定義屬性時，Symbol 值必須放在方括號之中
    // 屬姓名必須是字符串，name 是Symbol值，不是字符串，所以必須把 Symbol 值必須放在方括號
    var obj = {
        [name]: "an",
        foo:123
    }

    console.log(obj[name])      // 返回 an

    // 寫法三，透過 Object.defineProperty()
    var obj = {}
    Object.defineProperty(obj, name, {value:"an"})
    console.log(obj[name])      // 返回 an

    // 寫法四，Symbol 作為函數屬性的屬姓名
    var obj = {
        [name](){}
    }

}

// Symbol 屬姓名的遍歷
// Symbol 屬姓名無法被 for-in、for-of、Object.keys()、Object.getOwnPropertyNames()、JSON.stringify()返回
// Symbol 宣告的屬性，不是私有屬性，但只能被 Object.getOwnPropertySymbols() 和 Reflect.ownKeys() 返回
function example5(){
    let a = Symbol("a")
    let b = Symbol("b")

    const obj = {
        [a]: "aa",
        [b]: "bb"
    }
    
    console.log(Object.keys(obj))                   // 返回 []

    for(var i in obj){
        console.log(i)                              // 無輸出
    }

    console.log(Object.getOwnPropertySymbols(obj))  // 返回 [ Symbol(a), Symbol(b) ]
    console.log(Reflect.ownKeys(obj))               // 返回 [ Symbol(a), Symbol(b) ]
}

// Symbol.for() 的使用
function example6(){
    let s1 = Symbol.for('foo');
    let s2 = Symbol.for('foo');
    let s3 = Symbol("foo")
    
    console.log(s1 === s2)  // 返回 true
    console.log(s3 === s2)  // 返回 false
    
    // 取得 symbol.for 變數中的 key值
    console.log(Symbol.keyFor(s1))  // 返回 foo，
    console.log(Symbol.keyFor(s3))  // 返回 undefined，

}


//example1()
//example2()
//example3()
//example4()
//example5()
example6()

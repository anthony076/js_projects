/*
#1  七種數據類型
    三種原始類型（primitive type)
        最基本的數據類型，不能再細分，分為
        1_數值(number)
        2_字串(string)
        3_布林值(Boolean)

    兩種特殊類型
        undefined :表示“未定義”或不存在
        null :表示空值，即此處的值為空

    一種合成類型合成類型(complex type)
        因為一個對象往往是多個原始類型的值的合成，可以看作是一個存放各種值的容器，稱為對象類型(Object) 
        可分為
            1_狹義的對象(object)
            2_數組(array)
            3_函數(function)

    ES6 新增 ；Symbol

#2  檢查類型的運算符
    typeof運算符，
        用於返回一個值的數據類型的名稱，
        注意，此方法只適用於原始類型，對於合成類型，顯示都是 object，
        對合成類型，要改用 instanceof

    instanceof運算符，
        適用於原始類型或合成類型，需要提供類型名稱

    Object.prototype.toString方法

#3  null 和 undefined 的區別
    null，代表變數有定義，但未賦值，代表一個空的對象
        ，null 轉為數值時為 0 ；

    undefined，是一個表示"此處無定義"的原始值，轉為數值時為NaN (代表非數字 Not a Number)
             ，代表1_變量未定義 或 2_未傳入函數參數 或 3_函數無返回值

#4  布林值的轉換規則
    會被轉譯成 false 的值: undefined、null、0、NaN、空字符串、
    除了這些之外，都會被轉譯成 ture，例如 空數組（[]）和空對象（{}）

#5  數值經度
        精度最多只能到53個二進制位，這意味著，絕對值小於2的53次方的整數，即-2^53到2^53，都可以精確表示。
    
    數值範圍溢出
        JavaScript 浮點數的64個二進制位，可顯示範圍為 2^1024到2^-1023，
        超過 2^1024 稱為正向溢出，超過時會顯示 Infinity
        超過 2^-1023 稱為負向溢出，超過時會顯示 0
*/

// 用 typeof 運算符，來檢查變量類型
function typeof_example(x){
    console.log("==== check_type() ====")

    if (typeof x === 'number'){
        console.log(x)
    } else {
        console.log("x type error")
    }
}

function instanceof_example(){
    console.log("==== instanceof_example() ====")
    console.log(typeof [])      // object
    console.log(typeof {})      // object
    console.log( [] instanceof Array)   // True
    console.log( {} instanceof Array)   // false
}

typeof_example(5)
typeof_example('5')

instanceof_example()


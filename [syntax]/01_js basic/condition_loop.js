// if 的單行表達式範例
function if_example_simple(x){
    console.log("==== if_example_simple() ====")

    if (x === 5) return x
}

// if 的多行表達式範例
function if_example(x){
    console.log("==== if_example() ====")

    if (x > 10){
        return "x > 10"
    } else if (x < 10) {
        return "x < 10" 
    } else {
        return "undefined"
    }
}

// switch 範例
// 注意，一定要有 break，否則會一直執行到有 break 語句、結尾或 return 的地方
function switch_example(x){
    console.log("==== switch_example() ====")

    switch(x){
        case 0:
            return 0
        case 1:
            return 1
        default:
            return "not found"
    }
}

// 三元表達式 範例
// 語法 (判斷式) ? <true 返回的結果> : <false 返回的結果>
function ternary_example(x){
    console.log("==== ternary_example() ====")

    return (x >= 10) ? "x>=10" : "x<10" 
}

// for 範例
// 語法，for (初始化表達式; 條件表達式; 遞增表達式) { 程式碼 }
function for_example(){
    console.log("==== for_exmpale() ====")
    var data = []
    for (var i=0; i<10; i++){
        data.push(i)
    }
    return data     //[ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 ]
}

// while-loop 範例
function while_example(){
    console.log("==== while_example() ====")

    var i = 0

    while(i < 10){
        console.log(i)
        i = i + 2
    }
}

function while_with_break(){
    console.log("==== while_with_break() ====")

    var i = 0

    while(i < 100){
        console.log(i)

        if(i === 20) break
        
        i = i + 2
    }
}

// do-while 範例，至少執行一次
// 先運行一次循環體，然後判斷循環條件
// 語法，do{程式碼} while(循環條件)
function do_while(x){
    console.log("==== do_while() ====")

    do{
        console.log(x)
        x++
    } while (x < 5)
}


// 在迴圈中使用 label
function label_example(){
    console.log("==== label_example() ====")

    top:
    for (var i = 0; i < 3; i++){

      for (var j = 0; j < 3; j++){
          
        if (i === 1 && j === 1){

            // 跳到指定的標籤外，直接跳出雙層循環
            // 若只有使用 break 不指定標籤，是跳出單層循環
            break top;
        }
        console.log('i=' + i + ', j=' + j);
      }
    }

}


if_example_simple(5)
if_example(5)
switch_example(0)
ternary_example(5)
console.log(for_example())
while_example()
while_with_break()
do_while(3)
label_example()
// ==== 範例一，在對象中的 this ====
function this_in_object() {

    var Person = {
        name: "An",
        describe: function () {
            // this 指當前的對象 Person
            return "姓名:" + this.name
        }
    }

    console.log(Person.describe())  // 姓名:An

}

//this_in_object()

// ==== 範例二，當 this 傳遞給不同的實例 ====
function this_in_differnt_object() {

    var A = {
        name: "An",
        describe: function () {
            // this 指當前的對象 A
            return "姓名:" + this.name
        }
    }

    var B = {
        name: "ching",
    }

    B.describe = A.describe

    console.log(B.describe())   // 姓名:ching
}

//this_in_differnt_object()

// ==== 容易出錯的寫法一，在對象中形成了內嵌函數結構中使用 this ====

// 有問題的寫法
var o = {
    f1: function () {
        console.log('outside:' + this);

        // f2-this 所在的環境，在另一個函數宣告中(非實例) = 沒有實例對象，
        // 因此 f2-this 指向全局
        var f2 = function () {
            console.log('inside:' + this);
        }();
    }
}

// outside: object
//o.f1()  // inside: Window

/*
// 修正的寫法
var o = {
    f1: function () {
        console.log("outside:" + this);

        var that = this;

        var f2 = function () {
            console.log("inside:" + that);
        }();
    }
}

        // outside: object
o.f1()  // inside: object
*/

// ==== 容易出錯的寫法二，在對象方法中，透過高階函數使用 this ====

// 有問題的寫法
var p = {
    v: 'hello',
    p: ['a1', 'a2'],
    
    // 在對象方法中，使用 array 的處理方法(高階函數)，
    // 容易形成 `內嵌函數結構中使用 this，造成內層 this指向外層實例
    f: function f() {
        this.p.forEach(function (item) {
            console.log(this.v + ' ' + item);
        });
    }
}

        // undefined a1
p.f()   // undefined a2


/*
// 修正的寫法
var p = {
    v: 'hello',
    p: ['a1', 'a2'],
    
    // 在對象方法中，使用 array 的處理方法(高階函數)，
    // 容易形成 `內嵌函數結構中使用 this，造成內層 this指向外層實例
    f: function f() {
        var that = this
        this.p.forEach(function (item) {
            console.log(that.v + ' ' + item);
        });
    }
}

        // hello a1
p.f()   // hello a2
*/

// ==== 容易出錯的寫法三，在回調函數中使用 this ====
// 回調函數一定是全局的，未綁定任何上下文(context)，因此在回調函數中的 this，一定是指向全局實例

// 解決回調函數中不能使用 this 的方法 ====
// 方法一，透過 Function.prototype.call()
// 用於欲綁定的對象沒有使用 this
/*
var q = {
    name:"i am qq"
}

// 模擬回調函數，回調函數一定是全局的
// 因此，回調函數中的 this 一定指向全局實例
var f = function(x){
    console.log(this.name + x)
}

// 指定函數的作用域，並調用函數
f.call(q, ", hello")       // "i am qq"
*/


// 方法二，透過 Function.prototype.apply()
// 用於欲綁定的對象沒有使用 this，且可用 array 形式傳入參數
var q = {
    name:"i am qq"
}

// 模擬回調函數，回調函數一定是全局的
// 因此，回調函數中的 this 一定指向全局實例
var f = function(x){
    console.log(this.name + x)
}

// 指定函數的作用域，並調用函數
f.apply(q, [", hello"])       // "i am qq"


// 方法三，透過 Function.prototype.bind()
// 用於欲綁定的對象已經被綁定，bind() 可以重新綁定後得到新函數
// 用於欲綁定的對象`已經有`使用 this
var cc = {
    count: 99
}

var counter = {
    count: 0,
    inc: function(){
        return this.count++
    }
}

// counter.inc() 中的 this 已經綁定到 counter-object
counter.inc()
console.log('count:' + counter.count)   // count:1

// 將 counter.inc() 透過 bind 重新綁定到 cc-object
var newf = counter.inc.bind(cc)
newf()
console.log('cc:' + cc.count)           // cc:100

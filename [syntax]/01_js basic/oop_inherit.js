
// ==== 透過構造函數的 prototype屬性繼承 ====
// 類似類成員，所有繼承的屬性和方法都是單例的，會連動
console.log("==== 透過構造函數的 prototype屬性繼承 ====")

var parent = {
    name: "base"
}

// 建立構造函數
function baseConstructor(name) { }

// 指定繼承對象
baseConstructor.prototype = parent

// 利用 構造函數名.prototype 取得原型對象(父類)
// 透過 prototype，為所有的實例添加屬性 (相當於設置父類的類屬性)
//baseConstructor.prototype.color = 'blue'  //  寫法一
parent.color = 'blue'                       //  寫法二

// 建立實例對象 (建立子類實例)
var aa = new baseConstructor("aa")
var bb = new baseConstructor("bb")

console.log(aa.color)   // blue
console.log(bb.color)   // blue

// 原型對象類似單例的效果，一經改變，會連帶修改所有的實例
// 相當於更改父類的類屬性
//baseConstructor.prototype.color = 'pink'  //  寫法一
parent.color = 'pink'                       //  寫法二

console.log(aa.color)   // pink
console.log(bb.color)   // pink

// 若在實例中覆寫原型的屬性，覆寫的結果只會保留在該實例中
// 相當於覆寫父類的屬性
bb.color = 'black'
console.log(aa.color)   // pink
console.log(bb.color)   // black


// ==== 透過 constructor 創建實例會遇到的問題 ====
console.log("==== 透過 constructor 創建實例會遇到的問題 ====")

// 構造函數 P()
function P(name) {
    this.name = name
}

var pa = new P("pa")

// P.prototype指向原型對象，
// P.prototype.constructor指向原型對象對應的構造函數，
// 所以 P.prototype.constructor = P
var pb = new P.prototype.constructor("pb")
console.log(pa instanceof P)    // true
console.log(pb instanceof P)    // true

// 未改變原型對象前
console.log(P.prototype.constructor)   //[Function: P]
// 變更原型對象時，原型對象指向的構造函數會跟著更改
P.prototype = { age: 123 }
console.log(P.prototype.constructor)   //[Function: Object]

// 若變更原型對象 + 利用 constructor 創造實例，會造成非預期效果
var pc = new P.prototype.constructor("pc")
console.log(P.prototype.constructor)    // [Function: Object]，更改原型對象時，原型對象指向的構造函數會跟著更改
console.log(pc instanceof P)            // false，透過 constructor 創建實例，會遇到變更原型對象時，連構造函數一起變更的問題
console.log(pc.name)                    // undefined，因為構造函數被變更了，所以沒有 name 的屬性

// 解決方法一，不要透過 constructor 創建實例，
// constructor 是動態的，會隨著 prototype 一起變動
var pd = new P('pd')
console.log(P.prototype.constructor)    // [Function: Object]
console.log(pd instanceof P)            // true
console.log(pd.name)                    // pd

// 解決方法二，在變更原型對象時，將 constructor 指向原來的構造函數
P.prototype = {
    age: 123,
    constructor: P,
}

var pe = new P.prototype.constructor("pe")
console.log(P.prototype.constructor)    // [Function: P]
console.log(pe instanceof P)            // true
console.log(pe.name)                    // name

// ==== 構造函數的繼承 (在子類中呼叫父類的構造函數) ====
console.log("==== 構造函數的繼承 (在子類中呼叫父類的構造函數) ====")

function Shape() {
    this.x = 0;
    this.y = 0;
}

Shape.prototype.move = function (x, y) {
    this.x += x;
    this.y += y;
    console.info('Shape moved.');
};

// 第一步，子類繼承父類的構造函數 (在子類中調用父類的構造函數)
function Rectangle() {
    // 用 call 或 apply 都可以，有參數就用 apply
    Shape.call(this); // 调用父类构造函数
}

// 第二步，(非必要) 指定子類`是否`要繼承父類的屬性和方法 
// 非必要，如果不需要繼承父類屬性和方法就不需要添加

// 寫法一，透過 Object.create() 指定原型對象，
// Object.create() 是創建新的原型實例對象，可避免因引用傳遞，造成連父類的原型Super.prototype一起被修改的問題
// Object.create() 的用法，
// https://developer.mozilla.org/zh-TW/docs/Web/JavaScript/Reference/Global_Objects/Object/create
Rectangle.prototype = Object.create(Shape.prototype);     

// 寫法二，直接指定原型對象，不推薦
// 因為對象的賦值是傳遞引用，此方法會有連父類的原型Super.prototype一起修改的疑慮
//Rectangle.prototype = Shape.prototype;                    

// 寫法三，透過 new
// 透過 new，子類會具有父類實例的屬性和方法，對於只想要初始化屬性，不需要父類方法時，不適用
//Rectangle.prototype = new Shape();

// 確保 Rectangle 的原生對象指向的構造函數是Rectangle
Rectangle.prototype.constructor = Rectangle;

var rect = new Rectangle();

console.log(rect instanceof Rectangle)  // true
console.log(rect instanceof Shape)      // true


// ==== 多重繼承(Mixin)範例 ====
// 父類 M1 的構造函數
function M1() {
    this.hello = 'hello';
}

// 父類 M2 的構造函數
function M2() {
    this.world = 'world';
}

// 子類 S 的構造函數
// 多重繼承，在子類的構造調用多個父類的構造函數
function S() {
    M1.call(this);  // 調用父類 M1 的構造函數
    M2.call(this);  // 調用父類 M2 的構造函數
}

// 讓子類S 繼承父類 M1 的成員
// 建議透過 Object.create() 去指定子類 prototype 屬性，避免因傳遞引用造成對父類的修改
// Object.create() 的用法，
// https://developer.mozilla.org/zh-TW/docs/Web/JavaScript/Reference/Global_Objects/Object/create
S.prototype = Object.create(M1.prototype);

// 將父類 M2 的成員，添加到 父類 M1 中
// 透過 Object.assign()，複製M2的屬性到子類S中，注意 Object.assign 是淺拷貝
// https://developer.mozilla.org/zh-TW/docs/Web/JavaScript/Reference/Global_Objects/Object/assign
Object.assign(S.prototype, M2.prototype);

// 指定 prototype後，constructor會跟著改變，
// 手動指定子類的構造函數，確保 constructor 是正確的
S.prototype.constructor = S;

var s = new S();
console.log(s.__proto__)    
console.log(Object.getPrototypeOf(s))    

console.log(s.hello) // 'hello'
console.log(s.world) // 'world'

// ==== 封裝私有變數 ====
console.log("==== 封裝私有變數 ====")
/*
// 方法一，將 getter/setter 寫在構造函數中 (利用構造函數中的閉包)
//      優點，私有變量無法從外部讀寫，不是很安全
//      缺點1，違背構造函數與實例對像在數據上相分離的原則
//      （即實例對象的數據，不應該保存在實例對像以外）
//      缺點2，數據總是存在內存，耗費內存
function Person(name){
    this.name = name
    var age = 0

    this.setAge = function(num){
        age = num
    }

    this.getAge = function(){
        return age
    }
}

an = new Person("Anthony")
console.log(an.age)         // undefined，私有變數，不能直接存取

an.setAge(100)              // 透過 setter 寫入私有變數
console.log(an.getAge())    // 100, 透過 getter 讀取私有變數
*/

/*
// 方法二，將 getter/setter 寫在原型對象中
// 缺點，私有變數可透過外部存取
function Person(name){
    this.name = name
    this.age = 0
}

Person.prototype = {
    // 確保原型對象指向的構造函數是正確的
    constructor: Person,

    // 定義 getter
    getAge:function(){
        return this.age
    },

    // 定義 setter
    setAge:function(num){
        this.age = num
    }
}

an = new Person("Anthony")
console.log(an.age)         // 0，私有變數寫在原型對象，可以直接存取

an.setAge(100)              // 透過 setter 寫入私有變數
console.log(an.getAge())    // 100, 透過 getter 讀取私有變數
*/

/*
// 方法三，將 getter/setter 寫在一般函數中
// 注意，一般函數需要調用，才可取得返回值，而 IIFE函數 可直接取得返回值
function Person(name){
    this.name = name
};

var base = function(){
    var _age = 0;
    
    // 定義 getter
    var getAge = function(){
        return _age
    };

    // 定義 setter
    var setAge = function(num){
        _age = num
    };

    return {
        getAge : getAge,
        setAge : setAge,
    };
};

Person.prototype = base()

an = new Person("Anthony")
console.log(an.age)         // 0，私有變數寫在原型對象，可以直接存取

an.setAge(100)              // 透過 setter 寫入私有變數
console.log(an.getAge());    // 100, 透過 getter 讀取私有變數
*/


// 方法四，將 getter/setter 寫在立即執行的函數表達式(IIFE)中
// 注意，一般函數需要調用，才可取得返回值，而 IIFE 可直接取得返回值
function Person(name){
    this.name = name
};

var base = (function(){
    var _age = 0;
    
    // 定義 getter
    var getAge = function(){
        return _age
    };

    // 定義 setter
    var setAge = function(num){
        _age = num
    };

    return {
        getAge : getAge,
        setAge : setAge,
    };

})();

Person.prototype = base

an = new Person("Anthony")
console.log(an.age)         // 0，私有變數寫在原型對象，可以直接存取

an.setAge(100)              // 透過 setter 寫入私有變數
console.log(an.getAge());    // 100, 透過 getter 讀取私有變數


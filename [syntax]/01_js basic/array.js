var arr = ['a', 'b', 'c']

// ==== array 是 object 的一種 ====
// array 是一種特殊的 oject，不需要鍵名的 object
console.log(typeof [1,2,3]) // object
// js 會自動為 array 補上鍵名
console.log(Object.keys(arr))   // [ '0', '1', '2' ]

// 判斷是否為 array
console.log([1,2,3] instanceof Array)   //true

// array 是 object 的一種，因此可以為 array 添加屬性，不影響 .length
arr['max'] = 666
console.log(arr.max)    // 666
console.log(arr)        // [ 'a', 'b', 'c', max: 666 ]


// ==== 元素的取用 ====
// 取用/賦值 array 中的元素時，可透過數字，js 會自動將數字轉換成字串
console.log(arr[0])     // a
arr[0.00] = 'aa'        // js 會將 0.00 轉換成文字的 '0'
console.log(arr[0])     // aa

// array 不能使用點符號來存取元素
//arr.0   // sytax error


// ==== array 中的空位 ====
arr = [ , , 2,]             // 逗號前方沒有值，會造成空位

console.log(arr.length)     // 3，空位是有宣告，但未定義的值，.length 會納入計算
console.log(arr[0])         // undefined，空位取用的結果，
delete arr[0]             
console.log(arr.length)     // 3，透過 delte 刪除元素，會造成空位，因此 length 不變
                            // length 會包含空位

// 空位的列舉                            
//1 使用 forEach、for-in、object.keys 遍歷元素時，會自動跳過空位
//  使用 .length 遍歷元素時，不會自動跳過空位，因為 length 會包含空位
//2 若將元素指定為 undefined，就不會跳過空位，因為手動指定為 undefined，就不是空位，其值為 undefined-object
//  undefined-object 也是數據類型的一種
console.log("空位的列舉")
arr.forEach(element=>console.log(element))

for (var key in arr){
    console.log(key)
}
for (var i=0;arr.length;i++){
    console.log(arr[i])
}

// ==== 常用 array 屬性和方法 ====
// 取得 arry 元素的數量，注意 .length 返回的是鍵名中最大整數 +1 
var arrr = [0]
console.log(arrr.length)    // 1
arrr[9] = 9 
console.log(arrr.length)    // 10
console.log(arrr)           // [0, 9]

// 透過 .length 刪除或清空或補全元素
// 當使用者指定 .length 時，js 會自動根據設定的 length值，調整元素的個數，
arrr.length = 1
console.log(arrr)       // [0]，刪除元素
arrr.length = 0         
console.log(arrr)       // []，清空 arry
arrr.length = 3
console.log(arrr)       // [0, 0, 0]，補全 元素

// 透過array 內建的高階函數 .forEach() 來列舉元素
arrr = [5,6,7,8]
arrr.forEach(element => console.log(element))  //.forEach() 是高階函數

buf = new Buffer(256)
console.log(typeof buf)

function object_is_referenced(){
    var a = {'a':'aa'}
    var b = a
    console.log(b)    // undefine

    a.p = 1             // 修改a的屬性
    console.log(a)
    console.log(b)    // b的屬性連動
}

function ReadWrite_object_property(){
    var obj = {
        p:"Hello World"
    }

    // 透過點運算符讀寫 object 屬性
    obj.p = "An"
    console.log(obj.p)

    // 透過方括號運算符讀寫 object 屬性
    obj['p'] = "ching"
    console.log(obj['p'])
}

function common_object_method(){

    var obj = {
        aa:123,
        bb:456,
        cc:789,
    }

    // 查看所有的鍵名
    console.log(Object.keys(obj))   // [ 'aa', 'bb', 'cc' ]

    // 刪除屬性
    delete obj.aa
    console.log(Object.keys(obj))   // [ 'bb', 'cc' ]

    // 判斷屬性(鍵名)是否存在
    console.log('aa' in obj)        // false
    console.log('bb' in obj)        // true

    // 利用 for-in 列舉 object 屬性
    for (var i in obj) {
        console.log('key：', i);
        console.log('value：', obj[i]);
    }
}

function use_with(){

    var obj = {
        p1: 1,
        p2: 2,
      };

      /*
      以下等同於
      obj.p1 = 4
      obj.p2 = 5
      */
      with (obj) {
        p1 = 4;
        p2 = 5;
      }
      console.log(obj.p1)
      console.log(obj.p2)
}



// object 賦值是傳遞引用的範例
//object_is_referenced()

// 讀取/寫入 object 中的屬性，可透過點運算符或方括號運算符
//read_object_property()

// object 常用的方法
//common_object_method()

// 透過 with 簡化 同一個對象的多個屬性
use_with()

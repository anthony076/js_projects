// ==== 構造函數基礎 ====
// 在函數內使用 this，代表此函數是構造函數
function A(){
    this.name = "abc"
}

// 構造函數必須透過 new 來調用
var a = new A()
console.log(a.name)

// ==== 避免構造函數被當做一般函數使用 ====
// 方法一，使用嚴格模式
function B(){
    // 使用嚴格模式
    'use strict'

    this.age = 100
}

var b = new B()

// 方法二，檢查調用者的類型
function C(){
    // 若是透過 new 調用，會產生 C的實例，若不是C的實例，自動返回一個 C 的實例
    if (!(this instanceof C)) {
        return new C();
      }
    
    this.age = 100
}

c1 = new C()
c2 = C()
console.log(c1 instanceof C)    // true
console.log(c2 instanceof C)    // true

// 方法三，檢查是否使用 new 命令
// 若使用 new 命令，則 new.target 會指向構造函數，若否，new.target = undefined
function D(){
    if (!new.target){
        throw new Error("please use new command")
    }

    this.age = 100
}
var d = new D()

// ==== 構造函數中的 return ====
//1 構造函數即使不加 return ，預設會返回 this 的實例對象
//  若加 return ，會返回 指定在 return 後方的新對象
//2 對一般函數使用 new ，則會返回空對象 (empty object)
var Vehicle = function (){
    this.buyer = 1000;
    return { price: 2000 };
  };
  
v = new Vehicle()
console.log(v.price)    // 2000
console.log(v.buyer)    // undefined


// 利用 function 命令宣告函數
function print(s){
    console.log(s)
}

// 利用 匿名函數(或稱 函數表達式) 宣告函數
var pprint = function(s){
    console.log(s)
}

// 遞歸範例
function fib(num) {
    if (num === 0) return 0;
    if (num === 1) return 1;
    return fib(num - 2) + fib(num - 1);
}

// 函數常用的內建屬性和內建方法
function common(a,b,c){ 
    console.log(b)
}

// 函數參數預設值的使用
function multiply(a, b = 2) {
    return a * b;
}


// ==== 利用 function 命令宣告函數範例 ====
//print("abc")            // abc

// ==== 利用 匿名函數(函數表達式) 宣告函數範例 ====
//pprint("def")           // def

// ==== 遞歸範例 ====
//console.log(fib(6))     // 8

// ==== 函數常用的內建屬性和內建方法 ====
//console.log(common.name)        // 取得函數名，common
//console.log(common.length)      // 取得函數宣告的引數長度, 3
//console.log(common.toString())    // 取得函數源碼, function common(a,b,c){ console.log(b)}

// ==== 函數參數預設值的使用 ====
//console.log(multiply(5))  // 10

// ==== 立即調用的函數表達式（IIFE）====
// 一般的函數表達式，需要調用才會執行
var add = function(a, b){
    return a + b
}

add(3,2);    // 5

// 立即調用的函數表達式（IIFE）
// 語法 ( function 函數名(){程式碼} () )
//1 最外層的圓括號，代表裡面的是會立即執行的表達式，而不是定義用的代碼區塊
//2 注意，使用IIFE，前一個語句一定要加分號 ; 
//  避免解析表達式時，會連前一句一起解析，而造成 Type error 的錯誤
(function iadd() { console.log("add") }())

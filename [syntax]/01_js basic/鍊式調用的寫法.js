// =========================
// 鍊式調用寫法一，透過一般對象
// 手動建立對象並直接返回該對象，不透過構造函數+原型鏈
// =========================
function factory() {
    let obj = {
        result: 0,
        init: function () { },
        add: function () { },
        minus: function () { },
    }

    obj.init = function (value) {
        this.result = value
        return this
    }

    obj.add = function (n) {
        this.result = this.result + n
        return this
    }

    obj.minus = function (n) {
        this.result = this.result - n
        return obj
    }

    return obj
}

const f = factory()
//console.log(f)  // return { result: 0, init: [Function], add: [Function], minus: [Function] }
//console.log(f.result)   // 返回 0
//console.log(f.init(10).add(5).minus(3).result)  // 返回 12

// 可以覆寫原有的函數
f.add = function (n) {
    this.result = 2 * n
    return this
}

//console.log(f.init(0).add(5).result)    // 返回 10

// 可以添加新的屬性或函數
f.multiple = function (n) {
    this.result = this.result * n
    return this
}

//console.log(f.init(3).multiple(5).result)    // 返回 15

// ===========
// 鍊式調用寫法二，透過構造函數+原型鏈產生的實例對象
// 缺點，繼承函數 (利用 .prototype 添加的函數), 是實例方法，不會直接顯示在實例對象的打印中
//      實例方法只能透過實例調用，但不會透過 console 顯示出來
// ===========
function factoryB(val) {
    if (val) {
        this.result = val
    } else {
        this.result = 0
    }
}

factoryB.prototype.init = function (value) {
    this.result = value
    return this
}

factoryB.prototype.add = function (value) {
    this.result = this.result + value

    //return factoryB // 錯誤用法，factoryB 是構造函數，不是實例對象
    return this
}

factoryB.prototype.minus = function (value) {
    this.result = this.result - value
    return this
}

const b = new factoryB(10)
console.log(b)          // 返回 factoryB { result: 10 }
console.log(b.result)   // 返回 10
console.log(b.init(10).add(5).minus(3).result)  // 返回 12


// ===========
// 鍊式調用方法三，使用構造函數的函數屬性 (不使用原型鏈)
// 不使用原型鏈，而是直接將方法定義在構造函數產生的屬性上
// ===========
function factoryC(val) {
    if (val) {
        this.result = val
    } else {
        this.result = 0
    }

    this.init = function (n) {
        this.result = n
        return this
    }

    this.add = function (n) {
        this.result = this.result + n
        return this
    }

    this.minus = function (n) {
        this.result = this.result - n
        return this
    }
}

const c = new factoryC(10)
console.log(c) // 返回，factoryC { result: 10, init: [Function], add: [Function], minus: [Function] }

console.log(c.result)   // 返回，10
console.log(c.init(10).add(5).minus(3).result)  // 返回，12

// var 建立變數
// 宣告變量a，在變量a與數值1之間建立引用關係，稱為將數值5 賦值給變量a 
var a = 5
console.log(a)

// 聲明變量但未賦值
var b;
b // undefined

// 類型自動推斷，但不利於IDE顯示類型
c = 1
console.log(c)

// 一次聲明多個變量
var d, e

// 變量可重新賦值(更改變量引用的對象)，但IDE顯示類型不正確
var f = 123
f = '456'
console.log(f)

/*
js 中的變量提升(hoisting)
JavaScript 引擎的工作方式是，先解析代碼，獲取所有被聲明的變量，然後再一行一行地運行。
這造成的結果，就是所有的變量的聲明語句，都會被提升到代碼的頭部，這就叫做變量提升（hoisting）。
    console.log(g);
    var g = 100;        // undefined

等同於
    var a;
    console.log(a);
    a = 1;
*/


/*
變數命名規則
第一個字符，可以是任意Unicode字母（包括英文字母和其他語言的字母），以及美元符號（$）和下劃線（_）。
第二個字符及後面的字符，除了Unicode字母、美元符號和下劃線，還可以用數字0-9。
合法變數名
    arg0
    _tmp
    $elem
    π
    臨時變量，可以使用中文當作變數名
非法變數名
    1a // 第一個字符不能是數字 
    *** // 標識符不能包含星號 
    a+b // 標識符不能包含加號 
    -d // 標識符不能包含減號或連詞線
保留字
    arguments、break、case、catch、class、const、continue、debugger、
    default、delete、do、else、enum、eval、export、extends、false、finally、for、 
    function、if、implements、import、in、instanceof、interface、let、new、null、package、
    private、protected、public、return、static、super、switch、this、throw、true、try、
    typeof、var、void、 while、with、yield
*/

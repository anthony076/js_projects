## 使用說明


### 必要檔案
檔案|說明
:--|:- 
tasks.json | 給 vscode 的 Run task 用
tsconfig.json | 給 typescript 編譯器用的配置檔
typings | 編寫 ts 時，用於第三方模塊的提示，若沒有此套件，ts 會找不到模塊
typings.json | typings 的配置檔，必須要有此檔案才能安裝第三方模塊的 dt


### 執行方式
- 安裝 package 和 types<br> 
    `00_install.bat` or `npm install & typings install debug --save`

- 三種執行方式<br>
    - Option1，手動編譯並執行<br>
    `01_build.bat` or `tsc main.ts & node main.js` <br>
    - Option2，透過 vscode 的 `Run-task` 進行編譯，但不執行，需要 `tasks.json` + `tsconfig.json`<br>
    `Ctrl + Shift + B`
    - Option3，透過 vscode 的 `Code Runner` 進行編譯，但不執行<br>
    需要全域安裝 `ts-node` 和區域安裝 `typescript`



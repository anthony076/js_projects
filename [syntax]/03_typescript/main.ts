/*
    // For export default { add, minus }
    import mylib from "./lib/mylib";

    console.log(mylib.add(10, 5))       // 15
    console.log(mylib.minus(10, 5))       // 5
*/

// For export { add, minus }
import { add, minus } from "./lib/mylib";

console.log(add(10, 5))       // 15
console.log(minus(10, 5))       // 5


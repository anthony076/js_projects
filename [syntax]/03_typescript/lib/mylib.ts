
const add = (a, b) => a + b
const minus = (a, b) => a - b

// ==== 導出方法一 ====
//1 導入方式，
//  import mylib from "./lib/mylib"
//2 調用方式 mylib.add()
//export default { add, minus }

// ==== 導出方法二 ====
//1 導入方式，
//  import { add, minus} from "./lib/mylib"
//2 調用方式 add()
export { add, minus }
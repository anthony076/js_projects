## Websocket Demo by `ws` Library


### Description
Basic communication example for node server/client side.
Client send a `start` command to server, then server will response data by async data stream.

### Stack
- ws
- chance (to generate random data)
- most.js (to generate async data stream)

### Usage
- npm install (or use `00_install.bat`)
- `node server` for server-side
- `node node_client` for client-side 
- use `02_clean.bat` to clean `node_modules` folder

### Result
![](./screenshot.png)


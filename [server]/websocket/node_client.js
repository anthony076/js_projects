const Websocket = require("ws");

// 執行此語句後就會開始與伺服端建立連線
let ws = new Websocket('ws://localhost:3000/test');

// 成功與 Server 建立連接後被調用
ws.on('open', function () {
    console.log(`Connection is established ...`);

    ws.send("start")

});

// 接收 server 回傳的訊息
ws.on('message', function (msg) {
    console.log(msg)
});

// Server 關閉連線的時候被調用
ws.on("close", function (msg) {
    if (msg === 1006) {
        console.log("Close connection...")
    }

    ws.close()
})

// client 發生錯誤時會調用此函數
ws.on("error", (err) => {
    errMsg = err.message

    if (errMsg.includes(400)) {
        console.log("[Err]: websocket connection path is not correct ...")
    } else if (errMsg.includes(401)) {
        console.log("[Err]: user verify not pass")
    } else if (errMsg.includes("ECONNREFUSED")) {
        console.log("[Err]: server not online")
    } else if (errMsg.includes("ECONNRESET")) {
        console.log("[Err]: server error")
    } else {
        console.log(errMsg)
    }
    ws.close()
})

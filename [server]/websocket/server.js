const WebSocket = require('ws');
const WebSocketServer = WebSocket.Server;

// 產生亂數數據
const Chance = require("chance")
const random = new Chance()
// 產生異步數據流
const most = require("most")

// 產生資料
const genInt = () => random.integer({ min: 0, max: 100 })
const genCity = () => random.city()

// 驗證客戶端
const check = (info) => {
    if (info.req.url === "/test") {
        return true
    } else {
        return false
    }
}

// 啟動 websocket server
const port = 3000
const feedSpeed = 500
const server = new WebSocketServer(
    {
        port: port,
        path: "/test",     // 限定路徑
        //maxPayload: 3,   // 限定訊息長度
        verifyClient: check
    }
);

// ==== 註冊客戶端建立連線後的相關回調事件 ====
// on-connection 客戶端成功建立連接後調用
server.on('connection', function (client) {

    console.log(`Client is connected ...`);

    // 利用 ping-pong 檢查 client 是否回應逾時
    client.isAlive = true;
    client.on('pong', () => this.isAlive = true);

    // 透過 on-message 監聽 Client-side 的訊息
    client.on('message', function (msg) {
        const s1$ = most.periodic(feedSpeed)
            .take(10)
            .map(() => JSON.stringify({ int: genInt(), city: genCity() }))
        s1$.observe(el => client.send(el))

    })

    // 監聽客戶端觸發的的 client.close()
    client.on('close', function (message) {
        console.log("Client disconnected ...")
    });

    // client 設定錯誤的時候被調用
    client.on("error", (err) => console.log(err.message))

});

// server 綁定 port 的時候被調用
server.on("listening",
    () => console.log(`Server is on localhost:${port}`)
)


// server 運行期間發生錯誤的時候被調用
server.on("error",
    (err) => console.log(err.message)
)

// 利用 ping-pong 檢查 client 是否回應逾時
setInterval(function ping() {
    server.clients.forEach(function each(client) {
        // 若 isAlive == false 就直接關閉
        // 回應逾時的時候，不透過 client.close() 關閉連線，
        // client.close() 是交互命令，會通知 client 要關閉連線，
        // 但因為已經回應逾時，因此 client 是無反應的，因此使用 client.terminate()
        // 不推薦對正常連接使用，client.terminate() 不會通知客戶端
        if (client.isAlive === false) return client.terminate();

        // 若 isAlive == true，
        // 先將 isAlive 設為 false，有反應才改為 true
        // 若無反應，下一次檢查時會關閉
        client.isAlive = false;
        client.ping();
    });
}, 10000);
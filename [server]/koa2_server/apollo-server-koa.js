const Koa = require('koa');
const app = new Koa();

const { ApolloServer } = require('apollo-server-koa');
const { GraphQLObjectType, GraphQLSchema, GraphQLString } = require('graphql');

let schema = new GraphQLSchema({
    // 定義 query 操作
    query: new GraphQLObjectType({
        name: "Query",
        fields: {
            // 使用 authors 查詢時，返回 authorModel 的所有資料
            hello: {
                // 返回資料的類型
                type: GraphQLString,

                // 定義取回資料的執行函數
                resolve: async () => "Hello apollo-server-koa"
            }
        }
    })
})

const server = new ApolloServer({ schema: schema });
server.applyMiddleware({ app });

app.listen({ port: 4000 }, () =>
    console.log(`Server ready at http://localhost:4000${server.graphqlPath}`),
);
// Need npm install apollo 
const { createApolloFetch } = require('apollo-fetch');

const fetch = createApolloFetch({
    uri: 'http://localhost:4000/graphql',
});

fetch({
    query: '{ hello }',
}).then(res => {
    console.log(res.data);
});


# Linux 版本的測試命令，
# 注意，不能用於 Windows 的 curl，json-format 不對
curl -X POST -H "Content-Type: application/json" -H "Accept: application/graphql" --data '{"query": "query {hello}" }' http://localhost:4000/graphql
curl -X POST -H "Content-Type: application/json" -H "Accept: application/graphql" -d '{"query": "query {hello}" }' http://localhost:4000/graphql
curl -X POST -H "Content-Type: application/json" -d '{"query": "query {hello}" }' http://localhost:4000/graphql
curl -X POST -H "Content-Type: application/json" -d '{"query": "{hello}" }' http://localhost:4000/graphql

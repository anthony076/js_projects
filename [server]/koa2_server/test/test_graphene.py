from gql import Client, gql
from gql.transport.requests import RequestsHTTPTransport

client = Client(transport=RequestsHTTPTransport(url='http://localhost:4000/graphql'))
query = gql(''' {hello} ''')

print(client.execute(query))
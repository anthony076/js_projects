const Koa = require('koa');
const mount = require('koa-mount');
const graphqlHTTP = require('koa-graphql');
const { buildSchema } = require('graphql');

const cors = require('@koa/cors');

const app = new Koa();

// Construct a schema, using GraphQL schema language
var schema = buildSchema(`
  type Query {
    hello: String
  }
`);
// The root provides a resolver function for each API endpoint
var root = {
  hello: () => 'Hello world!',
};

app.use(cors())
app.use(mount('/graphql', graphqlHTTP({
  schema: schema,
  rootValue: root,
  graphiql: true
})));

app.listen(4000, () => console.log("Server is located at http://localhost:4000/graphql"));